unit KelolaUser;

interface

uses
  USystemMenu, StdCtrls, Classes, Controls, Grids, BaseGrid, AdvGrid, AddUser, Login,
  Windows, Messages, SysUtils, Variants, Graphics, Forms,
  Dialogs, AdvGlowButton, AdvObj, AdvOfficeButtons, AdvPanel, ExtCtrls, Gauges;

type
  TfrmKelolaUser = class(TForm)
    btnCancel: TAdvGlowButton;
    MainPanel: TPanel;
    Panel1: TPanel;
    asgSystUsers: TAdvStringGrid;
    pnlFilter: TPanel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    PnlJenisSaldo: TAdvPanel;
    cmbAktivasi: TComboBox;
    btnAdd: TAdvGlowButton;
    btnEdit: TAdvGlowButton;
    btnRemove: TAdvGlowButton;
    btnFilter: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    btnAktivasi: TAdvGlowButton;
    Label3: TLabel;
    Label4: TLabel;
    Shape3: TShape;
    Shape1: TShape;
    Gauge2: TGauge;
    chbSFooter: TAdvOfficeCheckBox;
    procedure btnCancelClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure asgSystUsersButtonClick(Sender: TObject; ACol,
      ARow: Integer);
    procedure asgSystUsersGetAlignment(Sender: TObject; ARow,
      ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure btnRemoveClick(Sender: TObject);
    procedure btnAddClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure asgSystUsersCheckBoxClick(Sender: TObject; ACol,
      ARow: Integer; State: Boolean);
    procedure asgSystUsersCanEditCell(Sender: TObject; ARow, ACol: Integer;
      var CanEdit: Boolean);
    procedure FormShow(Sender: TObject);
    procedure asgSystUsersMouseWheelDown(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure asgSystUsersMouseWheelUp(Sender: TObject; Shift: TShiftState;
      MousePos: TPoint; var Handled: Boolean);
    procedure chbSFooterClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure btnAktivasiClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure asgSystUsersGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgSystUsersGetCellPrintColor(Sender: TObject; ARow,
      ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    systUsers : TSystemUser_Arr;
    accessList : TStringList;
    AIdMenu: INteger;
    procedure initform;
    procedure setGrid;
    procedure loadData;
    procedure ArrangeColsize;
    { Private declarations }
  public
    procedure execute(AMenuId: Integer);
    { Public declarations }
  end;

var
  frmKelolaUser: TfrmKelolaUser;

implementation

uses ChangePassword, MainMenu, UConst, UGeneral, OracleConnection;

{$R *.dfm}

{ TkelolaSystUserFrm }

procedure TfrmKelolaUser.execute(AMenuId: Integer);
begin
  AIdMenu := AMenuId;
  initform;
  setGrid;
  loadData;
  Run(Self);
end;

procedure TfrmKelolaUser.loadData;
var i:integer;
begin
  TSystemUser.AccessLevelList(accessList);
  systUsers.Clear;
  if cmbAktivasi.ItemIndex = 0 then
    systUsers.FindOnDB('','',GlobalSystemUser.AccessLevel,bonone)
  else if cmbAktivasi.ItemIndex = 1 then
    systUsers.FindOnDB('','',GlobalSystemUser.AccessLevel,boTrue)
  else if cmbAktivasi.ItemIndex = 2 then
    systUsers.FindOnDB('','',GlobalSystemUser.AccessLevel,boFalse);
  asgSystUsers.RowCount := systUsers.Count + 1;
  asgSystUsers.ClearNormalCells;
  asgSystUsers.Cells[0,0] := 'No.';
  asgSystUsers.Cells[1,0] := 'Login ID';
  asgSystUsers.Cells[2,0] := 'Nama Login';
  asgSystUsers.Cells[3,0] := 'Bisa Tambah User';
  asgSystUsers.Cells[4,0] := 'Action';
  asgSystUsers.Cells[5,0] := 'IsDisable';
  
  for i := 0 to systUsers.Count-1 do begin
    asgSystUsers.Cells[1,i+1] := systUsers[i].UserId;
    asgSystUsers.Cells[2,i+1] := systUsers[i].UserName;
    if systUsers[i].IsTambahUser = 'T' then begin
      asgSystUsers.Cells[3,i+1] := 'Ya';
    end else asgSystUsers.Cells[3,i+1] := 'Tidak';
    asgSystUsers.AddButton(4,i+1,60,19,'Set Password',haCenter,vaCenter);
    if systUsers[i].TglNonAktif <> 0 then
      asgSystUsers.Dates[5,i+1] := systUsers[i].TglNonAktif;
  end;
  asgSystUsers.AutoNumberCol(0);
  ArrangeColsize;
end;

procedure TfrmKelolaUser.setGrid;
begin
  asgSystUsers.ColCount := 7;
  asgSystUsers.RowCount := 2;
  asgSystUsers.ClearNormalCells;
end;

procedure TfrmKelolaUser.btnCancelClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmKelolaUser.btnCetakClick(Sender: TObject);
begin
  myConnection.advPrint.Grid:= asgSystUsers;
  asgSystUsers.PrintSettings.TitleLines.Clear;
  asgSystUsers.PrintSettings.TitleLines.Add('');
  asgSystUsers.PrintSettings.TitleLines.Add('Rekap Kelola User');
  asgSystUsers.PrintSettings.TitleLines.Add('');
  asgSystUsers.ColumnSize.Stretch := False;
  asgSystUsers.ColCount := 5;
  SetingPrint(asgSystUsers);
  myConnection.advPrint.Execute;
  asgSystUsers.ColCount := 7;
  asgSystUsers.ColWidths[5]:=0;
  asgSystUsers.ColumnSize.Stretch := True;
end;

procedure TfrmKelolaUser.FormCreate(Sender: TObject);
begin
  accessList := TStringList.Create;
  systUsers := TSystemUser_Arr.Create;
end;

procedure TfrmKelolaUser.FormDestroy(Sender: TObject);
begin
  SystUsers.Destroy;
  accessList.Destroy;
end;

procedure TfrmKelolaUser.FormShow(Sender: TObject);
begin
  execute(0);
end;

procedure TfrmKelolaUser.initform;
begin
  cmbAktivasi.ItemIndex := 1;
  chbSFooter.Checked := false;
  asgSystUsers.SearchFooter.Visible := false;
end;

procedure TfrmKelolaUser.ArrangeColsize;
begin
  asgSystUsers.AutoSizeColumns(True,2);
  if GlobalSystemUser.AccessLevel = LEVEL_DEVELOPER then Exit;
  asgSystUsers.ColWidths[5] := 0;
end;

procedure TfrmKelolaUser.asgSystUsersButtonClick(Sender: TObject; ACol, ARow: Integer);
begin
  Application.CreateForm(TfrmChangePassword, frmChangePassword);
  frmChangePassword.OnClose := frmMainMenu.ChildFormSingle;
  frmChangePassword.Execute(asgSystUsers.Cells[1, ARow])
end;

procedure TfrmKelolaUser.asgSystUsersGetAlignment(Sender: TObject;
  ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow = 0 then  HAlign := taCenter
  else if acol in [0] then HAlign := taRightJustify;
  if ACol in [4,5] then
    HAlign := taCenter;
end;

procedure TfrmKelolaUser.asgSystUsersGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin

  if (ACol > 0) and (arow > 0) and (asgSystUsers.Cells[5, ARow] <> '') then
    AFont.Color := clRed
  else AFont.Color := clBlack;
end;

procedure TfrmKelolaUser.asgSystUsersGetCellPrintColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin

  if (ACol > 0) and (arow > 0) and (asgSystUsers.Cells[5, ARow] <> '') then
    AFont.Color := clRed
  else AFont.Color := clBlack;
end;

procedure TfrmKelolaUser.asgSystUsersMouseWheelDown(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColsize;
end;

procedure TfrmKelolaUser.asgSystUsersMouseWheelUp(Sender: TObject;
  Shift: TShiftState; MousePos: TPoint; var Handled: Boolean);
begin
  ArrangeColsize;
end;

procedure TfrmKelolaUser.btnRemoveClick(Sender: TObject);
var aUser :TSystemUser; i:integer; state:boolean;
begin
  if (not BisaHapus(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  if (asgSystUsers.Row > 0) and (asgSystUsers.Cells[1, asgSystUsers.Row] <> '') then begin
    if asgSystUsers.Cells[6,asgSystUsers.Row] = '' then begin
      Inform('Login ID "'+ asgSystUsers.Cells[1, asgSystUsers.Row] +'" status aktif, tidak bisa dihapus. '+#13+' Non aktifkan dahulu login ID tersebut.');
      Exit;
    end else if VarToDateTime(asgSystUsers.Cells[6,asgSystUsers.Row]) = 0 then begin
      Inform('Login ID "'+ asgSystUsers.Cells[1, asgSystUsers.Row] +'" status aktif, tidak bisa dihapus. '+#13+' Non aktifkan dahulu login ID tersebut.');
      Exit;
    end;
    if MessageDlg('Yakin login ID "'+ asgSystUsers.Cells[1, asgSystUsers.Row] +'" akan dihapus??', mtConfirmation, [mbYes, mbNo], 0) = mrYes then begin
      aUser := TSystemUser.Create;
      aUser.UserId := asgSystUsers.Cells[1, asgSystUsers.row];
      aUser.DeleteOnDB;
      aUser.Destroy;
      Alert('Sukses');
      if asgSystUsers.Row <> asgSystUsers.RowCount - 1 then begin
        for i := asgSystUsers.Row +1 to asgSystUsers.RowCount -1 do begin
          asgSystUsers.Cells[1,i-1] := asgSystUsers.Cells[1,i];
          asgSystUsers.Cells[2,i-1] := asgSystUsers.Cells[2,i];
          asgSystUsers.Cells[3,i-1] := asgSystUsers.Cells[3,i];
          asgSystUsers.GetCheckBoxState(4,i,state);
          asgSystUsers.SetCheckBoxState(4,i-i,state);
        end;
      end;
      asgSystUsers.RowCount := asgSystUsers.RowCount - 1;
      ArrangeColsize;
    end;
  end;
end;

procedure TfrmKelolaUser.btnResetClick(Sender: TObject);
begin
  setGrid;
end;

procedure TfrmKelolaUser.chbSFooterClick(Sender: TObject);
begin
  asgSystUsers.SearchFooter.Visible := chbSFooter.checked;
end;

procedure TfrmKelolaUser.btnAddClick(Sender: TObject);
begin
  if (not BisaNambah(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  Application.CreateForm(TfrmAddUser, frmAddUser);
  frmAddUser.OnClose := frmMainMenu.ChildFormSingle;
  frmAddUser.execute;
  loadData;
end;

procedure TfrmKelolaUser.btnAktivasiClick(Sender: TObject);
var User : TSystemUser;
    ARow : Integer;
    state : Boolean;
begin
  if Confirmed('Data akan di aktif/non aktif kan ?') then begin
    ARow := asgSystUsers.Row;
    User := TSystemUser.Create;
    state := asgSystUsers.Cells[5,arow] <> '';
    systUsers[ARow-1].is_disabled := state;

    User.Aktivasi_User(TrimAll(asgSystUsers.Cells[1,Arow]),State);
    if state = false then begin
      Alert('User ' + asgSystUsers.Cells[1,Arow] + ' sudah di-NonAktifkan');
      loadData;
    end
    else begin
      Alert('User ' + asgSystUsers.Cells[1,Arow] + ' sudah di-Aktifkan');
      loadData;
    end;
    User.Destroy;
  end;
end;

procedure TfrmKelolaUser.btnEditClick(Sender: TObject);
begin
  if (not BisaEdit(AIdMenu)) then begin
    Alert(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;
  if (asgSystUsers.Row > 0) and (asgSystUsers.Cells[1, asgSystUsers.Row] <> '') and (asgSystUsers.Cells[6, asgSystUsers.Row] = '') then begin
    Application.CreateForm(TfrmAddUser, frmAddUser);
    frmAddUser.OnClose := frmMainMenu.ChildFormSingle;
    frmAddUser.Execute(asgSystUsers.Cells[1,asgSystUsers.Row]);
    LoadData;
  end;
end;

procedure TfrmKelolaUser.btnEksporClick(Sender: TObject);
begin
  asgExportToExcell(asgSystUsers, myConnection.SaveToExcell);
end;

procedure TfrmKelolaUser.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilter,btnFilter, 68);
end;

procedure TfrmKelolaUser.btnLoadClick(Sender: TObject);
begin
  loadData;
end;

procedure TfrmKelolaUser.asgSystUsersCheckBoxClick(Sender: TObject;
  ACol, ARow: Integer; State: Boolean);
var User : TSystemUser;
begin
  User := TSystemUser.Create;
  systUsers[ARow-1].is_disabled := state;
  systUsers[ARow-1].UpdateOnDB;
  asgSystUsers.GetCheckBoxState(4,ARow,State);

  User.Aktivasi_User(TrimAll(asgSystUsers.Cells[1,Arow]),State);
  if state = false then begin
    Alert('User ' + asgSystUsers.Cells[1,Arow] + ' sudah di-NonAktifkan');
    loadData;
  end
  else begin
    Alert('User ' + asgSystUsers.Cells[1,Arow] + ' sudah di-Aktifkan');
    loadData;
  end;
  User.Destroy;
end;

procedure TfrmKelolaUser.asgSystUsersCanEditCell(Sender: TObject; ARow,
  ACol: Integer; var CanEdit: Boolean);
begin
  if Arow > 0 then
    CanEdit := (ACol in [4,5])
end;

end.
