unit AddUser;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, AdvEdit,OracleConnection,UConst,StrUtils,Math,Menus,
  Login, Grids, BaseGrid, AdvGrid, USystemMenu, AdvGlowButton, AdvObj,
  AdvOfficeButtons;

type
  TfrmAddUser = class(TForm)
    GroupBox2: TGroupBox;
    Label1: TLabel;
    Label5: TLabel;
    txtLoginName: TAdvEdit;
    txtLoginID: TAdvEdit;
    btnSave: TAdvGlowButton;
    cancelBtn: TAdvGlowButton;
    chbBisaTambahUser: TAdvOfficeCheckBox;
    procedure cancelBtnClick(Sender: TObject);
    procedure btnSaveClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cbAccessLevelChange(Sender: TObject);
  private
    { Private declarations }
    accessList : TStringList;
    permissionArr: TSystemMenu_Arr;
    menuArr : TSystemMenu_Arr;
    accessArr:TSystemAccess_Arr;
    aUser:TSystemUser;
    menuGroupList:TStringList;
    IsTambahLg,IsEdit : boolean;
    ListTransNonPosting : Tstringlist;
    ListHideRowDataCabang : Tstringlist;
    procedure clearForm;
    function validateForm :boolean;
    procedure ArrangeColSize();
    procedure SetListTransNonPost;
  public
    { Public declarations }
    procedure execute; overload;
    function execute(ALoginID: string): boolean; overload;
  end;

var
  frmAddUser: TfrmAddUser;

implementation

uses mainMenu, UGeneral, URecord, UEngine;
{$R *.dfm}



procedure TfrmAddUser.clearForm;
var i,j,k:integer;
begin
  txtLoginID.Text := '';
  txtLoginName.Text := '';
  chbBisaTambahUser.Checked := False;
  accessList.Clear;
  TSystemUser.AccessLevelList(accessList);
  for i := 0 to accessList.Count-1 do begin
    if GlobalSystemUser.AccessLevel <= StrToInt(accessList.Names[i]) then
      break;
  end;
  j := accessList.Count-i;
  for k:=1 to j do accessList.Delete(i);

  if IsTambahLg then
    txtLoginID.SetFocus;
end;

procedure TfrmAddUser.execute;
begin
  IsEdit := false;
  ListTransNonPosting := TStringList.Create;
  ListHideRowDataCabang := TStringList.Create;
  SetListTransNonPost;
  clearForm;                                            
  txtLoginID.Enabled := true;
  btnSave.Caption := '&Simpan';
  aUser.Reset;
  ArrangeColSize;
  Run(Self, True);
end;

function TfrmAddUser.execute(ALoginID: string): boolean;
var JmlCabang, i, CabSeqTemp : integer;
begin     
  IsEdit := true;
  ListTransNonPosting := TStringList.Create;
  ListHideRowDataCabang := TStringList.Create;
  SetListTransNonPost;
  clearForm;
  aUser.SelectInDB(ALoginID, '');
  txtLoginID.Text := aUser.UserId;
  txtLoginID.Enabled := false;
  txtLoginName.Text := aUser.UserName;
  chbBisaTambahUser.Checked := aUser.IsTambahUser = TRUE_STRING;

  txtLoginID.Enabled := false;
  btnSave.Caption := '&Update';
  ArrangeColSize;
  Result := (Run(self, true)=mrOK);
end;

procedure TfrmAddUser.cancelBtnClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

function TfrmAddUser.validateForm: boolean;
var msgString:string;
begin
  result := true;
  if txtLoginID.Text = '' then begin
    msgstring := msgString + 'Login ID Masih Kosong' +SLineBreak;
    result := false;
  end else begin
    if (txtLoginID.Enabled) AND TSystemUser.IsExistInDB(trim(txtLoginID.Text)) then begin
      msgstring := msgString + 'Login ID sudah dipakai user lain.' +SLineBreak;
      result := false;
    end;
  end;
  if txtLoginName.Text = '' then begin
    msgstring := msgString + 'Nama Login Masih Kosong' +SLineBreak;
    result := false;
  end;
  if (btnSave.Caption = '&Simpan') AND (TSystemUser.UserIDAlreadyRegistered(txtLoginID.Text)) then begin
    MessageDlg('Login ID ini sudah terdaftar sebagai system user',mtInformation,[mbok],0);
  end;
  
  if msgString <> '' then
    MessageDlg(msgString,mtError,[mbOK],0);
end;

procedure TfrmAddUser.btnSaveClick(Sender: TObject);
var systemAccess:TSystemAccess_Arr;i:integer;
    accessListset:TAccessListSet;state:boolean;
begin
  if validateForm = true then begin
    aUser.UserId := txtLoginID.Text;
    aUser.UserName := txtLoginName.Text;
    if chbBisaTambahUser.Checked then
      aUser.IsTambahUser := TRUE_STRING
    else aUser.IsTambahUser := FALSE_STRING;

    systemAccess:=TSystemAccess_Arr.Create(aUser);
    if btnSave.Caption = '&Simpan' then begin
      try
        myConnection.ADOConnection.BeginTrans;
        aUser.InsertOnDB;
        systemAccess.InsertOnDB;
        myConnection.ADOConnection.CommitTrans;
      except
        myConnection.ADOConnection.RollbackTrans;
        raise;
      end;
      MessageDlg('Sukses' + SLineBreak + 'User dapat mengatur password pada saat login pertama',mtInformation,[mbOK],0);
      if MessageDlg('Tambah user baru?',mtInformation,[mbYes,mbNo],0) = mrYes then begin
        IsTambahLg := True;
        clearForm
      end else
        cancelBtnClick(nil);
    end else begin
      try
        myconnection.ADOConnection.BeginTrans;
        aUser.UpdateOnDB;
        systemAccess.DeleteUpdateOnDB;
        myConnection.ADOConnection.CommitTrans;
      except
        myConnection.ADOConnection.RollbackTrans;
        raise;
      end;
      Alert('Sukses');
      cancelBtnClick(nil);
    end;
    systemAccess.Destroy;
  end;
end;

procedure TfrmAddUser.FormCreate(Sender: TObject);
begin
  accessList := TStringList.Create;
  permissionArr:=TSystemMenu_Arr.create;
  menuArr := TSystemMenu_Arr.create;
  accessArr :=TSystemAccess_Arr.Create(self);
  aUser:=TSystemUser.Create;
  menuGroupList := TStringList.Create;

end;

procedure TfrmAddUser.FormDestroy(Sender: TObject);
begin
  accessList.Destroy;
  permissionArr.destroy;
  menuArr.Destroy;
  accessArr.Destroy;
  aUser.Destroy;
  menuGroupList.Destroy;
end;

procedure TfrmAddUser.FormShow(Sender: TObject);
begin
  if txtLoginID.Enabled then txtLoginId.SetFocus else txtLoginName.SetFocus;
  ArrangeColSize;
end;


procedure TfrmAddUser.SetListTransNonPost;
begin
  ListTransNonPosting.Clear;
end;

procedure TfrmAddUser.cbAccessLevelChange(Sender: TObject);
begin
  ArrangeColSize;
end;

procedure TfrmAddUser.ArrangeColSize;
var i : integer;
begin
end;

end.
