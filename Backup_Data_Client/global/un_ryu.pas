unit un_ryu;

{Edit by chan 23/08/04}
interface
uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,Math,StrUtils,
  Dialogs, StdCtrls, AdvEdit, AdvGrid, DateUtils, UConst,OracleConnection,
  ExtCtrls,
  TmsAdvGridExcel, AdvGlowButton;

  function CheckControls(Sender: TWinControl): Boolean;
  function CorrectRound(x:Extended):Longint;
  function CekInteger(AString: string): boolean;
  function CekFloat(AString: string): boolean;
  function ReplaceSubStr(Input, Find, ReplaceWith: string): string;
  function CekIntegerFmt(AString: string): boolean;
  function CekIsValidDate(AInput: String): Boolean;

  function StrFmtToIntDef(AString: string; ADefault: integer): integer;
  function StrFmtToInt(AString: string): integer;
  function StrFmtToFloatDef(AString: string; ADefault: real): real;
  function StrFmtToFloat(AString: string): real;

  function CekFloatFmt(AString: string): boolean;
  function IntToStrFmt(AInteger: integer): string;
  function FloatToStrFmt(AFloat: real): string;
  function FloatToStrFmt1(AFloat: real): string;
  function FloatToStrFmt2(AFloat: real): string;
  function FloatToStrFmt3(AFloat: real): string;
  function FloatToStrFmt4(AFloat: real): string;

  function IntToStrCurrency(AInteger: integer): string;
  function FormatMoney(Value: Currency): String;
  function MoneyToIndonesianText(input: String): String;
  function EkstrakString(Input: string; pemisah: char; partisi: integer): string;
  function Confirmed(prompt: string): Boolean;
  function SaveConfirmed: Boolean;
  function CaptionPeriode(op1:TSQLOperator;Date1:TDate;Date2:TDate):string;
  function CaptionPeriode2(op1:TSQLOperator;Date1:TDate;Date2:TDate):string;
  function CaptionPeriode3(op1:TSQLOperator;Date1:TDate;Date2:TDate):string;
  { for Stallion project }
  function GetKwartal(Tanggal: TDate): String;
  function Kuadrat(n: real; Counter: integer): real;

  procedure Alert(AlertMsg: string);
  procedure Inform(Information: string);
  procedure FailSave;
  procedure SuccessSave;
  procedure FailUpdate;
  procedure SuccessUpdate;
  procedure AccessDenied;
  procedure ResetControls(Sender: TWinControl);
  procedure ResetGrid(Sender: TAdvStringGrid; DefaultRowCount,DefaultColCount,
                      DefaultFixedRows, DefaultFixedCols: integer);
  procedure Delay(Milisecond: Word);

  function UnQuotedStr(const S: string): string;
  function IsClear(AGrid: TAdvStringGrid; ARow, ACol: integer): boolean;

  procedure RemoveNameFromStringList(var Target:TStringList;NameToRemove:string);
  function SudahAdaDiGrid(grid:TAdvStringGrid;aCol,FromRow:integer;aNoDokumen:string):boolean;

  function getNamaBulan(aBulan:integer):string;
  function NewIfThen(AValue: Boolean; const ATrue: TBooleanOperator; const AFalse: TBooleanOperator = boNone): TBooleanOperator;

  //menentukan tanggal(dd/mm/yyyy) dengan memasukan bulan yang diinginkan
  function SetTDateByMonth(ADate: TDate; MonthOf: word): TDate;
  //utk menghapus row terakhir;
  procedure DeleteRowTerakhir(sender : TAdvStringGrid;LimitRow,RowDelete:integer);
  procedure DeleteRowKosong(sender : TAdvStringGrid;FixedRow,KolKriteria:integer;IsNumber,isFloatingFooter:boolean);

  //Melakukan penyimpanan ke dalam database
  function SaveToDB(SQL: string; InsertMode: boolean=true):boolean;

  //mengambil baris yang aktif atau yang akan digunakan
  //digunakan dalam forms Rekapitulasi/Kelola sewaktu edit insert data
  function GetRow(AGrid: TAdvStringGrid; InsertMode: boolean=true): integer;
  function InputNote: string;

  function ExecTransaction(SQL: string; InsertMode: boolean=true): boolean;
  function ExecTransaction2(SQL: string; InsertMode: boolean=true): boolean;
  function ExecTransaction3(SQL: string): boolean;

  function LastMonth(adate: TDate): Tdate;
  function IsTextExistInGrid(grid : TAdvStringGrid;col:integer;Text:string):boolean;
  function ConvertStrToFloat(AValue: string; ASeparator: char): Double;

  procedure SetCellFontColor(aGrid : TAdvStringGrid;FontColor:TColor;InitCol,Row:integer);
  procedure SetComboboxSize(Sender: TComboBox; NewWidth:integer);
  procedure SetFilterSize(Sender: TPanel; AButton: TButton; AShowSize:integer);

  procedure ExportGridToExcell(aGrid : TAdvStringGrid; aDialogs : TSaveDialog);
  procedure ExportGridToExcellNew(aGrid : TAdvStringGrid; aDialogs : TSaveDialog);
  function ExecDelete(SQL: string): boolean;
  function ExecAktivasi(SQL: string; isAktif: boolean=true): boolean;

  procedure SetPanelFilter(Sender: TObject; APenelFilter: TPanel; AHeight: integer);
  procedure SetPanelFilterGlowBttn(Sender: TObject; APenelFilter: TPanel; AHeight: integer);
  procedure LimitDecimal(Sender: TObject; aCol: Integer; var aKey: Char); overload;
  function IsNumberOnly(aText : String): boolean;  {-- cek angka semua nggak?}
  function IsChar13Exist(aText : String): boolean;  {-- cek apakah teks ada #13?}
  function IsNotSameBefore(var aBefore : String; aAfter : String):Boolean;
  function FindTextOnGrid(var aRow: Integer; aBeginRow: Integer; aSearchCol: Integer; aGrid: TAdvStringGrid; aFindText: String):Boolean;
  procedure SetRowFontColor(aMasterGrid: TAdvStringGrid; aFromCol, aRow: Integer; aColor: TColor);
  procedure SetRowFontStyle(aMasterGrid: TAdvStringGrid; aFromCol, aRow: Integer; aStyle: TFontStyle);
  procedure SetRowColor(aMasterGrid: TAdvStringGrid; aFromCol, aRow: Integer; aColor: TColor);

  procedure DoubleSize(Sender : TObject);
  procedure DoubleBack(Sender : TObject);
  procedure SetNumber(var AKey: Char);

  function AIsNumberExist(AFieldNomor, ATable, ANomorTrans: String; ATipe : string=''): Boolean;
  function JarakBulan(const ABulan1: TDate; const ABulan2: TDate): Integer;
  procedure RegionalSetting_Indonesia;
//  function AIsNumberExistPtg(AFieldNomor, ATable, ANomorTrans: String): Boolean;

implementation

uses Subroutines;

procedure RegionalSetting_Indonesia;
begin
  CurrencyString := 'Rp.';
  CurrencyFormat := 2; {Contoh: 'Rp. 1'}
  NegCurrFormat := 14; {Contoh: '(Rp. 1)'}
  CurrencyDecimals := 2;
  TimeAMString := 'AM';
  TimePMString := 'PM';
  ThousandSeparator := '.';
  DecimalSeparator := ',';
  DateSeparator := '/';
  TimeSeparator := ':';
  LongTimeFormat := 'hh:mm:ss';
  ShortTimeFormat := 'hh:mm';
  ShortDateFormat := 'dd/MM/yyyy';
  LongDateFormat := 'dd MMMM yyyy';

  LongDayNames[1] := 'Senin';
  LongDayNames[2] := 'Selasa';
  LongDayNames[3] := 'Rabu';
  LongDayNames[4] := 'Kamis';
  LongDayNames[5] := 'Jumat';
  LongDayNames[6] := 'Sabtu';
  LongDayNames[7] := 'Minggu';

  ShortDayNames[1] := 'Sen';
  ShortDayNames[2] := 'Sel';
  ShortDayNames[3] := 'Rab';
  ShortDayNames[4] := 'Kam';
  ShortDayNames[5] := 'Jum';
  ShortDayNames[6] := 'Sab';
  ShortDayNames[7] := 'Ming';

  ShortMonthNames[1] := 'Jan';
  ShortMonthNames[2] := 'Feb';
  ShortMonthNames[3] := 'Mar';
  ShortMonthNames[4] := 'Apr';
  ShortMonthNames[5] := 'Mei';
  ShortMonthNames[6] := 'Jun';
  ShortMonthNames[7] := 'Jul';
  ShortMonthNames[8] := 'Agt';
  ShortMonthNames[9] := 'Sep';
  ShortMonthNames[10] := 'Okt';
  ShortMonthNames[11] := 'Nov';
  ShortMonthNames[12] := 'Des';

  LongMonthNames[1] := 'Januari';
  LongMonthNames[2] := 'Februari';
  LongMonthNames[3] := 'Maret';
  LongMonthNames[4] := 'April';
  LongMonthNames[5] := 'Mei';
  LongMonthNames[6] := 'Juni';
  LongMonthNames[7] := 'Juli';
  LongMonthNames[8] := 'Agustus';
  LongMonthNames[9] := 'September';
  LongMonthNames[10] := 'Oktober';
  LongMonthNames[11] := 'November';
  LongMonthNames[12] := 'Desember';
end;

procedure ExportGridToExcell(aGrid : TAdvStringGrid; aDialogs : TSaveDialog);
begin
  if aDialogs.Execute then begin
    myConnection.AdvExcel.Options.ExportOverwrite  := omAlways;
    myConnection.AdvExcel.AdvStringGrid := aGrid;
    myConnection.AdvExcel.DateFormat := 'dd-MMM';
    myConnection.AdvExcel.GridStartRow := 0;
    myConnection.AdvExcel.GridStartCol := 0;
    if FileExists(aDialogs.FileName) then begin
      if Confirmed(aDialogs.FileName+' already exist.'+#13+'Do you want to replace it?') then begin
          DeleteFile(aDialogs.FileName);
          myConnection.AdvExcel.DateFormat := 'dd-MMM';
          myConnection.AdvExcel.XLSExport(aDialogs.FileName);
      end else exit;
    end else begin
      myConnection.AdvExcel.DateFormat := 'dd-MMM';
//      myConnection.AdvExcel.XLSExport(aDialogs.FileName);
      myConnection.AdvExcel.XLSExport(aDialogs.FileName);
    end;
  end;
end;

procedure ExportGridToExcellNew(aGrid : TAdvStringGrid; aDialogs : TSaveDialog);
begin
  if aDialogs.Execute then begin
    myConnection.AdvExcel.Options.ExportOverwrite  := omAlways;
    myConnection.AdvExcel.AdvStringGrid := aGrid;
    myConnection.AdvExcel.DateFormat := 'dd-MMM';
    myConnection.AdvExcel.GridStartRow := 0;
    myConnection.AdvExcel.GridStartCol := 0;
    if FileExists(aDialogs.FileName) then begin
      if Confirmed(aDialogs.FileName+' already exist.'+#13+'Do you want to replace it?') then begin
          DeleteFile(aDialogs.FileName);
          myConnection.AdvExcel.DateFormat := 'dd-MMM';
          aGrid.SaveToXLS(aDialogs.FileName);
//          myConnection.AdvExcel.XLSExport(aDialogs.FileName);
      end else exit;
    end else begin
      myConnection.AdvExcel.DateFormat := 'dd-MMM';
      aGrid.SaveToXLS(aDialogs.FileName);
//      myConnection.AdvExcel.XLSExport(aDialogs.FileName);
//      myConnection.AdvExcel.XLSExport(aDialogs.FileName);
    end;
  end;
end;


  function getNamaBulan(aBulan:integer):string;
  var listBulan : TStringList;
  begin
    listBulan := TStringList.Create;
    getMonths(listBulan);
    Result := listBulan.Values[IntToStr(aBulan)];
    listBulan.Free;
  end;

  function SudahAdaDiGrid(grid:TAdvStringGrid;aCol,FromRow:integer;aNoDokumen:string):boolean;
  var i:integer;
  begin
    Result := False;
    for i:= FromRow to grid.RowCount-1 do begin
      if grid.Cells[aCol,i] = aNoDokumen then begin
        Result := True;
        break;
      end;
    end;
  end;

  
  function CorrectRound(x:Extended):Longint;
  {untuk menghasilkan nilai round akurat tanpa mendekati nilai genap terdekat dari pembulatan}
  begin
    Result := trunc(x);
    if (Frac(x) >= 0.5) then
      Result := Result + 1;
  end;

  procedure RemoveNameFromStringList(var Target:TStringList;NameToRemove:string);
  var i:integer;
  begin
    for i:= 0 to Target.Count-1 do begin
      if UpperCase(Target.Names[i]) = UpperCase(NameToRemove) then begin
        Target.Delete(i);
        break;
      end;
    end;
  end;

  function CheckControls(Sender: TWinControl): Boolean;
  var i: integer;
  begin
    result:= True;
    for i:= 0 to Sender.ControlCount-1 do begin
      if Sender.Controls[i] is TAdvEdit then
        case TAdvEdit(Sender.Controls[i]).Tag of
          0:  if TAdvEdit(Sender.Controls[i]).Text = '' then begin
                result:= False;
                TAdvEdit(Sender.Controls[i]).SetFocus;
                ShowMessage('"'+TAdvEdit(Sender.Controls[i]).LabelCaption+'" belum diisi...');
                break;
              end;
        end;
      if Sender.Controls[i] is TComboBox then
        case TComboBox(Sender.Controls[i]).Tag of
          0:  if (TComboBox(Sender.Controls[i]).ItemIndex = -1) and
                 (TComboBox(Sender.Controls[i]).Visible) then begin
                result:= False;
                TComboBox(Sender.Controls[i]).SetFocus;
                ShowMessage('Harus dipilih salah satu...');
                break;
              end;
        end;
    end;
  end;

  procedure ResetControls(Sender: TWinControl);
  var i: integer;
  begin
    for i:= 0 to Sender.ControlCount-1 do begin
      if Sender.Controls[i] is TAdvEdit then begin
        if (TAdvEdit(Sender.Controls[i]).EditType = etString) or
           (TAdvEdit(Sender.Controls[i]).EditType = etMixedCase) or
           (TAdvEdit(Sender.Controls[i]).EditType = etLowerCase) or
           (TAdvEdit(Sender.Controls[i]).EditType = etUppercase) then
          if TAdvEdit(Sender.Controls[i]).Tag in [0,1] then
            TAdvEdit(Sender.Controls[i]).Text:= '';
        if (TAdvEdit(Sender.Controls[i]).EditType = etMoney) or
           (TAdvEdit(Sender.Controls[i]).EditType = etNumeric) or
           (TAdvEdit(Sender.Controls[i]).EditType = etFloat) then
          if TAdvEdit(Sender.Controls[i]).Tag in [0] then
            TAdvEdit(Sender.Controls[i]).Text:= '0'
//          else
//            TAdvEdit(Sender.Controls[i]).Text:= TAdvEdit(Sender.Controls[i]).EmptyText;
      end;
      if Sender.Controls[i] is TAdvMaskEdit then
        TAdvMaskEdit(Sender.Controls[i]).Clear;
      if Sender.Controls[i] is TComboBox then
        TComboBox(Sender.Controls[i]).ItemIndex:= 0;
      if Sender.Controls[i] is TMemo then
        TMemo(Sender.Controls[i]).Lines.Clear;
      if Sender.Controls[i] is TCheckBox then
        case TCheckBox(Sender.Controls[i]).Tag of
          0 : TCheckBox(Sender.Controls[i]).Checked:= False;
        end;
      if Sender.Controls[i] is TLabel then
        case TLabel(Sender.Controls[i]).Tag of
          1: TLabel(Sender.Controls[i]).Caption:= '';
        end;
    end;
  end;

  procedure ResetGrid(Sender: TAdvStringGrid; DefaultRowCount,DefaultColCount,
                      DefaultFixedRows, DefaultFixedCols: integer);
  begin
    Sender.BeginUpdate;
    Sender.ExpandAll;
    Sender.RowCount                 := DefaultRowCount;
    Sender.ClearNormalCells;
    Sender.AutoSize                 := False;
    Sender.ColumnSize.Stretch       := False;
    Sender.ColCount                 := DefaultColCount;
    Sender.FixedRows                := DefaultFixedRows;
    Sender.FixedFont.Style          := []; // add by chan
    Sender.FixedCols                := DefaultFixedCols;
    Sender.ColumnSize.StretchColumn := DefaultColCount-1;
    Sender.Rows[Sender.RowCount-1].Clear;
    Sender.RemoveAllNodes;
    Sender.ColumnSize.Stretch       := True;
    Sender.EndUpdate;
  end;

  procedure Delay(Milisecond: Word);
  var WaktuSekarang,WaktuBeres:cardinal;
  begin
    WaktuSekarang:=GetTickCount;
    WaktuBeres:=WaktuSekarang+milisecond;
      while WaktuBeres>GetTickCount do
        application.ProcessMessages;
  end;

  function GetKwartal(Tanggal: TDate): String;
  begin
    case MonthOf(Tanggal) of
      1 : result:= 'I/';
      2 : result:= 'II/';
      3 : result:= 'III/';
      4 : result:= 'IV/';
      5 : result:= 'V/';
      6 : result:= 'VI/';
      7 : result:= 'VII/';
      8 : result:= 'VIII/';
      9 : result:= 'IX/';
      10: result:= 'X/';
      11: result:= 'XI/';
      12: result:= 'XII/';
    end;
    result:= result + IntToStr(YearOf(Tanggal))
  end;

{-----------------------------------------------------------------------}
  function CekInteger(AString: string): boolean;
  begin
    try
      StrToInt(AString);
      Result:=true
    except
      on EConvertError do
        Result:=false;
    end;
  end;

  function CekFloat(AString: string): boolean;
  begin
    try
      StrToFloat(AString);
      Result:=true
    except
      on EConvertError do
        Result:=false;
    end;
  end;

  function ReplaceSubStr(Input, Find, ReplaceWith: string): string;
  { I.S. : Input, Find, dan ReplaceWith terdefinisi
    F.S. : menghasilkan string Input di mana substring Find diganti dengan
           substring ReplaceWith }
  var i: integer;
      Tmp: string;
  begin
    while (Pos(Find,Input)>0) do begin
      i:=Pos(Find,Input);
      Delete(Input,i,Length(Find));
      Tmp:=Copy(Input,i,Length(Input)-i+2);
      Delete(Input,i,Length(Input)-i+2);
      Input:=Input+ReplaceWith+Tmp;
    end;

    Result:=Input;
  end;

  function CekIntegerFmt(AString: string): boolean;
  begin
    Result := CekInteger(ReplaceSubStr(AString,ThousandSeparator,''));
  end;

  function CekFloatFmt(AString: string): boolean;
  begin
    Result := CekFloat(ReplaceSubStr(AString,ThousandSeparator,''));
  end;

  function CekIsValidDate(AInput: String): Boolean;
  var
    AHari, ABulan, ATahun: String;
    AHr, ABl, AThn: Word;
    isTahun, isHari, isBulan : Boolean;
    ATemp: Integer;
  begin
    AHari   := TrimAll(EkstrakString(AInput, '/',1));
    ABulan  := TrimAll(EkstrakString(AInput, '/',2));
    ATahun  := TrimAll(EkstrakString(AInput, '/',3));
    AThn := StrToIntDef(ATahun, 0); AHr := StrToIntDef(AHari, 0) ; ABl := StrToIntDef(ABulan, 0);
    ATemp := DaysInAMonth(AThn, ABl);
    isTahun := (AThn >= 1) and (AThn <= 9999);
    isBulan := (ABl >= 1) and (ABl <= 12);
    isHari  := (AHr >= 1) and (AHr <= ATemp);
    Result  := isTahun and isBulan and isHari;
    if (not Result) then begin
      if (not isHari) then
        Alert('Format hari masih salah')
      else if (not isBulan) then
        Alert('Format bulan masih salah')
      else if (not isTahun) then
        Alert('Format tahun masih salah');
    end;
  end;


  function StrFmtToIntDef(AString: string; ADefault: integer): integer;
  begin
    if (CekIntegerFmt(AString)) then
      Result := StrToInt(ReplaceSubStr(AString,ThousandSeparator,''))
    else
      Result := ADefault;
  end;

  function StrFmtToInt(AString: string): integer;
  begin
    Result := StrFmtToIntDef(AString, 0);
  end;

  function StrFmtToFloatDef(AString: string; ADefault: real): real;
  begin
    if (CekFloatFmt(AString)) then
      Result := StrToFloat(ReplaceSubStr(AString,ThousandSeparator,''))
    else
      Result := ADefault;
  end;

  function StrFmtToFloat(AString: string): real;
  begin
    Result := StrFmtToFloatDef(AString,0.00);
  end;

  function IntToStrFmt(AInteger: integer): string;
  begin
    Result := FormatFloat('#,##0',AInteger);
  end;

  function FloatToStrFmt(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0',AFloat);
  end;

  function FloatToStrFmt1(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0.0',AFloat);
  end;

  function FloatToStrFmt2(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0.00',AFloat);
  end;

  function FloatToStrFmt3(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0.000',AFloat);
  end;

  function FloatToStrFmt4(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0.0000',AFloat);
  end;

  function IntToStrCurrency(AInteger: integer): string;
  begin
    Result := CurrencyString+' '+FormatFloat('#,##0.00',AInteger);
  end;

  function FormatMoney(Value: Currency): String;
  begin
    Result := CurrencyString+' '+FormatCurr('#,##0.00',Value);
  end;

  function MoneyToIndonesianText(input: String): String;
  const vlstep : array [0..4] of string = (' ','ribu ','juta ','milyar ','trilyun ');
  var i,j,k,panjang,m : integer;
      stemp,stemp2,addstr : string;
      nl2,nltemp,qtemp : string;
      good,belas : boolean;
  begin
    good := false;
    for i:=1 to Length(input) do
    begin
      if (input[i] <> '0') then
         good := true;
      if (good) then
         nl2 := nl2 + input[i];
    end;
    if (length(nl2) > 15) then
       nltemp:=Copy(nl2,Length(nl2)-15+1,15)
    else nltemp := nl2;
    stemp2 := '';
    for i:=0 to 4 do
    begin
      k := Length(nltemp);
      if (k = 0) then
         break;
      if (k >= 3) then
         qtemp := Copy(nltemp,Length(nltemp)-2,3)
      else qtemp := nltemp;
      nltemp := Copy(nltemp,1,Length(nltemp)-3);
      stemp := '';
      belas := false;
      if (k >= 3) then
         panjang := 3
      else panjang:=k;
      m := 4-panjang;
      for j:=1 to panjang do
      begin
        addstr := '';
        if (qtemp[j] = '9') then
           addstr := 'sembilan ';
        if (qtemp[j] = '8') then
           addstr := 'delapan ';
        if (qtemp[j] = '7') then
           addstr := 'tujuh ';
        if (qtemp[j] = '6') then
           addstr := 'enam ';
        if (qtemp[j] = '5') then
           addstr := 'lima ';
        if (qtemp[j] = '4') then
           addstr := 'empat ';
        if (qtemp[j] = '3') then
           addstr := 'tiga ';
        if (qtemp[j] = '2') then
           addstr := 'dua ';
        if (qtemp[j] = '1') then
        begin
          case m of
          1,2: case m of
                1:addstr := 'se';
                2:belas := true;
               end;
          3: if (not belas) then
             begin
                if (i = 1) then
                begin
                  if (stemp = '') then
                     addstr := 'se'
                  else addstr := 'satu ' ;
                end else addstr := 'satu ';
             end else addstr := 'se';
          end;
        end;
        if (qtemp[j] = '0') then
           if (belas) then
           begin
             addstr := 'sepuluh ';
             belas := false;
           end;
        if ((addstr <> '') or (belas)) then
        begin
          case m of
          1: addstr := addstr + 'ratus ';
          2: if (not belas) then
                addstr := addstr+ 'puluh ';
          3: if (belas) then
                addstr := addstr+ 'belas ';
          end;
        end;
        stemp := stemp + addstr;
        inc(m);
      end;
      if (stemp <> '') then
         stemp2 := stemp + vlstep[i] + stemp2;
    end;
    result := stemp2+'rupiah';
    if (result <> '') then
       result[1] := upcase(result[1]);
  end;

  function EkstrakString(Input: string; pemisah: char; partisi: integer): string;
  { I.S. : Input = string yang akan di-extract, pemisah = karakter yang
           dipakai sebagai tanda pemisah, partisi = bagian ke berapa yang
           akan diambil, paling kiri adalah bagian ke-1
    F.S. : menghasilkan substring ke-partisi dari Input dengan batas pemisah }
  var Ctr, Posisi: integer;
      TmpString: string;
  begin
    TmpString:='';
    Ctr:=0;
    while (Ctr<partisi) do
    begin
      Posisi:=Pos(pemisah,Input);
      if (Posisi=0) then
        Posisi:=Length(Input)+1;
      TmpString:=Copy(Input,1,Posisi-1);
      Delete(Input,1,Posisi);
      Ctr:=Ctr+1;
    end;

    Result:=TmpString;
  end;

  function Confirmed(prompt: string): Boolean;
  begin
    Result := ( MessageDlg(prompt, mtConfirmation, [mbYes, mbNo], 0) = mrYes );
  end;

  function SaveConfirmed: Boolean;
  begin
    Result := Confirmed(MSG_SAVE_CONFIRMATION);
  end;

  procedure Alert(AlertMsg: string);
  begin
    MessageDlg(AlertMsg, mtWarning, [mbOK], 0);
  end;

  procedure Inform(Information: string);
  begin
    MessageDlg(Information, mtInformation, [mbOK], 0);
  end;

  procedure FailSave;
  begin
    ShowMessage(MSG_UNSUCCESS_SAVING);
  end;

  procedure SuccessSave;
  begin
    ShowMessage(MSG_SUCCESS_SAVING);
  end;

  procedure FailUpdate;
  begin
    ShowMessage(MSG_UNSUCCESS_UPDATE);
  end;

  procedure SuccessUpdate;
  begin
    ShowMessage(MSG_SUCCESS_UPDATE);
  end;

  procedure AccessDenied;
  begin
    ShowMessage(MSG_UNAUTHORISED_ACCESS);
  end;

  function CaptionPeriode(op1:TSQLOperator;Date1:TDate;Date2:TDate):string;
  var hasil : string;
  begin
     hasil := 'Periode Global';
     if (Date1 <> 0) and (Date2 <> 0) then
        hasil := 'Periode '+FormatDateTime(ShortDateFormat,Date1) + ' s/d '+FormatDateTime(ShortDateFormat,Date2)
     else if Date1<> 0 then begin
       {soGreaterThan,
       soGreaterThanEqualsTo,soEquals,soLessThan, soLessThanEqualsTo);}
        if (op1 = soEquals) or (Trunc(Date1) = Trunc(Date2)) then
          hasil := 'Per Tanggal '+ FormatDateTime(ShortDateFormat,Date1)
        else if (op1 = soGreaterThanEqualsTo) or (op1 = soGreaterThan) then
          hasil := 'Periode Dari '+ FormatDateTime(ShortDateFormat,Date1)+' Sampai Sekarang'
        else if (op1 = soLessThanEqualsTo) or (op1=soLessThan) then
          hasil := 'Periode Awal s/d '+ FormatDateTime(ShortDateFormat,Date1);
     end;
     Result := hasil;
  end;

  function CaptionPeriode2(op1:TSQLOperator;Date1:TDate;Date2:TDate):string;
  var hasil : string;
  begin
     hasil := 'Periode Global';
     if (Date1 <> 0) and (Date2 <> 0) then
        hasil := 'Periode Jatuh Tempo '+FormatDateTime(ShortDateFormat,Date1) + ' s/d '+FormatDateTime(ShortDateFormat,Date2)
     else if Date1<> 0 then begin
       {soGreaterThan,
       soGreaterThanEqualsTo,soEquals,soLessThan, soLessThanEqualsTo);}
        if (op1 = soEquals) or (Trunc(Date1) = Trunc(Date2)) then
          hasil := 'Per Tanggal Jatuh Tempo '+ FormatDateTime(ShortDateFormat,Date1)
        else if (op1 = soGreaterThanEqualsTo) or (op1 = soGreaterThan) then
          hasil := 'Periode Jatuh Tempo Dari '+ FormatDateTime(ShortDateFormat,Date1)+' Sampai Sekarang'
        else if (op1 = soLessThanEqualsTo) or (op1=soLessThan) then
          hasil := 'Periode Jatuh Tempo Awal s/d '+ FormatDateTime(ShortDateFormat,Date1);
     end;
     Result := hasil;
  end;

  function CaptionPeriode3(op1:TSQLOperator;Date1:TDate;Date2:TDate):string;
  var hasil : string;
  begin
     hasil := 'Periode Global';
     if (Date1 <> 0) and (Date2 <> 0) then
        hasil := 'Periode '+FormatDateTime(ShortDateFormat,Date1) + ' s/d '+FormatDateTime(ShortDateFormat,Date2)
     else if Date1<> 0 then begin
       {soGreaterThan,
       soGreaterThanEqualsTo,soEquals,soLessThan, soLessThanEqualsTo);}
        if (op1 = soEquals) or (Trunc(Date1) = Trunc(Date2)) then
          hasil := 'Per Tanggal '+ FormatDateTime(ShortDateFormat,Date1)
        else if (op1 = soGreaterThanEqualsTo) or (op1 = soGreaterThan) then
          hasil := 'Dari Tanggal '+ FormatDateTime(ShortDateFormat,Date1)+' Sampai Sekarang'
        else if (op1 = soLessThanEqualsTo) or (op1=soLessThan) then
          hasil := 'Periode Awal s/d '+ FormatDateTime(ShortDateFormat,Date1);
     end;
     Result := hasil;
  end;

  function Kuadrat(n: real; Counter: integer): real;
  var tmp: real; i: integer;
  begin
    tmp:= n;
    for i := 1  to Counter-1 do
      tmp:= tmp * n;
    Result:= tmp;
  end;

  function UnQuotedStr(const S: string): string;
  var
   // I: Integer;
    hasil: string;
  begin
    hasil:= s;
    Delete(hasil, 1,1);
    Delete(hasil, length(hasil),1);
    Result := hasil;
  end;

  function IsClear(AGrid: TAdvStringGrid; ARow, ACol: integer): boolean;
  begin
    result := False;
    if (AGrid.Cells[ACol,ARow] = '') then
      result := True;
  end;

  function NewIfThen(AValue: Boolean; const ATrue: TBooleanOperator; const AFalse: TBooleanOperator):  TBooleanOperator;
  begin
    if AValue then Result := ATrue
    else Result := AFalse;
  end;

  function SetTDateByMonth(ADate: TDate; MonthOf: word): TDate;
  var AYear, AMonth, ADay: word;
  begin
    DecodeDate(ADate,AYear,AMonth,ADay);
    Result:= EncodeDate(AYear,AMonth,ADay);
  end;
{-----------------------------------------------------------------------}

procedure DeleteRowTerakhir(sender : TAdvStringGrid;LimitRow,RowDelete:integer);
begin
  if sender.RowCount > LimitRow then
    sender.RowCount := sender.RowCount - RowDelete;
end;

procedure DeleteRowKosong(sender : TAdvStringGrid;FixedRow,KolKriteria:integer;IsNumber,isFloatingFooter:boolean);
var i,row:integer;Kriteria : string;
begin
  row := IfThen(isFloatingFooter,sender.RowCount-2,sender.RowCount-1);
  for i:= FixedRow to row do begin
    Kriteria := IfThen(IsNumber,IntToStr(0),'');
    If (sender.Cells[KolKriteria,i] = Kriteria)  then begin
       sender.RemoveRows(i,1);
       sender.Update;

    end;
  end;
end;


function SaveToDB(SQL: string; InsertMode: boolean):boolean;
begin
  try
    myConnection.BeginSQL;
    myConnection.ExecSQL(SQL);
    myConnection.EndSQL;
    Inform(IfThen(InsertMode, MSG_SUCCESS_SAVING, MSG_SUCCESS_UPDATE));
    Result:= True;
    except
      myConnection.UndoSQL;
      Result:= False;
      Inform(IfThen(InsertMode, MSG_UNSUCCESS_SAVING, MSG_UNSUCCESS_UPDATE));
  end;
end;

function GetRow(AGrid: TAdvStringGrid; InsertMode: boolean=true): integer;
begin
  if InsertMode then begin
    AGrid.AddRow;
    Result:= AGrid.RowCount-1;
  end else Result:= AGrid.Row;
end;

function InputNote: string;
begin
  Result:= InputBox('Notes', 'Entry Notes','');
end;

function ExecTransaction(SQL: string; InsertMode: boolean=true): boolean;
begin
  try
    myConnection.BeginSQL;
    myConnection.ExecSQL(SQL);
    myConnection.EndSQL;
    Inform(IfThen(InsertMode,MSG_SUCCESS_SAVING,MSG_SUCCESS_UPDATE));
    Result:= true;
    except
      myConnection.UndoSQL;
      Inform(IfThen(InsertMode,MSG_UNSUCCESS_SAVING,MSG_UNSUCCESS_UPDATE));
      Result:= false;
  end;

end;

function ExecTransaction2(SQL: string; InsertMode: boolean=true): boolean;
begin
  try
    myConnection.BeginSQL;
    myConnection.ExecSQL(SQL);
    myConnection.EndSQL;
    Result:= true;
    except
      myConnection.UndoSQL;
      Result:= false;
  end;
end;

function ExecTransaction3(SQL: string): boolean;
begin
  try
    myConnection.ExecSQL(SQL);
    Result:= true;
    except
//      raise;
      Result:= false;
  end;
end;

function LastMonth(adate: TDate): Tdate;
var d,m,y:Word;
    val : tdate;
begin
  DecodeDate(adate,y,m,d);
  if m>1 then
    m := m-1
  else begin
    m := 12;
    y := y-1;
  end;
  If m=2 then begin
    If (d=29) or (d=30) Or (d=31) then
      If y Mod 4 = 0 then
        d:=29
      else
        d:=28;
  end
  else
    If m<6 then begin
      If m mod 2 = 0 then
        d:=31
      else
        d:=30;
    end
    else begin
      If m mod 2 <> 0 then
        d:=31
      else
        d:=30;
    end;
  val := EncodeDate(y,m,d);
  if not IsValidDate(y,m,d) then begin
    Val := Endofthemonth(EncodeDate(y,m,1))
  end;
  Result:=val;
end;

function IsTextExistInGrid(grid : TAdvStringGrid;col:integer;Text:string):boolean;
var i:integer;
begin
  Result := False;
  for i:= 1 to grid.RowCount-1 do
    if UpperCase(grid.Cells[col,i]) = UpperCase(Text) then begin
      Result := True;
      Alert('Nilai sudah ada pada grid!');
      Break;
    end;
end;


procedure SetCellFontColor(aGrid : TAdvStringGrid;FontColor:TColor;InitCol,Row:integer);
var i:integer;
begin
  for i := InitCol to aGrid.ColCount - 1 do
    aGrid.FontColors[i,Row] := FontColor;
end;

procedure SetComboboxSize(Sender: TComboBox; NewWidth:integer);
begin
  Sender.Width:= NewWidth;
end;

procedure SetFilterSize(Sender: TPanel; AButton: TButton; AShowSize:integer);
begin
  Sender.Height:= IfThen(Sender.Height <> 0, 0, AShowSize);
  AButton.Caption:= IfThen(Sender.Height <> 0, 'Hide &Filter', 'Show &Filter');
end;

function ConvertStrToFloat(AValue: string; ASeparator: char): Double;
begin
  Result:= StrToFloat(ReplaceSubStr(AValue,ASeparator,''));
end;

function ExecDelete(SQL: string): boolean;
begin
  try
    myConnection.BeginSQL;
    myConnection.ExecSQL(SQL);
    myConnection.EndSQL;
   // Inform(MSG_SUCCESS_DELETING);
    Result:= true;
    except
      myConnection.UndoSQL;
    //  Inform(MSG_UNSUCCESS_DELETING);
      Result:= false;
  end;
end;

function ExecAktivasi(SQL: string; isAktif: boolean=true): boolean;
begin
  try
    myConnection.BeginSQL;
    myConnection.ExecSQL(SQL);
    myConnection.EndSQL;
    Inform(IfThen(isAktif, MSG_SUCCESS_AKTIVASI,MSG_SUCCESS_NOT_AKTIVASI));
    Result:= true;
    except
      myConnection.UndoSQL;
      Inform(IfThen(isAktif, MSG_UNSUCCESS_AKTIVASI, MSG_UNSUCCESS_NOT_AKTIVASI));
      Result:= false;
  end;
end;


  procedure SetPanelFilter(Sender: TObject; APenelFilter: TPanel; AHeight: integer);
  begin
  APenelFilter.Update;
    (Sender as TButton).Caption:= IfThen(APenelFilter.Height <> 0, 'Lihat &Filter', 'Tutup &Filter');
    APenelFilter.Height:= IfThen(APenelFilter.Height <> 0, 0, AHeight);
  end;

  procedure LimitDecimal(Sender: TObject; aCol: Integer; var aKey: Char);
  begin
    if Sender is TAdvStringGrid then
      if (Sender as TAdvStringGrid).Col = aCol then begin
        if aKey = ThousandSeparator then begin aKey := DecimalSeparator; Exit; end;
        if not (aKey in [DecimalSeparator, '-', '0'..'9',#8]) then aKey := #0;
      end;
    if Sender is TAdvEdit then
      if not (aKey in [DecimalSeparator,'0'..'9',#8]) then
        aKey := #0;
  end;


  procedure SetPanelFilterGlowBttn(Sender: TObject; APenelFilter: TPanel; AHeight: integer);
  begin
    APenelFilter.Update;
    (Sender as TAdvGlowButton).Caption:= IfThen(APenelFilter.Height <> 0, 'Lihat &Filter', 'Tutup &Filter');
    APenelFilter.Height:= IfThen(APenelFilter.Height <> 0, 0, AHeight);
  end;


  function IsNumberOnly(aText : String): boolean;
  var
    i : integer;
  begin
    Result := TRUE;
    if length(aText) <> 0 then
      for i := 1 to length(aText) do begin
        if not (aText[i] in ['1','2','3','4','5','6','7','8','9','0',',','.','-']) then begin
          Result := FALSE;
          BREAK;
        end;
      end;
  end;

  function IsChar13Exist(aText : String): boolean;
  var i: integer;
  begin
    Result := FALSE;
    if length(aText) <> 0 then
      for i:=0 to length(aText)-1 do begin
        if aText[i] = #13 then Result:= True;
      end;
  end;

  function IsNotSameBefore(var aBefore : String; aAfter : String):Boolean;
  begin
    Result := FALSE;
    if aBefore <> aAfter then begin
      Result  := TRUE;
    end;
    aBefore := aAfter;
  end;

function FindTextOnGrid(var aRow: Integer; aBeginRow: Integer; aSearchCol: Integer; aGrid: TAdvStringGrid; aFindText: String):Boolean;
var i, j: Integer;
    Found: Boolean;
    TmpStr, Check: String;
begin
  TmpStr:=UpperCase(aFindText);
  Found := False;
  for i := aBeginRow to aGrid.RowCount do begin
    for j := 1 to Length(aGrid.Cells[aSearchCol, i-1]) do begin
      Check := UpperCase(Copy(aGrid.Cells[aSearchCol, i-1],j,Length(TmpStr)));
      if Check = TmpStr then begin
        Found := True;
        aRow := i;
        if Found then Break;
      end;
    end;
    if Found then Break;
  end;
  Result:= Found;
end;

procedure SetRowFontColor(aMasterGrid: TAdvStringGrid; aFromCol, aRow: Integer; aColor: TColor);
var
  i : integer;
begin
  for i:=aFromCol to aMasterGrid.ColCount-1 do
    aMasterGrid.FontColors[i, aRow] := aColor;
end;

procedure SetRowFontStyle(aMasterGrid: TAdvStringGrid; aFromCol, aRow: Integer; aStyle: TFontStyle);
var
  i : integer;
begin
  for i:= aFromCol to aMasterGrid.ColCount-1 do
    aMasterGrid.FontStyles[i, aRow] := [aStyle];
end;

procedure SetRowColor(aMasterGrid: TAdvStringGrid; aFromCol, aRow: Integer; aColor: TColor);
var
  i : integer;
begin
  for i:= aFromCol to aMasterGrid.ColCount-1 do
    aMasterGrid.Colors[i, aRow] := aColor;
end;

//
procedure DoubleSize(Sender : TObject);
begin
  (Sender as TCombobox).Width := (Sender as TCombobox).Width * 2;
end;

procedure DoubleBack(Sender : TObject);
begin
  (Sender as TCombobox).Width := (Sender as TCombobox).Width div 2;
end;


procedure SetNumber(var AKey: Char);
begin
  if AKey = ThousandSeparator then begin
    AKey := DecimalSeparator;
    Exit;
  end;
  if not (AKey in [DecimalSeparator,'0'..'9',#8]) then AKey := #0;
end;

function AIsNumberExist(AFieldNomor, ATable, ANomorTrans, ATipe: String ): Boolean;
begin
//  Result := (getIntegerFromSQL('SELECT COUNT(*) FROM '+ATable+' WHERE '+AFieldNomor+' = '+FormatSQLString(ANomorTrans)) > 0);
  Result := (getIntegerFromSQL('SELECT COUNT(*) FROM '+ATable+' WHERE '+AFieldNomor+' = '+FormatSQLString(ANomorTrans) +ATipe) > 0);
end;

function JarakBulan(const ABulan1: TDate; const ABulan2: TDate): Integer;
begin
  if FormatDateTime('M', ABulan2) = '2' then
      Result := Trunc(MonthSpan(ABulan1, ABulan2+1))
  else Result := Trunc(MonthSpan(ABulan1, ABulan2));
//  Result := Trunc(MonthSpan(ABulan1, ABulan2));
end;

end.
