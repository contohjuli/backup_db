unit UGlobalTitle;
//jika akan build exe project ini maka tandain directif dibawah ini
//diawal dengan mengasih tanda $
{DEFINE GARUT}
{DEFINE BANDUNG}
{DEFINE GOMBONG}
{DEFINE PURWOKERTO}
{DEFINE PADALARANG}
{DEFINE BALIKPAPAN}
{DEFINE SAMARINDA}
{DEFINE BANJAR}
{DEFINE CIAWI}
{DEFINE PEKALONGAN}
{DEFINE MAGELANG}
{DEFINE BANJARBARU}
{DEFINE MEDAN}
{DEFINE BINJAI}
{DEFINE LAMPUNG}
{DEFINE BANDAR}
{DEFINE TEBINGTINGGI}

{$DEFINE BANDUNG_TAX}
{DEFINE PADALARANG_TAX}
{DEFINE GARUT_TAX}
{DEFINE GOMBONG_TAX}
{DEFINE PURWOKERTO_TAX}
{DEFINE CIAWI_TAX}
{DEFINE BANJAR_TAX}
{DEFINE MEDAN_TAX}
{DEFINE LAMPUNG_TAX}
{DEFINE BINJAI_TAX}



interface

uses
  OracleConnection, Subroutines, UConst,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ImgList, Menus, FthCtrls, FthSBar, ExtCtrls, ComCtrls,
  ToolWin, AdvGrid, AdvEdit, Gauges;//, WinXP; //, sTooledit; //sCustomComboEdit, 

const

{$IFDEF GARUT}
  APPLICATION_NAMES   = 'KRESNA UNGGAS MAJU';
  COMPANY_NAME        = 'KRESNA UNGGAS MAJU GARUT';
  APPLICATION_TITLE   = 'Kresna Unggas Maju Garut';
  ADRESS_1            = 'Jl. Raya Garut....Telp (0262)-(234723)';
  CITY                = 'GARUT';
  INISIAL_COMPANY     = 'KUM GARUT';
  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
  FIXED_COLOR         = clTeal;//$00C46200;
  FIXED_FONT_COLOR    = clWhite;
  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
  SELECTION_COLOR     = $0094E6FB;//$00B9F9BC;
  FILE_UDL            = '\connection.udl';
  PANEL_COLOR         = clTeal;
  EXE_NAME            = 'KUM_GARUT';
  DATA_DML_NAME       = 'dml_ktm_garut';
  IS_PUSAT            = TRUE_STRING;
  IS_KALIMANTAN       = FALSE_STRING;
  AUSER_DB_NAME       = 'ktm_garut_cabang';
{$ENDIF}

{$IFDEF BANDUNG}
  APPLICATION_NAMES = 'KRESNA UNGGAS MAKMUR';
  COMPANY_NAME      = 'KRESNA UNGGAS MAKMUR BANDUNG';
  APPLICATION_TITLE = 'Kresna Unggas Makmur Bandung';
  ADRESS_1          = 'Jl. Walini No 10 (022)-(7798210) Rancaekek - Bandung';
  CITY              = 'BANDUNG';
  INISIAL_COMPANY   = 'KUM BANDUNG';
  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
  FIXED_COLOR         = $0082C0AF;//$00C46200;
  FIXED_FONT_COLOR    = clBlack;
  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
  SELECTION_COLOR     = $00CAFFFF;//$0094E6FB;//$00B9F9BC;
  FILE_UDL            = '\ktm_bandung.udl';
  PANEL_COLOR         = $00447A63;
  EXE_NAME            = 'KUM_BANDUNG';
  DATA_DML_NAME       = 'dml_ktm_rancaekek';
  IS_PUSAT            = TRUE_STRING;
  IS_KALIMANTAN       = FALSE_STRING;
  AUSER_DB_NAME       = 'ktm_rancaekek_cabang';
{$ENDIF}


{$IFDEF GOMBONG}
  APPLICATION_NAMES = 'ISMAYA UNGGAS MAKMUR';
  COMPANY_NAME      = 'ISMAYA UNGGAS MAKMUR';
  APPLICATION_TITLE = 'Ismaya Unggas Makmur';
  ADRESS_1          = 'Jl. Raya Gombong...';
  CITY              = 'GOMBONG';
  INISIAL_COMPANY   = 'IUM GOMBONG';
  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
  FIXED_COLOR         = $0082C0AF;//$00C46200;
  FIXED_FONT_COLOR    = clBlack;
  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
  SELECTION_COLOR     = $0094E6FB;//$00B9F9BC;
  FILE_UDL            = '\connection.udl';
  DATA_DML_NAME       = 'dml_ktm_gombong';
  PANEL_COLOR         = $00447A63;
  IS_PUSAT            = TRUE_STRING;
  EXE_NAME            = 'IUM_GOMBONG';
  IS_KALIMANTAN       = FALSE_STRING;
  AUSER_DB_NAME       = 'ktm_gombong_cabang';
{$ENDIF}

{$IFDEF PADALARANG}
  APPLICATION_NAMES = 'KRESNA UNGGAS MANDIRI';
  COMPANY_NAME      = 'KRESNA UNGGAS MANDIRI';
  APPLICATION_TITLE = 'Kresna Unggas Mandiri';
  ADRESS_1          = 'Jl. Raya ...';
  CITY              = 'PADALARANG';
  INISIAL_COMPANY   = 'KUM PADALARANG';
  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
  FIXED_COLOR         = $0082C0AF;//$00C46200;
  FIXED_FONT_COLOR    = clBlack;
  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
  SELECTION_COLOR     = $0094E6FB;//$00B9F9BC;
  FILE_UDL            = '\connection.udl';
  DATA_DML_NAME       = 'dml_ktm_padalarang';
  PANEL_COLOR         = $00447A63;
  IS_PUSAT            = TRUE_STRING;
  EXE_NAME            = 'KUM_PADALARANG';
  IS_KALIMANTAN       = FALSE_STRING;
  AUSER_DB_NAME       = 'ktm_padalarang_cabang';
{$ENDIF}

{$IFDEF PURWOKERTO}
  APPLICATION_NAMES = 'ISMAYA UNGGAS JAYA';
  COMPANY_NAME      = 'ISMAYA UNGGAS JAYA';
  APPLICATION_TITLE = 'Ismaya Unggas Jaya';
  ADRESS_1          = 'Jl. Raya ...';
  CITY              = 'PURWOKERTO';
  INISIAL_COMPANY   = 'IUJ PURWOKERTO';
  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
  FIXED_COLOR         = $0082C0AF;//$00C46200;
  FIXED_FONT_COLOR    = clBlack;
  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
  SELECTION_COLOR     = $0094E6FB;//$00B9F9BC;
  FILE_UDL            = '\connection.udl';
  DATA_DML_NAME       = 'dml_ktm_purwokerto';
  PANEL_COLOR         = $00447A63;
  IS_PUSAT            = TRUE_STRING;
  EXE_NAME            = 'IUJ_PURWOKERTO';
  IS_KALIMANTAN       = FALSE_STRING;
  AUSER_DB_NAME       = 'iuj_purwokerto_cabang';
{$ENDIF}


{$IFDEF BANJAR}
  APPLICATION_NAMES = 'ISMAYA UNGGAS MAJU';
  COMPANY_NAME      = 'ISMAYA UNGGAS MAJU';
  APPLICATION_TITLE = 'Ismaya Unggas Maju - Banjar';
  ADRESS_1          = 'Jl. Raya ...';
  CITY              = 'BANJAR';
  INISIAL_COMPANY   = 'IUM BANJAR';
  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
  FIXED_COLOR         = $0082C0AF;//$00C46200;
  FIXED_FONT_COLOR    = clBlack;
  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
  SELECTION_COLOR     = $0094E6FB;//$00B9F9BC;
  FILE_UDL            = '\connection.udl';
  DATA_DML_NAME       = 'dml_ktm_banjar';
  PANEL_COLOR         = $00447A63;
  IS_PUSAT            = TRUE_STRING;
  EXE_NAME            = 'IUM_BANJAR';
  IS_KALIMANTAN       = FALSE_STRING;
  AUSER_DB_NAME       = 'ktm_banjar_cabang';
{$ENDIF}

{$IFDEF CIAWI}
  APPLICATION_NAMES = 'ISMAYA UNGGAS MANDIRI';
  COMPANY_NAME      = 'ISMAYA UNGGAS MANDIRI';
  APPLICATION_TITLE = 'Ismaya Unggas Mandiri - Ciawi';
  ADRESS_1          = 'Jl. Raya ...';
  CITY              = 'CIAWI';
  INISIAL_COMPANY   = 'IUM';
  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
  FIXED_COLOR         = $0082C0AF;//$00C46200;
  FIXED_FONT_COLOR    = clBlack;
  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
  SELECTION_COLOR     = $0094E6FB;//$00B9F9BC;
  FILE_UDL            = '\connection.udl';
  DATA_DML_NAME       = 'dml_ktm_ciawi';
  PANEL_COLOR         = $00447A63;
  IS_PUSAT            = TRUE_STRING;
  EXE_NAME            = 'IUM_CIAWI';
  IS_KALIMANTAN       = FALSE_STRING;
  AUSER_DB_NAME       = 'ktm_ciawi_cabang';
{$ENDIF}

{$IFDEF SAMARINDA}
  APPLICATION_NAMES = 'SAMARINDA MITRA LESTARI';
  COMPANY_NAME      = 'SAMARINDA MITRA LESTARI';
  APPLICATION_TITLE = 'Samarinda Mitra Lestari';
  ADRESS_1          = 'Jl. Raya ...';
  CITY              = 'SAMARINDA';
  INISIAL_COMPANY   = 'SMILE SAMARINDA';
  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
  FIXED_COLOR         = $0082C0AF;//$00C46200;
  FIXED_FONT_COLOR    = clBlack;
  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
  SELECTION_COLOR     = $0094E6FB;//$00B9F9BC;
  FILE_UDL            = '\connection.udl';
  DATA_DML_NAME       = 'dml_ktm_samarinda';
  PANEL_COLOR         = $00447A63;
  IS_PUSAT            = TRUE_STRING;
  EXE_NAME            = 'SMILE_SAMARINDA';
  IS_KALIMANTAN       = TRUE_STRING;
  AUSER_DB_NAME       = 'ktm_samarinda_cabang';
{$ENDIF}


{$IFDEF LAMPUNG}
//  APPLICATION_NAMES = 'SAMARINDA MITRA LESTARI';
//  COMPANY_NAME      = 'SAMARINDA MITRA LESTARI';
//  APPLICATION_TITLE = 'Samarinda Mitra Lestari';
//  ADRESS_1          = 'Jl. Raya ...';
//  CITY              = 'SAMARINDA';
//  INISIAL_COMPANY   = 'SMILE SAMARINDA';
//  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
//  FIXED_COLOR         = $0082C0AF;//$00C46200;
//  FIXED_FONT_COLOR    = clBlack;
//  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
//  SELECTION_COLOR     = $0094E6FB;//$00B9F9BC;
//  FILE_UDL            = '\connection.udl';
//  DATA_DML_NAME       = 'dml_ktm_samarinda';
//  PANEL_COLOR         = $00447A63;
//  IS_PUSAT            = TRUE_STRING;
//  EXE_NAME            = 'SMILE_SAMARINDA';
//  IS_KALIMANTAN       = TRUE_STRING;
  AUSER_DB_NAME       = 'ktm_lampung_cabang';
{$ENDIF}

{$IFDEF BANDAR}
  AUSER_DB_NAME       = 'ktm_bandarjaya_cabang';
{$ENDIF}

{$IFDEF PURBALINGGA}
  APPLICATION_NAMES = 'KURNIA_TERUS_JAYA';
  COMPANY_NAME      = 'KURNIA TERUS JAYA';
  APPLICATION_TITLE = 'Kurnia Terus Jaya';
  ADRESS_1          = 'Jl. Raya........';
  CITY              = 'PURBALINGGA';
  INISIAL_COMPANY   = 'KTJ';
{$ENDIF}
{$IFDEF TEGAL}
  APPLICATION_NAMES = 'KURNIA_JAYA_SANTOSA';
  COMPANY_NAME      = 'KURNIA JAYA SANTOSA';
  APPLICATION_TITLE = 'Kurnia Jaya Santosa';
  ADRESS_1          = 'Jl. Raya........';
  CITY              = 'TEGAL';
  INISIAL_COMPANY   = 'KJS';
{$ENDIF}
{$IFDEF PEKALONGAN}
  APPLICATION_NAMES = 'KURNIA_PEKALONGAN_JAYA';
  COMPANY_NAME      = 'KURNIA PEKALONGAN JAYA';
  APPLICATION_TITLE = 'Kurnia Pekalongan Jaya';
  ADRESS_1          = 'Jl. Raya........';
  CITY              = 'PEKALONGAN';
  INISIAL_COMPANY   = 'KPJ';
{$ENDIF}
{$IFDEF MAGELANG}
  APPLICATION_NAMES = 'KURNIA_TERUS_MAKMUR';
  COMPANY_NAME      = 'PT KURNIA TERUS MAKMUR';
  APPLICATION_TITLE = 'Kurnia Terus Makmur';
  ADRESS_1          = 'Jl. Raya........';
  CITY              = 'MAGELANG';
  INISIAL_COMPANY   = 'KTM';
{$ENDIF}


{$IFDEF BALIKPAPAN}
  APPLICATION_NAMES = 'PUJA BALIKPAPAN';
  COMPANY_NAME      = 'PUJA BALIKPAPAN';
  APPLICATION_TITLE = 'Puja Balikpapan';
  ADRESS_1          = 'Jl. Raya ...';
  CITY              = 'BALIKPAPAN';
  INISIAL_COMPANY   = 'TMS';
  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
  FIXED_COLOR         = $0082C0AF;//$00C46200;
  FIXED_FONT_COLOR    = clBlack;
  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
  SELECTION_COLOR     = $0094E6FB;//$00B9F9BC;
  FILE_UDL            = '\connection.udl';
  DATA_DML_NAME       = 'dml_ktm_banjarbaru';
  PANEL_COLOR         = $00447A63;
  IS_PUSAT            = TRUE_STRING;
  EXE_NAME            = 'PUJA_BALIKPAPAN';
  IS_KALIMANTAN       = TRUE_STRING;
  AUSER_DB_NAME       = 'ktm_balikpapan_cabang';
{$ENDIF}

{$IFDEF MEDAN}
  APPLICATION_NAMES = 'SIDO MAKMUR ABADI';
  COMPANY_NAME      = 'SIDO MAKMUR ABADI MEDAN';
  APPLICATION_TITLE = 'Sido Makmur Abadi Makmur';
  ADRESS_1          = 'Jl....';
  CITY              = 'MEDAN';
  INISIAL_COMPANY   = 'SIMA MEDAN';
//  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
//  FIXED_COLOR         = $0082C0AF;//$00C46200;
//  FIXED_FONT_COLOR    = clBlack;
//  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
//  SELECTION_COLOR     = $00CAFFFF;//$0094E6FB;//$00B9F9BC;
  FILE_UDL            = '\sima_medan.udl';
  PANEL_COLOR         = $00447A63;
  EXE_NAME            = 'SIMA_MEDAN';
  DATA_DML_NAME       = 'dml_ktm_medan';
//  IS_PUSAT            = TRUE_STRING;
  IS_KALIMANTAN       = FALSE_STRING;
  AUSER_DB_NAME       = 'ktm_medan_cabang';
  INISIAL_WIL         = 'MD';
{$ENDIF}

{$IFDEF BINJAI}
  APPLICATION_NAMES = 'PANDAWA UNGGAS MAKMUR';
  COMPANY_NAME      = 'PANDAWA UNGGAS MAKMUR';
  APPLICATION_TITLE = 'Pandawa Unggas Makmur';
  ADRESS_1          = 'Jl. Raya ...';
  CITY              = 'BINJAI';
  INISIAL_COMPANY   = 'PUM BINJAI';
//  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
//  FIXED_COLOR         = $0082C0AF;//$00C46200;
//  FIXED_FONT_COLOR    = clBlack;
//  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
//  SELECTION_COLOR     = $0094E6FB;//$00B9F9BC;
  FILE_UDL            = '\connection.udl';
  DATA_DML_NAME       = 'dml_ktm_binjai';
  PANEL_COLOR         = $00447A63;
//  IS_PUSAT            = TRUE_STRING;
  EXE_NAME            = 'PUM_BINJAI';
  IS_KALIMANTAN       = TRUE_STRING;
  AUSER_DB_NAME       = 'ktm_binjai_cabang';
  INISIAL_WIL         = 'SD';
{$ENDIF}

{$IFDEF TEBINGTINGGI}
  APPLICATION_NAMES = 'PANDAWA UNGGAS MAJU';
  COMPANY_NAME      = 'PANDAWA UNGGAS MAJU';
  APPLICATION_TITLE = 'Pandawa Unggas Maju';
  ADRESS_1          = 'Jl. Raya ...';
  CITY              = 'TEBING TINGGI';
  INISIAL_COMPANY   = 'PUM TEBING TINGGI';
//  FORM_BG_COLOR       = clSkyBlue;//$00FFBE7D;
//  FIXED_COLOR         = $0082C0AF;//$00C46200;
//  FIXED_FONT_COLOR    = clBlack;
//  PRIMARY_BAND_COLOR  = $00C8E8D1;//$00B9F9BC;
//  SELECTION_COLOR     = $0094E6FB;//$00B9F9BC;
  FILE_UDL            = '\connection.udl';
  DATA_DML_NAME       = 'dml_ktm_tebing_cabang';
  PANEL_COLOR         = $00447A63;
//  IS_PUSAT            = TRUE_STRING;
  EXE_NAME            = 'PUM_BINJAI';
  IS_KALIMANTAN       = TRUE_STRING;
  AUSER_DB_NAME       = 'ktm_tebingtinggi_cabang';
  INISIAL_WIL         = 'SD';
{$ENDIF}

{$IFDEF BANDUNG_TAX}
  AUSER_DB_NAME       = 'ktm_rancaekek_cabang_tax';
{$ENDIF}

{$IFDEF PADALARANG_TAX}
  AUSER_DB_NAME       = 'ktm_padalarang_cabang_tax';
{$ENDIF}

{$IFDEF GARUT_TAX}
  AUSER_DB_NAME       = 'ktm_garut_cabang_tax';
{$ENDIF}

{$IFDEF GOMBONG_TAX}
  AUSER_DB_NAME       = 'ktm_gombong_cabang_tax';
{$ENDIF}

{$IFDEF PURWOKERTO_TAX}
  AUSER_DB_NAME       = 'iuj_purwokerto_cabang_tax';
{$ENDIF}

{$IFDEF CIAWI_TAX}
  AUSER_DB_NAME       = 'ktm_ciawi_cabang_tax';
{$ENDIF}

{$IFDEF BANJAR_TAX}
  AUSER_DB_NAME       = 'ktm_banjar_cabang_tax';
{$ENDIF}

{$IFDEF MEDAN_TAX}
  AUSER_DB_NAME       = 'ktm_medan_cabang_tax';

{$ENDIF}

{$IFDEF LAMPUNG_TAX}
  AUSER_DB_NAME       = 'ktm_lampung_cabang_tax';
{$ENDIF}

{$IFDEF BINJAI_TAX}
  AUSER_DB_NAME       = 'ktm_binjai_cabang_tax';
{$ENDIF}


implementation

end.
