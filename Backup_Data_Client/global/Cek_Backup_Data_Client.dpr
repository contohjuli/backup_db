program Cek_Backup_Data_Client;

uses
  Forms,
  CekBackupData in 'CekBackupData.pas' {frmCekBackupData},
  Subroutines in 'Subroutines.pas',
  Login in 'Login.pas' {frmLogin},
  MainMenu in 'MainMenu.pas' {frmMainMenu},
  MasterProjectInput in '..\form\master\MasterProjectInput.pas' {frmMasterProjectInput},
  MasterProjectRekap in '..\form\master\MasterProjectRekap.pas' {frmMasterProjectRekap},
  ChangePassword in '..\user\ChangePassword.pas' {frmChangePassword},
  UEngine in '..\engine\UEngine.pas',
  UengineCloud in '..\engine\UengineCloud.pas',
  URecord in '..\engine\URecord.pas',
  UConst in 'UConst.pas',
  AddUser in '..\user\AddUser.pas' {frmAddUser},
  KelolaUser in '..\user\KelolaUser.pas' {frmKelolaUser},
  OracleConnection in 'OracleConnection.pas' {myConnection: TDataModule},
  USystemMenu in 'USystemMenu.pas',
  UCreateForm in '..\engine\UCreateForm.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.Title := 'SIMBADA';
  Application.CreateForm(TfrmMainMenu, frmMainMenu);
  Application.CreateForm(TfrmLogin, frmLogin);
  Application.CreateForm(TmyConnection, myConnection);
  frmLogin.Status;
  Application.Run;
end.
