program Backup_Data_Client;

uses
  Forms,
  Backupdata in 'Backupdata.pas' {Form1},
  OracleConnection in 'OracleConnection.pas' {myConnection: TDataModule},
  Subroutines in 'Subroutines.pas',
  UConst in 'UConst.pas',
  un_ryu in 'un_ryu.pas',
  UGlobalTitle in 'UGlobalTitle.pas',
  UengineCloud in '..\engine\UengineCloud.pas',
  URecord in '..\engine\URecord.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TmyConnection, myConnection);
  Application.CreateForm(TForm1, Form1);
  Application.Run;
end.
