unit MainMenu;
                    
interface
                                              
uses
  OracleConnection, UConst, strUtils, BaseGrid, Grids,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ImgList, Menus, FthCtrls, FthSBar, ExtCtrls, ComCtrls,
  ToolWin, AdvGrid, AdvEdit, Gauges, sPanel, ColCombo, IdTimeServer,
  IdBaseComponent, IdComponent, IdCustomTCPServer, IdDayTimeServer,
  AdvOfficeTabSet, AdvOfficeTabSetStylers, AdvMenus, AdvMenuStylers, AdvToolBar,
  AdvToolBarStylers, AdvAppStyler, ActnList, AdvPanel, AdvGlowButton,
  AdvNavBar, GradientLabel, TlHelp32, AdvDateTimePicker, AdvSplitter,
  AdvGlassButton, AdvShape, SUIImagePanel, SUIStatusBar, AdvPicture,
  AdvSmoothButton;

{Personalized Windows Message for StatusBar}
const
  WM_UpdateStatusBarMsg = WM_User + 8;
  WM_ResizeScrollBoxMsg = WM_User + 16;

type
  TfrmMainMenu = class(TForm)
    Panel1: TPanel;
    Timer1: TTimer;
    Panel2: TPanel;
    sttTanggal: TStaticText;
    sttUser: TStaticText;
    Label1: TLabel;
    Timer2: TTimer;
    AdvOfficeMDITabSet1: TAdvOfficeMDITabSet;
    AdvOfficeTabSetOfficeStyler1: TAdvOfficeTabSetOfficeStyler;
    AdvShape2: TAdvShape;
    suiPanel1: TsuiPanel;
    btnKelolaUser: TAdvGlowButton;
    btnMst_prj: TAdvGlowButton;
    btnLapProgressJob: TAdvGlowButton;
    AdvGlowButton1: TAdvGlowButton;
    btnLogoff: TAdvGlowButton;
    btntutup: TAdvGlowButton;
    StatusBar: TsuiStatusBar;

    procedure UpdateStatusBar;
    procedure ResizeScrollBox(var AMessage: TMessage); message WM_ResizeScrollBoxMsg;

    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure Timer1Timer(Sender: TObject);
    procedure FileLogoff1Click(Sender: TObject);
    procedure FileExit1Click(Sender: TObject);
    procedure ViewGoToMenu1Click(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
//    procedure GetPanel(AMenuID: integer);
    procedure FormCanResize(Sender: TObject; var NewWidth, NewHeight: Integer;
      var Resize: Boolean);
    procedure FormDestroy(Sender: TObject);
    procedure AdvToolBar1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);

    procedure OnLabelClick(Sender: TObject);
    procedure Houver(Sender: TObject);
    procedure UnHouver(Sender: TObject);
    procedure lblKeluarClick(Sender: TObject);
    procedure lblLogOffClick(Sender: TObject);
    procedure AdvOfficeMDITabSet1TabClose(Sender: TObject;
      TabIndex: Integer; var Allow: Boolean);
    procedure GradientLabel3Click(Sender: TObject);
    procedure lblPemakaianItemsClick(Sender: TObject);
    procedure lblSuratJalanClick(Sender: TObject);
    procedure gotomenu1Click(Sender: TObject);
    procedure logoff1Click(Sender: TObject);
    procedure btntutupClick(Sender: TObject);
    procedure btnLogoffClick(Sender: TObject);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure btnLapProgressJobClick(Sender: TObject);
    procedure btnMst_prjClick(Sender: TObject);
    procedure btnKelolaUserClick(Sender: TObject);

  protected
    { Private declarations }
    LastPanel: TPanel; LastPanelParent: TWinControl;
    LastPanelWidth, LastPanelHeight: integer;
    CurrentTopic: string;
    ListCurrentMenuId: TStringList;
    SudahBackup: Boolean;
    function AddSubMenuItem(aMenuItem:TMenuItem; aCaption:string;AImageIndex: integer; AShortcut: TShortcut; ATag: integer; AEnabled: boolean):TMenuItem;
    procedure AddMainMenuItem(AMenuItem: TMenuItem; ACaption: string; AImageIndex: integer; AShortcut: TShortcut; ATag: integer; AEnabled: boolean);
    procedure AddShortcutBarButton(APanelName: string; AButtonName: string; AImageIndex: integer; ATag: integer);
    procedure ChangeDisplay;
    procedure RefreshFavourite;
    //Cek Apakah Menu ini sudah show belum/
     function FormIsShow(AMenuId : Integer): Boolean;


    //procedure SelfUpdate;
//    procedure PutLastPanel(AShowDefault: boolean = true);
    procedure ChildFormClose(Sender: TObject; var Action: TCloseAction);


    procedure ChildKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure CloseAllChild;
    function Str2PChar(var S: String): PChar;
    function FindProcess(ProcName: String): boolean;
  public
    { Public declarations }
    procedure ChildFormSingle(Sender: TObject; var Action: TCloseAction);
    procedure OpenForm(TFrmClass : TFormClass; AShowModal: boolean = false);
  end;

var
  frmMainMenu: TfrmMainMenu;
  StatusBarText: string = '';
  ACurrMenuId: Integer;

procedure RearrangeForms;
procedure RearrangeFormsNew(AUserId: string);
procedure InitializeApplication;
procedure FinalizeApplication;

function Run(Sender: TForm; AIsDialog: Boolean=False): TModalResult;
{function CanAdd(Sender: TForm): boolean;
function BisaNambah(ID: integer): Boolean;
function BisaLiat(ID: integer): Boolean;
function BisaPrint(ID:integer):Boolean;
function CanEdit(Sender: TForm): boolean;
function CanLookup(Sender: TForm): boolean;
function CanBrowse(Sender: TForm): boolean;
function CanReview(Sender: TForm): boolean;
function CanRevers(Sender: TForm): boolean;
function CanPrint(Sender: TForm): boolean;   }
function BisaNambah(AMenuId: Integer): boolean;
function BisaEdit(AMenuId: Integer): boolean;
function BisaHapus(AMenuId: Integer): boolean;
function BisaLihatRekap(AMenuId: Integer): boolean;
function BisaLihatLaporan(AMenuId: Integer): boolean;
function BisaPrint(AMenuId: Integer): boolean;
function BisaEkspor(AMenuId: Integer): boolean;

procedure StatusBar(Text: string); overload; // set Text='' to clear
procedure StatusBar(Position: integer); overload; // set Position=-1 to hide
procedure ScrollBox(Width, Height: integer);

implementation

uses Login, USystemMenu,
  ugeneral, ChangePassword, CekBackupData, MasterProjectRekap, KelolaUser, 
  UCreateForm;
{$R *.dfm}

procedure RearrangeForms;

var i, j: integer; Form: TForm;
  Panel: TPanel; Control: TControl; MainPanelFound: boolean; NeedUpdate: boolean;
  AdvStringGrid: TAdvStringGrid; AdvEdit: TAdvEdit; Button: TButton; DateTimePicker: TDateTimePicker;
  ALabel : TLabel;
  //vComboJurnal: TColumnComboBox;
begin
  frmMainMenu.Caption := Application.Title;
//  GlobalSystemMenu_Arr.LoadFromDB();

  for i:=0 to Application.ComponentCount-1 do
  if (Application.Components[i] is TForm) then begin
    Form := (Application.Components[i] as TForm);
    if (Form.Name='frmMainMenu') or (Form.Name='frmAbout') or (Form.Name='frmLogin') then continue;
//    if Form.Name = 'frmPenerimaanTandaJadi' then
//      Alert('Cek');

    Form.BorderStyle := bsDialog;
    Form.Position := poDesktopCenter;
    Form.WindowState := wsNormal;
    //Form.Color := FORM_BG_COLOR;
    Form.Color  := $00F6E9D9;//clBtnFace; //$cccccc;
    MainPanelFound := false;

    for j:=0 to Form.ComponentCount-1 do
    if (Form.Components[j] is TAdvStringGrid) then begin
      AdvStringGrid := (Form.Components[j] as TAdvStringGrid);
      with AdvStringGrid do begin
        Color:=clWindow;
        HintColor:=clInfoBk;
        SelectionColor:=$0094E6FB;
        SelectionColorTo:=$001595EE;
        SelectionTextColor:=clBlack;
        URLColor:=clBlue;
        URLShow:=false;
        ScrollColor:=clNone;
        ActiveRowColor:=clInfoBk;
        ActiveRowShow:=false;
        ActiveCellShow:=false;
        Font.Color:=clWindowText;
        Font.Style:=[];
        Font.Size:=8;
        Font.Name:='Tahoma';
        ActiveCellColor:=$0094E6FB;
        ActiveCellColorTo:=$001595EE;
        ActiveCellFont.Color:=clWindowText;
        ActiveCellFont.Style:=[fsBold];
        ActiveCellFont.Size:=8;
        ActiveCellFont.Name:='Tahoma';
        FixedFont.Color:=clWindowText;
        FixedFont.Style:=[fsBold];
        FixedFont.Size:=8;
        FixedFont.Name:='Tahoma';
        Balloon.BackgroundColor:=clNone;
        Balloon.TextColor:=clNone;
        Balloon.Transparency:=0;
        Bands.PrimaryColor:=clInfoBk;
        Bands.SecondaryColor:=clWindow;
        Bands.Active:=false;
        SortSettings.IndexColor:=clYellow;
        FloatingFooter.Color:=clBtnFace;
        ControlLook.CheckSize:=15;
        ControlLook.Color:=clBlack;
        ControlLook.CommentColor:=clRed;
        ControlLook.ControlStyle:=csWinXP;
        ControlLook.FixedGradientFrom:=$00FCE1CB;
        ControlLook.FixedGradientTo:=$00E0A57D;
        ControlLook.RadioSize:=10;
        ControlLook.FlatButton:=false;
        ControlLook.ProgressBorderColor:=clGray;
        ControlLook.ProgressXP:=false;
        ControlLook.NoDisabledCheckRadioLook := True;
        ControlLook.NoDisabledButtonLook := True;        
        Look:=glXP;
        SearchFooter.Color:=$00FCE1CB;
        SearchFooter.ColorTo:=$00E0A57D;
        GridLineColor:=clSilver;
        Grouping.HeaderColor:=$00D68759;
        Grouping.HeaderColorTo:=$00933803;
        Grouping.HeaderTextColor:=clWhite;
        Grouping.HeaderUnderline:=true;
        Grouping.HeaderLineColor:=clBlue;
        Grouping.HeaderLineWidth:=2;
        Grouping.SummaryColor:=$00FADAC4;
        Grouping.SummaryColorTo:=$00F5BFA0;
        Grouping.SummaryTextColor:=clNone;
        Grouping.SummaryLine:=false;
        Grouping.SummaryLineColor:=clBlue;
        Grouping.SummaryLineWidth:=2;
        BackGround.Top:=0;
        BackGround.Left:=0;
//        BackGround.Display:=[bdTile];
        BackGround.Cells:=bcNormal;
        BackGround.Color:=clWindow;
        BackGround.ColorTo:=clBtnFace;
        Flat := False;
        WordWrap := False;
        SearchFooter.ShowClose := false;
//        con;
      end;
      if AdvStringGrid.Tag = -1 then begin
        AdvStringGrid.Bands.Active          := False;
        AdvStringGrid.SortSettings.Show     := False;
      end else begin
        AdvStringGrid.SortSettings.Show     := True;
        AdvStringGrid.SortSettings.Column   := AdvStringGrid.ColCount-1;
        AdvStringGrid.Bands.Active          := True;
        AdvStringGrid.SortSettings.AutoFormat:= True;
        AdvStringGrid.SortSettings.NormalCellsOnly := True;
      end;
      AdvStringGrid.Invalidate;
//      AdvStringGrid.FixedColor            := clSkyBlue; //FIXED_COLOR;
//      AdvStringGrid.FixedFont.Color       := clBlack;// FIXED_FONT_COLOR;
//      AdvStringGrid.FixedFont.Style       := [fsBold];//AdvStringGrid.FixedFont.Style + [fsBold];
//      AdvStringGrid.SelectionColor        := $00BBEEFF;
//      AdvStringGrid.ShowSelection         := True;
//      AdvStringGrid.SelectionTextColor    := $0051325F;
//      AdvStringGrid.Bands.PrimaryColor    := PRIMARY_BAND_COLOR;
//      AdvStringGrid.Flat                  := True;
//      AdvStringGrid.SizeWhileTyping.Width := True;
//      AdvStringGrid.FloatingFooter.Color  := clSkyBlue; //FIXED_COLOR;
//      AdvStringGrid.ControlLook.NoDisabledButtonLook := True;
//      AdvStringGrid.ControlLook.ControlStyle := csWinXP;
//      AdvStringGrid.Navigation.AutoComboDropSize := True;
//      AdvStringGrid.Options := [goFixedVertline, goFixedHorzline, goVertline,
//                                goHorzline, goDrawFocusSelected, goColSizing];
//      AdvStringGrid.MouseActions.AutoSizeColOnDblClick := False;
//      AdvStringGrid.VAlignment:= vtaCenter;
        //AdvStringGrid.AutoNumberCol(0);


    end else if (Form.Components[j] is TAdvEdit) then begin
      AdvEdit := (Form.Components[j] as TAdvEdit);
      AdvEdit.DisabledColor := AdvEdit.Color;
      //case AdvEdit.EditType of
        //etMoney:
        //etFloat:
        //etNumeric:
      //end;
    end else if (Form.Components[j] is TDateTimePicker) then begin
      DateTimePicker := (Form.Components[j] as TDateTimePicker);
      DateTimePicker.Format := '';
      if DateTimePicker.Kind=dtkDate then begin
        DateTimePicker.DateMode := dmComboBox;
        DateTimePicker.DateFormat := dfShort;
        DateTimePicker.Format := 'dd/MM/yyyy';
      end;
//    end else if (Form.Components[j] is TColumnComboBox) then begin
//      vComboJurnal := (Form.Components[j] as TColumnComboBox);
////      vComboJurnal.Visible := JENIS_JURNAL <> UConst.IS_JURNAL_BULANAN;
    end else if (Form.Components[j] is TButton) then begin
      Button := (Form.Components[j] as TButton);
      if (Button.Name='btnCancel') then
        Button.Cancel := true
      // handy
      else if (Button.Name='btnFilter') then
        Button.Visible  := False
      else if (UpperCase(Button.Name)='BTNPRINT') or (UpperCase(Button.Name)='BTNCETAK') or (UpperCase(Button.Name)='BTNPRIN')then
        Button.Caption  := '&Print'
      else if (UpperCase(Button.Name)='BTNEKSPOR') or (UpperCase(Button.Name)='BTNEKSPORT') or (UpperCase(Button.Name)='BTNEXCEL')then
        Button.Caption  := '&Export';

    end else if (Form.Components[j] is TPanel) then begin
      Panel := (Form.Components[j] as TPanel);

      if (UpperCase(Panel.Name)='MAINPANEL') and (Panel.Name<>'MainPanel') then begin
        Panel.Name := 'MainPanel';
        //Panel.Update;
        //Panel.Caption := 'Silahkan tunggu sebentar....';


      end;
      Panel.ParentBackground := True;
      Panel.ParentColor := True;
      if (Panel.Name='MainPanel') then
        MainPanelFound := true
      else Panel.Caption := '';
        //
    end else if (Form.Components[j] is TLabel) then begin
      ALabel := (Form.Components[j] as TLabel);
    	if ALabel.Tag = 0 then
        ALabel.Transparent := True
      else ALabel.Transparent:= False;
      if UpperCase(ALabel.Name) = 'LBLJURNAL' then
//        ALabel.Visible := JENIS_JURNAL <> UConst.IS_JURNAL_BULANAN;
      ALabel.ParentColor := True;
      ALabel.Font.Color := clBlack;
    end;

    NeedUpdate := false;
    for j:=0 to GlobalSystemMenu_Arr.Count-1 do
      if (UpperCase(GlobalSystemMenu_Arr[j].FormName)=UpperCase(Form.Name)) and
        (GlobalSystemMenu_Arr[j].FormType in [ftLookup,ftBrowse]) then
        NeedUpdate := true;

    if (NeedUpdate) and (not MainPanelFound) then begin
      Panel := TPanel.Create(Form);
      Panel.Name := 'MainPanel';
      Panel.Width := Form.ClientWidth;
      Panel.Height := Form.ClientHeight;
      Panel.Caption := '';
      //Panel.Color := FORM_BG_COLOR;
      Panel.BevelOuter := bvNone;
      for j:=0 to Form.ComponentCount-1 do
      if (Form.Components[j] is TControl) then begin
        Control := (Form.Components[j] as TControl);
        if (Control<>Panel) and (Control.Parent=Form) then
          Control.Parent := Panel;
        if (Control is TAdvStringGrid) then begin
          AdvStringGrid := (Control as TAdvStringGrid);
          AdvStringGrid.Anchors := [akLeft,akTop,akRight,akBottom];
        end else begin
          if (Control.Left>Panel.Width div 2) then
            Control.Anchors := Control.Anchors - [akLeft] + [akRight];
          if (Control.Top>Panel.Height div 2) then
            Control.Anchors := Control.Anchors - [akTop] + [akBottom];
        end;
      end;
      Panel.Parent := Form;
      Panel.Align := alClient;
    end;
  end;
end;

procedure RearrangeFormsNew(AUserId: string);
var i, j: integer; Form: TForm;
  Panel: TPanel; Control: TControl; MainPanelFound: boolean; NeedUpdate: boolean;
  AdvStringGrid: TAdvStringGrid; AdvEdit: TAdvEdit; Button: TButton; DateTimePicker: TDateTimePicker;
  ALabel : TLabel;
  //vComboJurnal: TColumnComboBox;
begin
  frmMainMenu.Caption := Application.Title;
//  GlobalSystemMenu_Arr.LoadFromDB(Puma_Pembelian,0);

  for i:=0 to Application.ComponentCount-1 do
  if (Application.Components[i] is TForm) then begin
    Form := (Application.Components[i] as TForm);
    if (Form.Name='frmMainMenu') or (Form.Name='frmAbout') or (Form.Name='frmLogin') then continue;
//    if Form.Name = 'frmPenerimaanTandaJadi' then
//      Alert('Cek');

    Form.BorderStyle := bsDialog;
    Form.Position := poDesktopCenter;
    Form.WindowState := wsNormal;
    //Form.Color := FORM_BG_COLOR;
    Form.Color  := $00F6E9D9;//clBtnFace; //$cccccc;
    MainPanelFound := false;

    for j:=0 to Form.ComponentCount-1 do
    if (Form.Components[j] is TAdvStringGrid) then begin
      AdvStringGrid := (Form.Components[j] as TAdvStringGrid);
      with AdvStringGrid do begin
        Color:=clWindow;
        HintColor:=clInfoBk;
        SelectionColor:=$0094E6FB;
        SelectionColorTo:=$001595EE;
        SelectionTextColor:=clBlack;
        URLColor:=clBlue;
        URLShow:=false;
        ScrollColor:=clNone;
        ActiveRowColor:=clInfoBk;
        ActiveRowShow:=false;
        ActiveCellShow:=false;
        Font.Color:=clWindowText;
        Font.Style:=[];
        Font.Size:=8;
        Font.Name:='Tahoma';
        ActiveCellColor:=$0094E6FB;
        ActiveCellColorTo:=$001595EE;
        ActiveCellFont.Color:=clWindowText;
        ActiveCellFont.Style:=[fsBold];
        ActiveCellFont.Size:=8;
        ActiveCellFont.Name:='Tahoma';                         
        FixedFont.Color:=clWindowText;
        FixedFont.Style:=[fsBold];
        FixedFont.Size:=8;
        FixedFont.Name:='Tahoma';
        Balloon.BackgroundColor:=clNone;
        Balloon.TextColor:=clNone;
        Balloon.Transparency:=0;
        Bands.PrimaryColor:=clInfoBk;
        Bands.SecondaryColor:=clWindow;
        Bands.Active:=false;
        SortSettings.IndexColor:=clYellow;
        FloatingFooter.Color:=clBtnFace;
        ControlLook.CheckSize:=15;
        ControlLook.Color:=clBlack;
        ControlLook.CommentColor:=clRed;
        ControlLook.ControlStyle:=csWinXP;
        ControlLook.FixedGradientFrom:=$00FCE1CB;
        ControlLook.FixedGradientTo:=$00E0A57D;
        ControlLook.RadioSize:=10;
        ControlLook.FlatButton:=false;
        ControlLook.ProgressBorderColor:=clGray;
        ControlLook.ProgressXP:=false;
        Look:=glXP;
        SearchFooter.Color:=$00FCE1CB;
        SearchFooter.ColorTo:=$00E0A57D;
        GridLineColor:=clSilver;
        Grouping.HeaderColor:=$00D68759;
        Grouping.HeaderColorTo:=$00933803;
        Grouping.HeaderTextColor:=clWhite;
        Grouping.HeaderUnderline:=true;
        Grouping.HeaderLineColor:=clBlue;
        Grouping.HeaderLineWidth:=2;
        Grouping.SummaryColor:=$00FADAC4;
        Grouping.SummaryColorTo:=$00F5BFA0;
        Grouping.SummaryTextColor:=clNone;
        Grouping.SummaryLine:=false;
        Grouping.SummaryLineColor:=clBlue;
        Grouping.SummaryLineWidth:=2;
        BackGround.Top:=0;
        BackGround.Left:=0;
//        BackGround.Display:=[bdTile];
        BackGround.Cells:=bcNormal;
        BackGround.Color:=clWindow;
        BackGround.ColorTo:=clBtnFace;
      end;
//      AdvStringGrid.FixedColor            := clSkyBlue; //FIXED_COLOR;
//      AdvStringGrid.FixedFont.Color       := clBlack;// FIXED_FONT_COLOR;
//      AdvStringGrid.FixedFont.Style       := [fsBold];//AdvStringGrid.FixedFont.Style + [fsBold];
//      AdvStringGrid.SelectionColor        := $00BBEEFF;
//      AdvStringGrid.ShowSelection         := True;
//      AdvStringGrid.SelectionTextColor    := $0051325F;
//      AdvStringGrid.Bands.PrimaryColor    := PRIMARY_BAND_COLOR;
//      AdvStringGrid.Flat                  := True;
//      AdvStringGrid.SizeWhileTyping.Width := True;
//      AdvStringGrid.FloatingFooter.Color  := clSkyBlue; //FIXED_COLOR;
//      AdvStringGrid.ControlLook.NoDisabledButtonLook := True;
//      AdvStringGrid.ControlLook.ControlStyle := csWinXP;
//      AdvStringGrid.Navigation.AutoComboDropSize := True;
//      AdvStringGrid.Options := [goFixedVertline, goFixedHorzline, goVertline,
//                                goHorzline, goDrawFocusSelected, goColSizing];
//      AdvStringGrid.MouseActions.AutoSizeColOnDblClick := False;
//      AdvStringGrid.VAlignment:= vtaCenter;
        //AdvStringGrid.AutoNumberCol(0);

      if AdvStringGrid.Tag = -1 then begin
        AdvStringGrid.Bands.Active          := False;
        AdvStringGrid.SortSettings.Show     := False;
      end else begin
        AdvStringGrid.SortSettings.Show     := True;
        AdvStringGrid.SortSettings.Column   := AdvStringGrid.ColCount-1;
        AdvStringGrid.Bands.Active          := True;
        AdvStringGrid.SortSettings.AutoFormat:= True;
        AdvStringGrid.SortSettings.NormalCellsOnly := True;
      end;
      AdvStringGrid.Invalidate;
    end else if (Form.Components[j] is TAdvEdit) then begin
      AdvEdit := (Form.Components[j] as TAdvEdit);
      AdvEdit.DisabledColor := AdvEdit.Color;
      //case AdvEdit.EditType of
        //etMoney:
        //etFloat:
        //etNumeric:
      //end;
    end else if (Form.Components[j] is TDateTimePicker) then begin
      DateTimePicker := (Form.Components[j] as TDateTimePicker);
      DateTimePicker.Format := '';
      if DateTimePicker.Kind=dtkDate then begin
        DateTimePicker.DateMode := dmComboBox;
        DateTimePicker.DateFormat := dfShort;
        DateTimePicker.Format := 'dd/MM/yyyy';
      end;
//    end else if (Form.Components[j] is TColumnComboBox) then begin
//      vComboJurnal := (Form.Components[j] as TColumnComboBox);
//      vComboJurnal.Visible := JENIS_JURNAL <> UConst.IS_JURNAL_BULANAN;
    end else if (Form.Components[j] is TButton) then begin
      Button := (Form.Components[j] as TButton);
      if (Button.Name='btnCancel') then
        Button.Cancel := true
      // handy
      else if (Button.Name='btnFilter') then
        Button.Visible  := False
      else if (UpperCase(Button.Name)='BTNPRINT') or (UpperCase(Button.Name)='BTNCETAK') or (UpperCase(Button.Name)='BTNPRIN')then
        Button.Caption  := '&Print'
      else if (UpperCase(Button.Name)='BTNEKSPOR') or (UpperCase(Button.Name)='BTNEKSPORT') or (UpperCase(Button.Name)='BTNEXCEL')then
        Button.Caption  := '&Export';

    end else if (Form.Components[j] is TPanel) then begin
      Panel := (Form.Components[j] as TPanel);

      if (UpperCase(Panel.Name)='MAINPANEL') and (Panel.Name<>'MainPanel') then begin
        Panel.Name := 'MainPanel';
        //Panel.Update;
        //Panel.Caption := 'Silahkan tunggu sebentar....';


      end;
      Panel.ParentBackground := True;
      Panel.ParentColor := True;
      if (Panel.Name='MainPanel') then
        MainPanelFound := true
      else Panel.Caption := '';
        //
    end else if (Form.Components[j] is TLabel) then begin
      ALabel := (Form.Components[j] as TLabel);
    	if ALabel.Tag = 0 then
        ALabel.Transparent := True
      else ALabel.Transparent:= False;
      if UpperCase(ALabel.Name) = 'LBLJURNAL' then
//        ALabel.Visible := JENIS_JURNAL <> UConst.IS_JURNAL_BULANAN;
      ALabel.ParentColor := True;
      ALabel.Font.Color := clBlack;
    end;

    NeedUpdate := false;
    for j:=0 to GlobalSystemMenu_Arr.Count-1 do
      if (UpperCase(GlobalSystemMenu_Arr[j].FormName)=UpperCase(Form.Name)) and
        (GlobalSystemMenu_Arr[j].FormType in [ftLookup,ftBrowse]) then
        NeedUpdate := true;

    if (NeedUpdate) and (not MainPanelFound) then begin
      Panel := TPanel.Create(Form);
      Panel.Name := 'MainPanel';
      Panel.Width := Form.ClientWidth;
      Panel.Height := Form.ClientHeight;
      Panel.Caption := '';
      //Panel.Color := FORM_BG_COLOR;
      Panel.BevelOuter := bvNone;
      for j:=0 to Form.ComponentCount-1 do
      if (Form.Components[j] is TControl) then begin
        Control := (Form.Components[j] as TControl);
        if (Control<>Panel) and (Control.Parent=Form) then
          Control.Parent := Panel;
        if (Control is TAdvStringGrid) then begin
          AdvStringGrid := (Control as TAdvStringGrid);
          AdvStringGrid.Anchors := [akLeft,akTop,akRight,akBottom];
        end else begin
          if (Control.Left>Panel.Width div 2) then
            Control.Anchors := Control.Anchors - [akLeft] + [akRight];
          if (Control.Top>Panel.Height div 2) then
            Control.Anchors := Control.Anchors - [akTop] + [akBottom];
        end;
      end;
      Panel.Parent := Form;
      Panel.Align := alClient;
    end;
  end;                                  
end;

procedure InitializeApplication;
begin
 Application.Name := 'Art_audio';
 Application.Title := 'ART AUDIO';
 Application.HelpFile := 'art_audio.chm';
end;

procedure FinalizeApplication;
begin
  GlobalSystemUser.SystemAccess_Arr.SaveUsageCountOnDB;
  GlobalSystemUser.SystemAccess_Arr.Clear;
end;

function Run(Sender: TForm; AIsDialog: Boolean): TModalResult;
var X: TControl;
begin
  RearrangeFormChild(Sender);
  if Sender.FormStyle = fsMDIChild then begin
    Sender.WindowState := wsMaximized;
  end else begin
    X := TControl(Sender.FindComponent('MainPanel'));
    if (X<>nil) and (X.Parent<>Sender) then
      Result := mrOK
    else begin
      Sender.Position := poDesktopCenter;
      if AIsDialog then Sender.BorderStyle := bsDialog
      else Sender.BorderStyle := bsSizeable;
      Result := Sender.ShowModal;
    end;
  end;
end;

function BisaAccess(AMenuId: Integer; Access: TAccessList): boolean;
//var X: TSystemAccess;
begin
  Result := true;
//  X := GlobalSystemUser.SystemAccess_Arr.GetByMenuID(AMenuId);
//  Result := (X<>nil) and (Access in X.AccessList);
//  if (GlobalSystemUser.AccessLevel>=LEVEL_OWNER ) then Result := true;
end;
//, , , , , ,
function BisaNambah(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksInput);
end;

function BisaEdit(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksEdit);
end;

function BisaHapus(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksHapus);
end;

function BisaLihatRekap(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksRekap);
end;

function BisaLihatLaporan(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksReport);
end;

function BisaPrint(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksCetak);
end;

function BisaEkspor(AMenuId: Integer): boolean;
begin
  Result := BisaAccess(AMenuId, alAksExport);
end;

procedure ScrollBox;
begin
  PostMessage(Application.MainForm.Handle,WM_ResizeScrollBoxMsg,Width,Height);
  Application.ProcessMessages;
end;

procedure StatusBar(Text: string);
begin
  StatusBarText := Text;
  PostMessage(Application.MainForm.Handle,wm_UpdateStatusBarMsg,0,0);
  Application.ProcessMessages;
end;

procedure StatusBar(Position: integer);
begin
  if (Position=-1) then
    PostMessage(Application.MainForm.Handle,wm_UpdateStatusBarMsg,2,0)
  else
    PostMessage(Application.MainForm.Handle,wm_UpdateStatusBarMsg,1,Position);
  Application.ProcessMessages;
end;

procedure TfrmMainMenu.FormCreate(Sender: TObject);
begin
//  if trunc(dtpSystem.Date) < trunc(ServerNow) then begin
//    Application.Terminate;
//  end;
  ListCurrentMenuId := TStringList.Create;
  Self.DoubleBuffered := true;
//  StatusBar.Panels[2].Width := 50;
//  StatusBar.Panels[1].Width := 150;
//  StatusBar.Panels[0].Width := ClientWidth - StatusBar.Panels[1].Width - StatusBar.Panels[2].Width;
//
//  ProgressBar.SetBounds(
//    StatusBar.Panels[0].Width + 2,
//    StatusBar.Top + 2,
//    StatusBar.Panels[1].Width - 2,
//    StatusBar.Height - 2);
//  Panel2.SetBounds(StatusBar.Panels[0].Width + 2,
//    StatusBar.Top + 2,
//    StatusBar.Panels[1].Width - 2,
//    StatusBar.Height - 2);
  LastPanel := nil;  LastPanelParent := nil;
  LastPanelWidth := 0; LastPanelHeight := 0;
  StatusBar.Panels.Add;
  StatusBar.Panels[0].Width := 112;
  StatusBar.Panels.Add;
end;

procedure TfrmMainMenu.FormDestroy(Sender: TObject);
begin
  ListCurrentMenuId.Free;
end;

function TfrmMainMenu.FormIsShow(AMenuId: Integer): Boolean;
var
  i : Integer;
begin
  Result := False;
  for i := 0 to ListCurrentMenuId.Count-1 do begin
    if ListCurrentMenuId.Strings[i] = IntToStr(AMenuId) then begin
//      AdvOfficeMDITabSet1.ActiveTabIndex := i;
      Result := True;
      Break;
    end;
  end;
end;

procedure TfrmMainMenu.FormShow(Sender: TObject);
begin

  SudahBackup := FALSE;
  if (GlobalSystemUser.UserID='') then FileLogoff1Click(nil);
  sttUser.Caption := GlobalSystemUser.UserName;
  StatusBar.Panels[0].Text := ':. User : '+GlobalSystemUser.UserName;
//  btnProgressJob.Enabled := (GlobalSystemUser.AccessLevel = LEVEL_DEVELOPER) or (GlobalSystemUser.Is_all = TRUE_STRING);
//  btnLapProgressJob.Enabled := (GlobalSystemUser.AccessLevel = LEVEL_DEVELOPER) or (GlobalSystemUser.Is_all = TRUE_STRING);
end;

procedure TfrmMainMenu.gotomenu1Click(Sender: TObject);
var MenuID: string;tmp : integer;
begin
//  GetPanel(0);
  if InputQuery('Pilih Menu','Nomor Menu', MenuID) then  begin
    if UpperCase(copy(MenuID,1,1)) = 'P' then
      tmp := StrToIntDef('6'+copy(MenuId,2,3),0)
    else
      tmp := StrToIntDef(MenuID,0);
//    OpenForm(tmp);
  end;
end;

procedure TfrmMainMenu.lblPemakaianItemsClick(Sender: TObject);
var ALabel : TLabel;
begin

  ALabel:= (Sender as TLabel);
//  if (ALabel.Tag > 0) then OpenForm(ALabel.Tag);
end;

procedure TfrmMainMenu.GradientLabel3Click(Sender: TObject);
var //i : Integer;
   cmd : string;
   ada : boolean;
begin
  if SudahBackup = false then begin
    if Confirmed('Database hendak di-backup ?') then begin
//      cmd := 'exp art_audio/art_audio@orcl10g file=art_audio'+FormatDateTime('ddMMyy', ServerNow)+'.dmp';
//      cmd := 'exp art_audio/art_audio@xe file=art_audio'+FormatDateTime('ddMMyy', ServerNow)+'.dmp';
     cmd := 'exp art_audio/art_audio@192.168.0.12:1521/xe file=art_audio'+FormatDateTime('ddMMyy', ServerNow)+'.dmp';
      if WinExec(Str2PChar(cmd),SW_SHOWDEFAULT) < 31 then
        showmessage('Gagal backup data.')
      else begin
        repeat
          ada := FindProcess('exp.exe')
        until ada = false;
        showmessage('Data berhasil dibackup.')
      end;
    end;
  end;
end;

procedure TfrmMainMenu.FormCanResize(Sender: TObject; var NewWidth,
  NewHeight: Integer; var Resize: Boolean);
begin
  Resize := (NewWidth>400) and (NewHeight>300);
end;

procedure TfrmMainMenu.FormClose(Sender: TObject;
  var Action: TCloseAction);
var i : Integer;
//   cmd : string;
//    ada : boolean;
begin
//  if Confirmed('Program akan ditutup. Anda yakin?') then begin
////    if SudahBackup = false then begin
////      if Confirmed('Database hendak di-backup ?') then begin
////        //cmd := 'exp fam_mobil/fam_mobil@orcl10g file=fam_mobil'+FormatDateTime('ddMMyy', ServerNow)+'.dmp';
//////        cmd := 'exp art_audio/art_audio@xe file=art_audio'+FormatDateTime('ddMMyy', ServerNow)+'.dmp';
////        cmd := 'exp art_audio/art_audio@192.168.0.12:1521/xe file=art_audio'+FormatDateTime('ddMMyy', ServerNow)+'.dmp';
////        if WinExec(Str2PChar(cmd),SW_SHOWDEFAULT) < 31 then
////          showmessage('Gagal backup data.')
////        else begin
////          repeat
////            ada := FindProcess('exp.exe')
////          until ada = false;
////          showmessage('Data berhasil dibackup.');
////        end;
////      end;
////    end;
//    ListCurrentMenuId.Clear;
//    for i := 0 to MDIChildCount-1 do begin
//      MDIChildren[i].Close;
//    end;
//
////    HHCloseAll;
//    FinalizeApplication;
//  end else Action := caNone;
  try
    Action := caFree;
  except
    Action := caFree;
  end;
end;

procedure TfrmMainMenu.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
var i : integer;
begin
  if Confirmed('Program akan ditutup. Anda yakin?') then begin
//    if SudahBackup = false then begin
//      if Confirmed('Database hendak di-backup ?') then begin
//        //cmd := 'exp fam_mobil/fam_mobil@orcl10g file=fam_mobil'+FormatDateTime('ddMMyy', ServerNow)+'.dmp';
////        cmd := 'exp art_audio/art_audio@xe file=art_audio'+FormatDateTime('ddMMyy', ServerNow)+'.dmp';
//        cmd := 'exp art_audio/art_audio@192.168.0.12:1521/xe file=art_audio'+FormatDateTime('ddMMyy', ServerNow)+'.dmp';
//        if WinExec(Str2PChar(cmd),SW_SHOWDEFAULT) < 31 then
//          showmessage('Gagal backup data.')
//        else begin
//          repeat
//            ada := FindProcess('exp.exe')
//          until ada = false;
//          showmessage('Data berhasil dibackup.');
//        end;
//      end;
//    end;
    ListCurrentMenuId.Clear;
    for i := 0 to MDIChildCount-1 do begin
      MDIChildren[i].Close;
    end;

//    HHCloseAll;
    FinalizeApplication;
    CanClose := true;
  end else CanClose := false;
end;

procedure TfrmMainMenu.FormKeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  CASE key of
    VK_ESCAPE : CloseAllChild;
  END;
end;

procedure TfrmMainMenu.Timer1Timer(Sender: TObject);
begin
  StatusBar.Panels[1].Text := FormatDateTime(LongDateFormat+' | '+LongTimeFormat,Now);
end;

procedure TfrmMainMenu.ChangeDisplay;
var i: integer; Menu,subMenu: TMenuItem; LastSubmenuName,
LastMenuGroup: string;
begin
  ListCurrentMenuId.Clear;
  LastSubmenuName := '';
  tag := 0;
  if (GlobalSystemUser.UserId<>'') then begin
    GlobalSystemUser.SystemAccess_Arr.RecalculateUsage;
    Self.RefreshFavourite;
  end else begin
  end;

  for i:=0 to GlobalSystemMenu_Arr.Count-1 do begin
    if (Menu=nil) then continue;
    if (GlobalSystemMenu_Arr[i].MenuGroup<>LastMenuGroup) {and (AformType <> FormType)} then begin
      LastMenuGroup := GlobalSystemMenu_Arr[i].MenuGroup;
      if (i>0) then    Self.AddMainMenuItem(Menu, '-', -1, 0, 0, false);
      Self.AddMainMenuItem(Menu,'******* '+GlobalSystemMenu_Arr[i].MenuGroup+' *******', -1, 0, 0, false);
    end;
    if copy(GlobalSystemMenu_Arr[i].MenuName,0,1) <> '*' then begin
    end;
  end;
end;

procedure TfrmMainMenu.ChildFormClose(Sender: TObject;
  var Action: TCloseAction);
begin
  Action   := caFree;
end;

procedure TfrmMainMenu.ChildFormSingle(Sender: TObject;
  var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TfrmMainMenu.ChildKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
end;

procedure TfrmMainMenu.CloseAllChild;
var i : Integer;
begin
  for i := 0 to MDIChildCount-1 do begin
    MDIChildren[i].Close;
  end;
end;

function TfrmMainMenu.Str2PChar(var S: String): PChar;
begin
  IF (S <> '')
  THEN BEGIN
         IF (S[Length(S)]<>#0) { It has to end with a zero byte... }
         THEN S := S + #0;
       END;
  Str2PChar := @S[1];
end;

function TfrmMainMenu.FindProcess(ProcName: String): boolean;
var ExeName : String;
    List    : TStringList;
    Found   : Boolean;
    I       : Integer;
    proc : PROCESSENTRY32;
    hSnap : HWND;
    Looper : BOOL;
begin
    List := TStringList.Create;
    proc.dwSize := SizeOf(Proc);
    hSnap := CreateToolhelp32Snapshot(TH32CS_SNAPALL,0);
    Looper := Process32First(hSnap,proc);
    Found := False;
    while Integer(Looper) <> 0 do
    begin
      ExeName := ExtractFileName(proc.szExeFile);
      List.Add(ExeName);
      Looper := Process32Next(hSnap,proc);
    end;
    For I := 0 to List.Count - 1 do
    begin
      If List.Strings[I] = ProcName then
        Found := true
    end;
    List.Free;
    result := Found;
    CloseHandle(hSnap);
end;

procedure TfrmMainMenu.RefreshFavourite;
var i: integer; Prefix: string;
begin
  for i:=0 to 9 do
    with GlobalSystemUser.SystemAccess_Arr.TopUsage[i] do
      if Assigned(GlobalSystemUser.SystemAccess_Arr.TopUsage[i]) and
        (GlobalSystemUser.SystemAccess_Arr.TopUsage[i].UsageCount>0) then begin
        case SystemMenu.FormType of
          ftDetail: Prefix := 'New';
          ftLookup: Prefix := 'Data';
          ftReview: Prefix := 'New';
          ftBrowse: Prefix := 'Transaction';
          ftReport: Prefix := 'Report';
          ftSetting: Prefix := 'Setting';
        end;

        Self.AddShortcutBarButton('Favourite',Prefix+' '+SystemMenu.MenuName,SystemMenu.ImageIndex,SystemMenu.MenuId);
      end;
end;

procedure TfrmMainMenu.OpenForm(TFrmClass : TFormClass; AShowModal: boolean);
var Form2: TForm; SystemMenu: TSystemMenu; SystemAccess: TSystemAccess;

begin                               
  Form2 := TFrmClass.Create(Self);
  Form2.OnClose := ChildFormClose;
  Form2.OnKeyDown := ChildKeyDown;
  Form2.FormStyle := fsMDIChild;
  AdvOfficeMDITabSet1.AddTab(Form2);
end;

procedure TfrmMainMenu.UpdateStatusBar;
begin
  StatusBar.Panels[0].Text := ':. User : '+GlobalSystemUser.UserName;
end;

procedure TfrmMainMenu.ResizeScrollBox(var AMessage: TMessage);
begin
  LastPanel.Align := alNone;
  if (AMessage.WParam>=0) then LastPanelWidth := AMessage.WParam;
  if (AMessage.LParam>=0) then LastPanelHeight := AMessage.LParam;
end;

procedure TfrmMainMenu.FileLogoff1Click(Sender: TObject);

begin
  ListCurrentMenuId.Clear;
  GlobalSystemUser.Reset;
  CloseAllChild;
  frmLogin.LogOn;
  ChangeDisplay;
end;

procedure TfrmMainMenu.FileExit1Click(Sender: TObject);
begin
  Close;
end;

procedure TfrmMainMenu.ViewGoToMenu1Click(Sender: TObject);
var MenuID: string;tmp : integer;
begin
  if InputQuery('Pilih Menu','Nomor Menu', MenuID) then  begin
    if UpperCase(copy(MenuID,1,1)) = 'P' then
      tmp := StrToIntDef('6'+copy(MenuId,2,3),0)
    else
      tmp := StrToIntDef(MenuID,0);
  end;
end;

procedure TfrmMainMenu.AddMainMenuItem(AMenuItem: TMenuItem; ACaption: string;
  AImageIndex: integer; AShortcut: TShortcut; ATag: integer; AEnabled: boolean);
var NewMenuItem: TMenuItem;
begin
  NewMenuItem := TMenuItem.Create(AMenuItem);
  NewMenuItem.Caption := ACaption;
  NewMenuItem.ImageIndex := AImageIndex;
  NewMenuItem.Shortcut := AShortcut;
  NewMenuItem.Tag := ATag;
  NewMenuItem.Enabled := AEnabled;
  AMenuItem.Add(NewMenuItem);
end;

procedure TfrmMainMenu.AddShortcutBarButton(APanelName: string; AButtonName: string;
  AImageIndex: integer; ATag: integer);
begin
end;

function TfrmMainMenu.AddSubMenuItem(aMenuItem: TMenuItem;
  aCaption: string; AImageIndex: integer; AShortcut: TShortcut;
  ATag: integer; AEnabled: boolean):TMenuITem;
var subMenu : TMenuItem;
begin
  subMenu := TMenuItem.Create(aMenuItem);
  subMenu.Caption := aCaption;
  subMenu.ImageIndex := AImageIndex;
  subMenu.ShortCut := AShortcut;
  subMenu.Tag := ATag;
  subMenu.Enabled := AEnabled;
  aMenuItem.Add(subMenu);
  Result := subMenu;
end;

procedure TfrmMainMenu.btnLogoffClick(Sender: TObject);
begin
  frmLogin.LogOn;
  AdvOfficeMDITabSet1.CloseAllTabs;
  sttUser.Caption := GlobalSystemUser.UserName;
end;

procedure TfrmMainMenu.btnMst_prjClick(Sender: TObject);
begin
  OpenForm(TfrmMasterProjectRekap);
end;

procedure TfrmMainMenu.AdvGlowButton1Click(Sender: TObject);
begin
  Application.CreateForm(TfrmChangePassword, frmChangePassword);
  frmChangePassword.OnClose := frmMainMenu.ChildFormClose;
  frmChangePassword.Execute(GlobalSystemUser.UserId);
end;

procedure TfrmMainMenu.btnKelolaUserClick(Sender: TObject);
begin
  if GlobalSystemUser.IsTambahUser <> TRUE_STRING then begin
    Inform(MSG_UNAUTHORISED_ACCESS);
    Exit;
  end;

  OpenForm(TfrmKelolaUser);
end;

procedure TfrmMainMenu.btnLapProgressJobClick(Sender: TObject);
begin
  openForm(TfrmCekBackupData);
end;

procedure TfrmMainMenu.AdvOfficeMDITabSet1TabClose(Sender: TObject;
  TabIndex: Integer; var Allow: Boolean);
begin
  ListCurrentMenuId.Delete(TabIndex);
end;

procedure TfrmMainMenu.AdvToolBar1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  CASE key of
    VK_ESCAPE : CloseAllChild;
  END;
end;

procedure TfrmMainMenu.btntutupClick(Sender: TObject);
begin
  if Confirmed('Program akan ditutup. Anda yakin?') then begin
    try
      application.terminate;
    except
      application.terminate;
    end;
  end; 
end;

procedure TfrmMainMenu.OnLabelClick(Sender: TObject);
var ALabel : TLabel;
begin
  ALabel:= (Sender as TLabel);
end;

procedure TfrmMainMenu.Houver(Sender: TObject);
begin
  (Sender as TLabel).Font.Style:= [fsUnderLine];
  (Sender as TLabel).Font.Color:= clBlue;
end;

procedure TfrmMainMenu.UnHouver(Sender: TObject);
begin
  (Sender as TLabel).Font.Style:= [];
  (Sender as TLabel).Font.Color:= $00423740;
end;


procedure TfrmMainMenu.lblKeluarClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmMainMenu.lblLogOffClick(Sender: TObject);
begin
  ListCurrentMenuId.Clear;
  GlobalSystemUser.Reset;
  CloseAllChild;
  frmLogin.LogOn;
  ChangeDisplay;
end;

procedure TfrmMainMenu.lblSuratJalanClick(Sender: TObject);
var ALabel : TLabel;
begin
  ALabel:= (Sender as TLabel);
end;

procedure TfrmMainMenu.logoff1Click(Sender: TObject);
begin
  ListCurrentMenuId.Clear;
  GlobalSystemUser.Reset;
  CloseAllChild;
  frmLogin.LogOn;
  ChangeDisplay;
end;

end.


