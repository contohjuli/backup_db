unit Login;
    {DEFINE DEBUG}
interface

uses
  OracleConnection, UConst, 
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Mask, DB, ADODB, AdvEdit, ExtCtrls, Buttons, MainMenu, jpeg,
  AdvSmoothPanel;

type
  TfrmLogin = class(TForm)
    AdvSmoothPanel1: TAdvSmoothPanel;
    lblLogin: TLabel;
    lblPassword: TLabel;
    btnOK: TBitBtn;
    btnCancel: TBitBtn;
    txtLoginID: TAdvEdit;
    txtPass: TAdvEdit;
    Label1: TLabel;
    Image1: TImage;
    procedure btnOKClick(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure btnCancelClick(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClick(Sender: TObject);
  private
    { Private declarations }
    LoginMode: boolean;
    LocalIsBisaTambah, LocalLoginID, LocalLoginName: string;
    LocalAccessLevel, LocalNeedAccessLevel: integer;
  public
    { Public declarations }
    function LogOn: boolean;
    function Authorize(var ALoginID: string; ANeedAccessLevel: integer): boolean;
    procedure Status(const MessageStatus: string = '');

  end;

var
  frmLogin: TfrmLogin;
  NeedAccessLevel: integer = 0;
  myMainForm: TForm = nil;

implementation

uses USystemMenu, UGeneral;

{$R *.dfm}

procedure TfrmLogin.btnOKClick(Sender: TObject);
begin
  if ((UpperCase(txtLoginID.Text) = 'SYSTEM') and (UpperCase(txtPass.Text) = 'MANAGER')) then begin
    GlobalSystemUser.Reset;
    LocalLoginID := txtLoginID.Text;
    LocalLoginName := 'Default User';
    LocalAccessLevel := LEVEL_DEFAULT;
    LocalIsBisaTambah := TRUE_STRING;
    ModalResult := mrOK;
  end else if (UpperCase(txtLoginID.Text) = 'ORION') and (UpperCase(txtPass.Text) = 'BANDUNG') then begin
    GlobalSystemUser.Reset;
    LocalLoginID := txtLoginID.Text;
    LocalLoginName := 'Developer';
    LocalAccessLevel := LEVEL_DEVELOPER;
    LocalIsBisaTambah := TRUE_STRING;
    ModalResult := mrOK;
  end else
  if (txtLoginID.Text = '') then begin
    Alert('User id belum diisi.');
    txtLoginID.SetFocus;
    GlobalSystemUser.Reset;
    exit;
  end else if (GlobalSystemUser.SelectInDB(txtLoginID.Text, '') = false) then begin
    Alert('User id tidak terdaftar.');
    txtLoginID.SetFocus;
    GlobalSystemUser.Reset;
  end else if (GlobalSystemUser.Passwd <> txtPass.Text)    then begin
    Alert('Password salah.');
    txtLoginID.SetFocus;
    GlobalSystemUser.Reset;
  end else if (GlobalSystemUser.TglNonAktif<>0) then begin
    Alert('Login disabled.');
    txtLoginID.SetFocus;
    GlobalSystemUser.Reset;
  end else begin
    LocalLoginID := txtLoginID.Text;
    LocalLoginName := GlobalSystemUser.UserName;
    LocalAccessLevel := GlobalSystemUser.AccessLevel;
    LocalIsBisaTambah := GlobalSystemUser.IsTambahUser;

    if (LocalAccessLevel<LocalNeedAccessLevel) then begin
      if (LoginMode) then begin
        Alert('Logon need access level '+IntToStr(LocalNeedAccessLevel)+'.');
      end else
        Alert('Authentication need access level '+IntToStr(LocalNeedAccessLevel)+'.');
      txtLoginID.SetFocus;
    end else begin
      if (GlobalSystemUser.AccessLevel >= LEVEL_ADMIN) then
        ModalResult := mrOK
      else Begin  
        ModalResult := mrOK;
      End;

    end;
  end;
  btnOK.Enabled := true;

end;

procedure TfrmLogin.btnCancelClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmLogin.FormClick(Sender: TObject);
begin
  Application.Minimize;
end;

procedure TfrmLogin.FormCreate(Sender: TObject);
begin
  LoginMode := true;
  try
  finally
  end;
  Caption := ReplaceSubStr(Caption,'ApplicationTitle',Application.Title);
{$IFDEF POLITIX}
  lblCopyright.Visible := false;
{$ENDIF}
end;

procedure TfrmLogin.FormShow(Sender: TObject);
begin
  Self.Width := Screen.DesktopWidth-50;
  Self.Height := Screen.DesktopHeight-50;
  Self.Top := 0;
  Self.Left := 0;
  AdvSmoothPanel1.Left := trunc(Self.Width / 2) - trunc(AdvSmoothPanel1.Width/2);
  AdvSmoothPanel1.Top := trunc(Self.Height / 2) - trunc(AdvSmoothPanel1.Height/2);

end;

procedure TfrmLogin.Image1Click(Sender: TObject);
begin
  Application.Destroy;
end;

procedure TfrmLogin.FormActivate(Sender: TObject);
begin
  txtPass.MaxLength := 50;
  
  txtLoginID.Clear;
  txtPass.Clear;
  txtLoginID.SetFocus;
{$IFDEF DEBUG}
  txtLoginID.Text   := 'system';
  txtPass.Text  := 'or10nbd9';
  btnOK.SetFocus;
{$ENDIF}
end;

function TfrmLogin.LogOn: boolean;
begin
  Self.Caption := Application.Title+' - Login';
  LoginMode := true;
  LocalNeedAccessLevel := NeedAccessLevel;

  if Assigned(myMainForm) then myMainForm.Hide;
  Result := (ShowModal=mrOK);
  if (Result) then begin
    GlobalSystemUser.UserName := LocalLoginName;
    GlobalSystemUser.UserId := LocalLoginID;
    GlobalSystemUser.AccessLevel := LocalAccessLevel;
    GlobalSystemUser.IsTambahUser := LocalIsBisaTambah;


    if Assigned(myMainForm) then myMainForm.Show;
  end else begin
    Application.Terminate;
  end;
end;

function TfrmLogin.Authorize(var ALoginID: string; ANeedAccessLevel: integer): boolean;
var LastBorderStyle: TFormBorderStyle; LastCaption: string;
begin
  Self.Caption := Application.Title+' - Authorization';
  LastBorderStyle := BorderStyle; BorderStyle := bsToolWindow;
  LastCaption := Caption; Caption := Application.Title+' - Otorisasi';
  LoginMode := false;
  LocalNeedAccessLevel := ANeedAccessLevel;

  if (ShowModal=mrOK) then ALoginID := LocalLoginID;
  Result := (ModalResult = mrOK);
  BorderStyle := LastBorderStyle;
  Caption := LastCaption;
end;

procedure TfrmLogin.Status(const MessageStatus: string);
begin
  Self.Caption := Application.Title;
  lblLogin.Visible := (MessageStatus='');
  lblPassword.Visible := (MessageStatus='');
  txtLoginID.Visible := (MessageStatus='');
  txtPass.Visible := (MessageStatus='');
  btnOK.Visible := (MessageStatus='');
  btnCancel.Visible := (MessageStatus='');

  try
    if (MessageStatus<>'') then begin
      if (not Self.Visible) then Self.Show;
    end else begin
      if (Self.Visible) then Self.Hide;
    end;
  except
  end;
  Application.ProcessMessages;
end;

end.
