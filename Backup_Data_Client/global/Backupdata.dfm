object Form1: TForm1
  Left = 659
  Top = 337
  BorderStyle = bsDialog
  Caption = 'Backup Data'
  ClientHeight = 55
  ClientWidth = 136
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object btnBackUp: TButton
    Left = 33
    Top = 15
    Width = 75
    Height = 25
    Caption = 'BackUp Data'
    TabOrder = 0
    OnClick = btnBackUpClick
  end
end
