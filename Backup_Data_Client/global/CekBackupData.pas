unit CekBackupData;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, AsgFindDialog, StdCtrls, ComCtrls, AdvOfficeButtons, Grids, AdvObj,
  BaseGrid, AdvGrid, ExtCtrls, AdvPanel, AdvGlowButton, AdvCombo, IdMultipartFormData,
  Gauges, IdHttp, tmsAdvGridExcel, asgprev;

type
  TfrmCekBackupData = class(TForm)
    MainPanel: TPanel;
    pnlFilter: TPanel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    PnlJenisSaldo: TAdvPanel;
    Panel1: TPanel;
    asgRekap: TAdvStringGrid;
    btnFilter: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    btnCetak: TAdvGlowButton;
    chbSFooter: TAdvOfficeCheckBox;
    Label1: TLabel;
    Label3: TLabel;
    dtpDari: TDateTimePicker;
    dtpSampai: TDateTimePicker;
    cmbTipe: TAdvComboBox;
    Label2: TLabel;
    Label4: TLabel;
    cmbNamaProject: TAdvComboBox;
    Gauge1: TGauge;
    AdvExcel: TAdvGridExcelIO;
    SaveToExcell: TSaveDialog;
    advPrint: TAdvPreviewDialog;
    SaveDialog1: TSaveDialog;
    procedure btnLoadClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnFilterClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure chbSFooterClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure asgRekapButtonClick(Sender: TObject; ACol, ARow: Integer);
  private
    { Private declarations }
    procedure initForm;
    procedure setGrid;
    procedure loadProfile;
    procedure arrangeColSize;
    procedure loadData;
    procedure get_list_histori_backup(var AList : TStringList; dari, sampai : TDate; tipe : string; namaProject : string);
    procedure getDataCabang;


    procedure sqltonamevaluelistOnline(var AStringList: TStringList; ASQL: string);
    procedure get_json_list_from_url_by_post(var AList : tstringlist; url : string; Data : TIdMultiPartFormDataStream);
    function JSONtoString(json : string; field : integer) : string;
    procedure jsonToStringlist(var Alist : TStringList; json : string);
    function replaceHasilJSon(input : string; isAdaNull : Boolean = False) : string;
    function decriptNew(input: string): string;
    function getListNameCaseSensitive(list: TStringList; cari: string): string;
    function IsAlphaExist(AString: string): boolean;
    function IsNumericExist(AString: string): boolean;
    procedure asgExportToExcell(aGrid : TAdvStringGrid; aDialogs : TSaveDialog;IsCellFormat : boolean = true);

  public
    { Public declarations }
    procedure execute;
  end;

var
  frmCekBackupData: TfrmCekBackupData;

const
  colNo          = 0;
  colTanggal     = 1;
  colNamaProject = 2;
  colNamaDB      = 3;
  colTipe        = 4;
  colPath        = 5;
  colDownload    = 6;
  colPathDownload = 7;
  colFileName     = 8;

  TIPE_Harddisk  = 'H';
  TIPE_Flashdisk = 'F';

  TIPE_Harddisk_TEXT  = 'Harddisk';
  TIPE_Flashdisk_TEXT = 'Flashdisk';

implementation

uses
  Subroutines, StrUtils, OracleConnection, MainMenu;

{$R *.dfm}

{ TfrmCekBackupData }

procedure TfrmCekBackupData.arrangeColSize;
begin
  asgRekap.AutoNumberCol(colNo);
  asgRekap.AutoSizeColumns(True);
  asgRekap.ColWidths[colPathDownload] := 0;
end;                                         

procedure TfrmCekBackupData.asgExportToExcell(aGrid: TAdvStringGrid;
  aDialogs: TSaveDialog; IsCellFormat: boolean);
var i : Integer;
begin
  if aDialogs.Execute then begin
    AdvExcel.Options.ExportCellFormats := IsCellFormat;
    if FileExists(aDialogs.FileName) then begin
      AdvExcel.AdvStringGrid := aGrid;
      if Confirmed(aDialogs.FileName+' already exist.'+#13+'Do you want to replace it?') then begin
          DeleteFile(aDialogs.FileName);
          AdvExcel.XLSExport(aDialogs.FileName);
      end else exit;
    end else begin
      AdvExcel.AdvStringGrid := aGrid;
      AdvExcel.XLSExport(aDialogs.FileName);
    end;
  end;
end;

procedure TfrmCekBackupData.asgRekapButtonClick(Sender: TObject; ACol,
  ARow: Integer);
var s: TStream;
    http : TIdHttp;
    url : string;
    fileAsal, fileTujuan: PAnsiChar;

begin
  if ACol = colDownload then begin
    SaveDialog1.FileName := asgRekap.Cells[colFileName, ARow];
    GetCurrentDir;
    if SaveDialog1.Execute then begin
//      if not DirectoryExists(vApplikasiPath+'/download') then
//        CreateDir(vApplikasiPath+'/download');
      SetCurrentDir(vApplikasiPath);
      s := TFileStream.Create(asgRekap.Cells[colFileName, ARow], fmCreate);
      try
        http := TIdHttp.Create(nil);
        http.HandleRedirects := true;
        http.ReadTimeout := 600000;
        http.Request.ContentType := 'application/json';
        http.Request.ContentEncoding := 'utf-8';
        url := replacetext(asgRekap.Cells[colPathDownload, ARow], ' ','%20');
        http.Get(url, s);
        fileAsal:= PAnsiChar(vApplikasiPath+'/'+asgRekap.Cells[colFileName, ARow]);
        fileTujuan:= PAnsiChar(SaveDialog1.FileName);
        MoveFile(fileAsal, fileTujuan);

        Inform('Download berhasil.');
      finally
        http.Free;
        s.free;
      end;
    end;
  end;

end;

procedure TfrmCekBackupData.asgRekapGetAlignment(Sender: TObject; ARow,
  ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if (ARow < 1) then HAlign := taCenter
  else if ((acol IN [colNo])) then
          HAlign := taRightJustify;
  vAlign := vtaCenter;
end;

procedure TfrmCekBackupData.btnCetakClick(Sender: TObject);
begin
  advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Laporan Backup Data Client');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColumnSize.StretchColumn := 0;
  asgRekap.Colcount := asgRekap.Colcount-3;
  SetingPrint(asgRekap);
  advPrint.Execute;
  asgRekap.Colcount :=asgRekap.Colcount+3;
  asgRekap.ColumnSize.StretchColumn := -1;
  asgRekap.ColumnSize.Stretch := True;
  arrangecolsize;
end;

procedure TfrmCekBackupData.btnEksporClick(Sender: TObject);
begin
  asgExportToExcell(asgRekap, SaveToExcell, False);
end;

procedure TfrmCekBackupData.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilter, btnFilter, 95);
end;

procedure TfrmCekBackupData.btnLoadClick(Sender: TObject);
begin
  asgRekap.row := 1;
  asgRekap.Col := 1;
  loadData;
end;

procedure TfrmCekBackupData.btnResetClick(Sender: TObject);
begin
  asgRekap.row := 1;
  asgRekap.Col := 1;
  initform;
  setgrid;
end;

procedure TfrmCekBackupData.chbSFooterClick(Sender: TObject);
begin
  asgRekap.SearchFooter.Visible := chbSFooter.checked;
end;

function TfrmCekBackupData.decriptNew(input: string): string;
var list : TStringList;
    hasil, temp, cari : string;
    i, charke : Integer;
begin
  list := TStringList.create;
  list.Add('e9r=a');
  list.Add('asF=b');
  list.Add('ytH=c');
  list.Add('43y=d');
  list.Add('nSu=e');
  list.Add('fdQ=f');
  list.Add('uyo=g');
  list.Add('jhr=h');
  list.Add('vgb=i');
  list.Add('nm7=j');
  list.Add('cvF=k');
  list.Add('yKV=l');
  list.Add('k93=m');
  list.Add('fdk=n');
  list.Add('vfd=o');
  list.Add('34g=p');
  list.Add('320=q');
  list.Add('eds=r');
  list.Add('ehj=s');
  list.Add('ebv=t');
  list.Add('etr=u');
  list.Add('w94=v');
  list.Add('vcf=w');
  list.Add('pty=x');
  list.Add('j9f=y');
  list.Add('xje=z');
  list.Add('e92=A');
  list.Add('asD=B');
  list.Add('yFH=C');
  list.Add('4Xy=D');
  list.Add('n1u=E');
  list.Add('GdQ=F');
  list.Add('Yyo=G');
  list.Add('jJr=H');
  list.Add('v7b=I');
  list.Add('NM7=J');
  list.Add('c0F=K');
  list.Add('QeV=L');
  list.Add('kg3=M');
  list.Add('jhk=N');
  list.Add('cir=O');
  list.Add('dZi=P');
  list.Add('kR1=Q');
  list.Add('fdk=R');
  list.Add('jLi=S');
  list.Add('f9w=T');
  list.Add('DYx=U');
  list.Add('vde=V');
  list.Add('akb=W');
  list.Add('dsf=X');
  list.Add('gfv=Y');
  list.Add('jul=Z');
  list.Add('cHs=0');
  list.Add('dIe=1');
  list.Add('lgu=2');
  list.Add('mIe=3');
  list.Add('SuM=4');
  list.Add('heN=5');
  list.Add('32x=6');
  list.Add('efr=7');
  list.Add('fd8=8');
  list.Add('fUK=9');

  hasil := '';
  cari := '';
  charke := 0;
  for i := 1 to Length(input) do begin
    if (IsAlphaExist(input[i])) or (IsNumericExist(input[i])) then begin
      charke := charke + 1;
      cari := cari + input[i];
      if (charke = 3) then begin
  //      temp := list.Values[cari];
        temp := getListNameCaseSensitive(list, cari);

        if temp = '' then
          temp := cari;
        hasil := hasil + temp;
        cari := '';
        charke := 0;
      end;
    end else hasil := hasil + input[i];

  end;
  Result := hasil;
  list.Destroy;
end;

procedure TfrmCekBackupData.execute;
begin
  btnReset.Click;
  Run(Self);
end;

procedure TfrmCekBackupData.FormShow(Sender: TObject);
begin
  Execute;
end;

procedure TfrmCekBackupData.getDataCabang;
var
  vSql, vFilter : String;
  AList: TStringList;
  i : Integer;
  name, value : string;
begin
  AList := TStringlist.Create;
  vSql := 'SELECT DISTINCT nama FROM( '+
            'SELECT nama FROM master_project '+
            ' UNION ALL '+
            'SELECT nama_project AS nama FROM histori_backup '+
          ')AS dataa order by nama';
  sqltonamevaluelistOnline(AList, vSql);

  Gauge1.Show;
  cmbNamaProject.Items.Clear;
  cmbNamaProject.Items.Add('Semua');
  for i := 0 to AList.Count-1 do begin
    Gauge1.Progress := Round((i+1)/AList.Count * 100);
    cmbNamaProject.Items.Add(EkstrakString(AList.Names[i], '#', 1));
  end;
  cmbNamaProject.ItemIndex := 0;
  Gauge1.Hide;
  AList.Destroy;
end;

function TfrmCekBackupData.getListNameCaseSensitive(list: TStringList;
  cari: string): string;
var i : Integer;
begin
  Result := '';
  for i := 0 to list.count-1 do begin
    if list.Names[i] = cari then begin
      Result := list.ValueFromIndex[i];
      Exit;
    end;
  end;
end;

procedure TfrmCekBackupData.get_json_list_from_url_by_post(
  var AList: tstringlist; url: string; Data: TIdMultiPartFormDataStream);
var jsonIsi, jSonPerBaris, objJson : string;
    http : TIdHttp;
    i : Integer;
    seq : string;
    sql : string;
begin
  http := TIdHttp.Create(nil);
  http.HandleRedirects := true;
  http.ReadTimeout := 600000;
  http.Request.ContentType := 'application/json';
  http.Request.ContentEncoding := 'utf-8';
  http.ProxyParams.BasicAuthentication := False;
  jsonIsi := http.Post(url, data);
  if (jsonIsi <> '[]') and not(containstext(jsonisi, 'Tidak ada data')) then begin
    jsonToStringlist(AList, jsonIsi);
  end else AList.Clear;

  http.free;
end;

procedure TfrmCekBackupData.get_list_histori_backup(var AList : TStringList; dari, sampai : TDate; tipe : string; namaProject : string);
var
  filter : string;
  vsql : string;
begin
  AList := TStringlist.Create ;
  filter := '';
  if tipe <> '' then
    filter := filter + ' and tipe = '+QuotedStr(tipe);
  if namaProject <> '' then
    filter := filter + ' and nama_project = '+QuotedStr(namaProject);
  vSql := 'select seq, concat(nama_project, ''#'', nama_db, ''#'', tanggal, ''#'', tipe, ''#'', path, ''#'', IFNULL(path_cloud, '''')) from histori_backup '+
            'where date(tanggal) between ('+QuotedStr(FormatDateTime('YYYY-mm-dd', dtpDari.Date))+') and ('+QuotedStr(FormatDateTime('YYYY-mm-dd', dtpSampai.Date))+') '+
            filter;
  sqltonamevaluelistOnline(AList, vSql);
end;

procedure TfrmCekBackupData.initForm;
begin
  dtpDari.Date := Now;
  dtpSampai.Date := Now;
  cmbTipe.ItemIndex := 0;
  
  getDataCabang;  
end;

function TfrmCekBackupData.IsAlphaExist(AString: string): boolean;
var i: integer;
begin
  Result := false;
  for i:=1 to length(AString) do
    if (AString[i] in ['a'..'z','A'..'Z']) then Result := true;
end;

function TfrmCekBackupData.IsNumericExist(AString: string): boolean;
var i: integer;
begin
  Result := false;
  for i:=1 to length(AString) do
    if (AString[i] in ['0'..'9']) then Result := true;
end;

function TfrmCekBackupData.JSONtoString(json: string; field: integer): string;
var objJson : string;
begin
  objJson := EkstrakString(jSon, '^',field);
  result := EkstrakString(objJson, '~',2);
end;

procedure TfrmCekBackupData.jsonToStringlist(var Alist: TStringList;
  json: string);
var i : Integer;
    pjg : Integer;
    temp : string;
    tempname : string;
    huruf : string;
begin
  pjg := Length(json);
  Alist.Clear;
  temp := '';
  tempname := '';
  for i := 1 to pjg do begin
    huruf := json[i];
    if tempname <> '' then begin
      if tempname = '},{' then begin
        Alist.Add(temp);
        temp := huruf;
        tempname := '';
      end else begin
        tempname := tempname + huruf;
      end;
    end else if huruf = '}' then begin
      tempname := tempname + huruf;
    end else begin
      temp := temp + huruf;
    end;
  end;
  if temp <> '' then
    Alist.Add(temp);
end;

procedure TfrmCekBackupData.loadData;
var
  vSql, vFilter : String;
  AList: TStringList;
  i : Integer;
  name, value : string;
  tipe : string;
  namaProject : string;
  vRow : Integer;
begin
  if cmbTipe.ItemIndex = 1 then
    tipe := TIPE_Harddisk
  else if cmbTipe.ItemIndex = 2 then
    tipe := TIPE_Flashdisk
  else tipe := '';

  if cmbNamaProject.ItemIndex = 0 then
    namaProject := ''
  else namaProject := cmbNamaProject.Items.Strings[cmbNamaProject.Itemindex];

  get_list_histori_backup(AList, dtpDari.Date, dtpSampai.Date, tipe, namaProject);
  Gauge1.Show;
  setGrid;
  for i := 0 to AList.Count-1 do begin
//                   0        1             2          3      4     5
//    vSql := 'select seq, nama_project, nama_db, tanggal, tipe, path from histori_backup '+
    Gauge1.Progress := Round((i+1)/AList.Count * 100);
    name := AList.Names[i];
    value := AList.ValueFromIndex[i];
    name := AList.Names[i];
    if i > 0 then
      asgRekap.AddRow;
    vRow := asgRekap.RowCount-1;
    asgRekap.Cells[colTanggal, vrow] := EkstrakString(AList.Values[name], '#', 3);
    asgRekap.Cells[colNamaProject, vrow] := EkstrakString(AList.Values[name], '#', 1);
    asgRekap.Cells[colNamaDB, vrow] := EkstrakString(AList.Values[name], '#', 2);
    if EkstrakString(AList.Values[name], '#', 4) = TIPE_Harddisk then
      asgRekap.Cells[colTipe, vrow] := TIPE_Harddisk_TEXT
    else asgRekap.Cells[colTipe, vrow] := TIPE_Flashdisk_TEXT;
    asgRekap.Cells[colPath, vrow] := replacestr(EkstrakString(AList.Values[name], '#', 5), '\\', '\');
    asgRekap.Cells[colPathDownload, vRow] := HostServerCloud+'/uploads/backup/'+replacestr(EkstrakString(AList.Values[name], '#', 6), '\\', '\');
    asgRekap.Cells[colFileName, vRow] := EkstrakString(replacestr(EkstrakString(AList.Values[name], '#', 6), '\\', '\'), '/', 2);
    asgRekap.AddButton(colDownload, vRow, 70, 20, 'Download', haCenter, vaCenter);
  end;
  arrangeColSize;
  Gauge1.Hide;
  AList.Destroy;
end;

procedure TfrmCekBackupData.loadProfile;
begin
end;

function TfrmCekBackupData.replaceHasilJSon(input: string;
  isAdaNull: Boolean): string;
begin
  Result := input;
  Result := ReplaceStr(Result, '","', '^');
  Result := ReplaceStr(Result, ',"', '^');
  Result := ReplaceStr(Result, '",', '^');
  if isAdaNull then begin
    Result := ReplaceStr(Result, ',null', '^null');
    Result := ReplaceStr(Result, 'null,', 'null^');
  end;
  Result := ReplaceStr(Result, '[{"', '');
  Result := ReplaceStr(Result, '{"', '');
  Result := ReplaceStr(Result, '"', '"');
  Result := ReplaceStr(Result, '"}]', '');
  Result := ReplaceStr(Result, '"}', '');
  Result := ReplaceStr(Result, '":"', '~');
  Result := ReplaceStr(Result, '"', '');
  Result := ReplaceStr(Result, '\/', '/');
end;

procedure TfrmCekBackupData.setGrid;
begin
  asgRekap.rowcount := 2;
  asgRekap.ColCount := 10;
  asgRekap.ClearNormalCells;
  arrangeColSize;
end;

procedure TfrmCekBackupData.sqltonamevaluelistOnline(
  var AStringList: TStringList; ASQL: string);
var i: integer;
    url : string;
    jSonPerBaris, parameter : string;
    tempList : TStringList;
    objJson : string;
    data : TIdMultiPartFormDataStream;
begin
  data := TIdMultiPartFormDataStream.Create;
  tempList := TStringList.Create;
  data.AddFormField('sql', asql);
  url := HostServerCloud+'/global/sqltonamevaluelist?'+parameter;
  get_json_list_from_url_by_post(tempList, url, data);
  AStringList.Clear;
  for i := 0 to tempList.Count-1 do begin
    jSonPerBaris := tempList.Strings[i];
    jSonPerBaris := replaceHasilJSon(jSonPerBaris);
    objJson := jSonPerBaris + '~';
    AStringList.Add(JSONtoString(objJson, 1)+'='+JSONtoString(objJson, 2));
  end;
  tempList.Destroy;
  data.Destroy;
end;

end.
