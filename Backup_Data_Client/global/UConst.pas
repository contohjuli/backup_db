unit UConst;
{$DEFINE ORCL}
{DEFINE POSTGRE}
interface

uses Graphics, AdvStyleIF;

const
  {EMAIL}
  Setting_EASend = 'ES-AA1141023508-00959-1856F4BBF62E35C2338720D2259DC407';
  Setting_EAGet  = 'EG-AA1150422356-00734-CF213AB1F2B11FC81A0135087066E04D';
  //Setting_From   = 'orion.bandung@gmail.com';
  //Setting_Email_Password = 'or10nbd9';
  Setting_From   = 'contohjuli@gmail.com';
  Setting_Email_Password = 'contohjulimn7';
  Setting_Email_SMTP = 'smtp.gmail.com';
  Setting_Email_POP  = 'imap.gmail.com';
  Setting_Email_Port_Kirim = 465;
  Setting_Email_Port_Ambil = 993;

  Setting_To1 = 'contohjuni@gmail.com';
  Setting_To2 = 'nsumantry@gmail.com';

  CHAT_SENT                     = 'S';
  CHAT_RECEIVED                 = 'D';

  
  {System Messages}
  MSG_UNAUTHORISED_ACCESS       = 'Anda tidak memiliki autorisasi untuk fungsi ini';
  MSG_SAVE_CONFIRMATION         = 'Apakah data sudah benar?';
  MSG_DELETE_CONFIRMATION       = 'Data ini akan dihapus?';
  MSG_SUCCESS_SAVING            = 'Penyimpanan berhasil.';
  MSG_UNSUCCESS_SAVING          = 'Penyimpanan tidak berhasil.';
  MSG_SUCCESS_UPDATE            = 'Update data berhasil.';
  MSG_UNSUCCESS_UPDATE          = 'Update data gagal.';
  MSG_DUPLICATE                 = 'Duplikasi data.';
  MSG_NO_DATA_FOUND             = 'Data tidak ada atau tidak ditemukan.';
  MSG_ADD_DATA                  = 'Tambah data lagi ?';
  MSG_SAVE_ADD_DATA             = 'Penyimpanan berhasil, '+#13+'tambah data lagi ?';
  MSG_NO_NUMBER                 = '[Empty]';
  MSG_UNDERCONSTRUCTION         = 'Under Construction.';
  MSG_CONFIRMED_AKTIVASI        = 'Data akan dinonaktifkan / diaktifkan ? ';
  MSG_SUCCESS_DELETING          = 'Data berhasil dihapus.';
  MSG_UNSUCCESS_DELETING        = 'Data tidak berhasil dihapus.';
  MSG_SUCCESS_AKTIVASI          = 'Data berhasil diaktifkan.';
  MSG_UNSUCCESS_AKTIVASI        = 'Data tidak berhasil diaktifkan.';
  MSG_SUCCESS_NOT_AKTIVASI      = 'Data berhasil dinonaktifkan.';
  MSG_UNSUCCESS_NOT_AKTIVASI    = 'Data tidak berhasil dinonaktifkankan.';
  MSG_CAN_NOT_DELETE            = 'Transaksi ini tidak bisa dihapus, '+#13+
                                  'karena sudah digunakan oleh transaksi yang lain.';
  MSG_ROW_DELETE_CONFIRMATION   = 'Baris ini akan dihapus?';
  MSG_CAN_NOT_UPDATE            = 'Transaksi ini tidak bisa diupdate, '+#13+
                                  'karena sudah digunakan oleh transaksi yang lain.';

  {Detail pembayaran massage}
  MSG_JENIS_BAYAR               = 'Tipe bayar harus dipilih.';
  MSG_ALAT_BAYAR                = 'Kas / Bank harus dipilih.';
  MSG_JUMLAH                    = 'Kolom jumlah harus diisi.';
  MSG_NO_DOKUMEN                = 'No dokumen harus diisi.';
  MSG_TGL_CAIR                  = 'Tgl cair harus diisi.';
  MSG_TGL_JT                    = 'Tgl jatuh tempo harus diisi.';
  MSG_DUPLICATE_DOKUMEN         = 'No. dokumen sudah ada.';
  MSG_TGL_JTH_TEMPO             = 'Tgl jatuh tempo harus >= tgl transaksi.';
  MSG_CEK_SALDO                 = 'Saldo tidak mencukupi.';

  MSG_SUPPLIER                  = 'Supplier harus dipilih.';
  MSG_CUSTOMER                  = 'Customer harus dipilih.';
  MSG_RELASI                    = 'Relasi harus dipilih.';
  MSG_NO_VOUCHER                = 'No. Voucher harus diisi.';

  BTN_CAP_OK                    = '&OK';
  BTN_CAP_SAVE                  = '&Simpan';
  BTN_CAP_CANCEL                = '&Batal';
  BTN_CAP_DELETE                = '&Hapus';
  BTN_CAP_CLOSE                 = 'Close';
  BTN_CAP_PRINT                 = '&Cetak';
  BTN_CAP_RESET                 = '&Reset';
  BTN_CAP_ADD                   = '&Tambah';
  BTN_CAP_EDIT                  = '&Ubah';
  BTN_CAP_LIST                  = 'List All';
  BTN_CAP_FIND                  = 'Ca&ri';
  BTN_CAP_DETAIL                = '&Detail';
  BTN_CAP_SHOW_FILTER           = '&Lihat Filter';
  BTN_CAP_HIDE_FILTER           = 'Tutup &Filter';

  {table field constants}

  {Type Projek}
  TYPE_PROJEK_BUILD_BARU  = 'B';
  TYPE_PROJEK_MAINTANANCE = 'M';
  TYPE_PROJEK_PENAMBAHAN  = 'N';
  TYPE_PROJEK_WAITING     = 'W';

  {Type Projek} 
  TYPE_PROJEK_BARU        = 'T';
  TYPE_PROJEK_MTC         = 'F';
  TYPE_PROJEK_NON_MTC     = 'L';

  {Type Progress Job}
  TYPE_PROGRESS_JOB_BARU      = 'B';
  TYPE_PROGRESS_JOB_TEST      = 'T';
  TYPE_PROGRESS_JOB_PERBAIKAN = 'P';
  TYPE_PROGRESS_JOB_LAIN_LAIN = 'L'; 

  {Menu Groups}
  MENU_GROUP_SYSTEM    = 'System';
  MENU_GROUP_FINANCE   = 'Finance';
  MENU_GROUP_INVENTORY = 'Inventory';
  MENU_GROUP_SALES     = 'Sales';
  MENU_GROUP_PURCHASES = 'Purchases';

  {Tanda Setting}
  TANDA_LBH_BESAR = 0;
  TANDA_SAMA_DGN = 1;
  TANDA_LBH_KCL = 2;

  STATUS_TRUE  = 'T';
  STATUS_FALSE = 'F';

  TIPE_BYR_CASH   = 'C';
  TIPE_BYR_CREDIT = 'R';

  TIPE_BAHAN_BAKU   = 'BB'; 
  TIPE_ACCESSORIES  = 'ACC'; 
  TIPE_BARANG_JADI  = 'BJ';

  TIPE_KONTRA_BON_BJ    = 'KBJ';
  TIPE_KONTRA_BON_BBACC = 'KBA';

  KASBANK_KAS     = 'K';
  KASBANK_BANK    = 'B';

  AKUN_ASSET       = 'A';
  AKUN_PASIVA      = 'H';
  AKUN_MODAL       = 'M';
  AKUN_BIAYA       = 'B';
  AKUN_PENDAPATAN  = 'P';
  AKUN_LAIN_LAIN   = 'L';

  {max level constraint}

  MAX_PROG_LEVEL                = 3;

  {document numbering schema}

  DISABLED_BG_COLOR = clBtnFace;
  DISABLED_FG_COLOR = clWindowText;

  {user management}
  LEVEL_OPERATOR    = 1;
  LEVEL_SUPERVISOR  = 2;
  LEVEL_MANAGER     = 3;
  LEVEL_ADMIN       = 4;
  LEVEL_OWNER       = 5;
  LEVEL_DEFAULT     = 9;
  LEVEL_DEVELOPER   = 10;
  PWD_LENGTH        = 8;
  DEVELOPER_NIK     = '~';
  OWNER_NIK         = '0';

  TEXT_LEVEL_OPERATOR    = 'Operator';
  TEXT_LEVEL_SUPERVISOR  = 'Supervisor';
  TEXT_LEVEL_MANAGER     = 'Manager';
  TEXT_LEVEL_ADMIN       = 'Admin';
  TEXT_LEVEL_OWNER       = 'Direksi';
  TEXT_LEVEL_DEVELOPER   = 'Developer';



{global variable}
var
  FOTOPATH: string;
  LOGPATH: string = 'Log\';
  SKINPATH: string = '???\Skin\';
  GLOBAL_REPORT_ID : integer;
  GLOBAL_THEMES : TTMSStyle = tsOffice2003Blue;

{cashier variable}
var
  TAX_RATE : integer = 10; {percent}
  BANK_CODE: string;
  BANK_DAY : integer =  3; {day}
  CANCEL_TOLERANCE: integer = 2; {dlm menit}

  isPOByPass : Boolean;

  SeqSalesIntern, SeqCustIntern : integer;
const
  {$IFDEF ORCL}
    DBisORCL = True;
  {$ENDIF}

  {$IFDEF POSTGRE}
    DBisORCL = False;
  {$ENDIF}
implementation

end.

