unit OracleConnection;
{$DEFINE DEBUG}
{$IFDEF DEBUG}
  {$DEFINE SQL_DEBUG}
{$ENDIF}

interface

uses
  ActiveX, ADOConst, ComObj, ADOInt,
  SysUtils, Classes, DB, ADODB, Variants, Controls,
  Windows, Messages, Graphics, Dialogs, Forms, DBTables,DateUtils,
  BaseGrid, AdvGrid, asgprev, AsgFindDialog, tmsAdvGridExcel;

type
  TSQLOperator = (soGreaterThan, soGreaterThanEqualsTo,soEquals,soLessThan, soLessThanEqualsTo);
  TBooleanOperator = (boTrue,boFalse,boNone);

  //add by j@idol
  TCompareOperator = (coEquals, coInEquals, coInclude);

  arrString = array of string;
  arInteger = array of integer;

  TmyConnection = class(TDataModule)
    ADOConnection: TADOConnection;
    ADOCommand: TADOCommand;
    ADODataSet: TADODataSet;
    advPrint: TAdvPreviewDialog;
    SaveToExcell: TSaveDialog;
    AdvFind: TAdvGridFindDialog;
    AdvExcel: TAdvGridExcelIO;
    ADOQuery1: TADOQuery;
    procedure DataModuleCreate(Sender: TObject);
    procedure DataModuleDestroy(Sender: TObject);
  protected
    { Private declarations }
    Level: integer;
    Log: array of TStringList;
  public
    { Public declarations }

    function StoredProc(AName: string): TParameters;
    procedure ExecStoredProc;
    function AddParamString(AName: string; AString: string; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamNull(AName: string): TParameter;
    function AddParamInteger(AName: string; AInteger: integer; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamFloat(AName: string; AFloat: real; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamDateTime(AName: string; ADateTime: TDateTime; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamDateTime2(AName: string; ADateTime: TDateTime; ADirection: TParameterDirection = pdInput): TParameter;
    function AddParamBoolean(AName: string; ABoolean: boolean; ADirection: TParameterDirection = pdInput): TParameter;
    function GetParamOutput(AName: string): variant;
    function Reconnect : boolean;
    procedure DumpSQL(ASQL: string);
    function ExecSQL(ASQL: string; isCloud : Boolean = true): integer;
    function OpenSQL(ASQL: string; isCLoud : Boolean = true): _RecordSet;

    function IsConnectionException(ErrorMsg : string) : boolean;   
    function BeginSQL: integer;
    procedure EndSQL;
    procedure UndoSQL;
    procedure JroRefreshCache;
    procedure JroCompactDatabase(const Source, Destination: string);
    procedure CompactDB;

    class function OpenRecordSet(ASQL: string): _RecordSet;
  end;

var
  myConnection: TmyConnection;
  ErrorFile : string = 'error.log';
  APathFlasDisk, AUserName, APassword, ASchema : string;
  HostServerCloud : string;
const
  TRUE_STRING = 'T';
  FALSE_STRING = 'F';
  TRUE_VALUE = -1;
  FALSE_VALUE = 0;

  procedure WriteLog(AFileName: string; ALog: string);
  procedure ReadLog(AFileName: string; var ALog: string);

  function ServerNow: TDateTime;
  function FormatSQLDateTimeNow: string;
  function FormatSQLDateNow: string;

  function FormatSQLBoolean(ABoolean: boolean): string;
  function FormatSQLDateTime(ADateTime: TDateTime): string;
  function FormatSQLDate(ADate: TDate): string;
  function FormatSQLTime(ATime: TTime): string;
  function FormatSQLDateTime2(ADateTime: TDateTime):string;
  function FormatSQLString(AString: string): string; overload;
  function FormatSQLString(AChar: char): string; overload;
  function FormatSQLNumber(AInteger: integer): string; overload;
  function FormatSQLNumber(AFloat: real): string; overload;
  function FormatSQLNULL: string;
  function FormatSQLNULL2: string;
  function FormatSQLOperator(operator:TSQLOperator):string;
  function FormatSQLBooleanOperator(operator:TBooleanOperator):string;
  function FormatSQLCompareOperator(operator:TCompareOperator):string;
  
  function BufferToBooleanDef(ABuffer: variant; ADefault: boolean): boolean;
  function BufferToBoolean(ABuffer: variant): boolean;
  function BufferToStringDef(ABuffer: variant; ADefault: string): string;
  function BufferToString(ABuffer: variant): string;
  function BufferToIntegerDef(ABuffer: variant; ADefault: integer): integer;
  function BufferToInteger(ABuffer: variant): integer;
  function BufferToFloatDef(ABuffer: variant; ADefault: real): real;
  function BufferToFloat(ABuffer: variant): real;
  function BufferToDateTimeDef(ABuffer: variant; ADefault: TDateTime): TDateTime;
  function BufferToDateTime(ABuffer: variant): TDateTime;

  procedure SetingPrint(Asg: TAdvStringGrid);
  function CreateNewSeq(ATableNama, ASequenceName: String): Integer;
  function GetCurrentSeq(NamaSeq, Table, ANomor : string) : integer;
  procedure setKoneksiSqlServer(namaDB : string; pass : string; hostServer : string);
  procedure setKoneksiOracle11G(namaDB : string; pass : string; hostServer : string);

var     vApplikasiPath : string;

implementation

uses Subroutines, UGlobalTitle, UengineCloud, StrUtils;
{$R *.dfm}

  procedure WriteLog(AFileName: string; ALog: string);
  var FP: Text;
  begin
    AssignFile(FP,AFileName);
    {$I-} Append(FP); {$I+}
    if (IOResult <> 0) then Rewrite(FP);
    Writeln(FP, ALog);
    CloseFile(FP);
  end;

  procedure ReadLog(AFileName: string; var ALog: string);
  var FP: Text;
  begin
    AssignFile(FP, AFileName); { File selected in dialog }
    Reset(FP);
    Readln(FP, ALog);                        { Read first line of file }
    CloseFile(FP);
  end;

  function ServerNow: TDateTime;
  var sql: string;
  begin
    result := now();
  end;

{ ---------------------------------------------------------------------------- }

procedure TmyConnection.DataModuleCreate(Sender: TObject);
var vApplikasiPath : string;
    path : string;
    AList : TStringList;
begin
  myConnection.CompactDB;
  AList := TStringList.Create;
  vApplikasiPath:= ExtractFilePath(Application.ExeName);
  path := vApplikasiPath+'\profile.dll';
  if FileExists(path) then
    AList.LoadFromFile(path);
  if AList.Count < 1 then begin
    Alert('Cek file profile backup');
    Application.Terminate;
  end else HostServerCloud := decriptNew(AList.Strings[0]);
  AList.Destroy;

  SaveToExcell.InitialDir := GetCurrentDir;

  Level := 0;
  setLength(Log,0);
end;

procedure TmyConnection.DataModuleDestroy(Sender: TObject);
var i: integer;
begin
  for i:=0 to high(Log) do Log[i].Destroy;

  //DestroyGlobalList;
  if (ADOConnection.Connected) then begin
    ADOConnection.Close;
    repeat Application.ProcessMessages; until not ADOConnection.Connected;
  end;
end;

function TmyConnection.BeginSQL: integer;
begin
  Level := ADOConnection.BeginTrans;
  Result := Level;

  //if (DMLFile<>'') then begin

    //writeLog(DMLFile,'-- Begin on level '+IntToStr(Level));
    setLength(Log,Level);
    Log[high(Log)] := TStringList.Create;
  //end;
end;

procedure TmyConnection.EndSQL;
begin
  if (Level=0) then Exit;
  dec(Level); ADOConnection.CommitTrans;

  Log[high(Log)].Destroy;
  setLength(Log,Level);
end;

procedure TmyConnection.UndoSQL;
begin
  if (Level=0) then Exit;
  dec(Level); ADOConnection.RollbackTrans;

    writeLog(ErrorFile,'RollBack data dump:'+sLineBreak+Log[high(Log)].Text);
    Log[high(Log)].Destroy;
    setLength(Log,Level);
end;

procedure TmyConnection.JroRefreshCache;
var
  JetEngine: OleVariant;
begin
  if not ADOConnection.Connected then Exit;
  JetEngine := CreateOleObject('jro.JetEngine');
  JetEngine.RefreshCache(ADOConnection.ConnectionObject);
end;

procedure TmyConnection.JroCompactDatabase(const Source, Destination: string);
var
  JetEngine: OleVariant;
begin
  JetEngine := CreateOleObject('jro.JetEngine');
  JetEngine.CompactDatabase(
    'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + Source,
    'Provider=Microsoft.Jet.OLEDB.4.0;Data Source=' + Destination + ';Jet OLEDB:Engine Type=5');
end;


procedure TmyConnection.CompactDB;
var MdbFileName: string;
    ReopenConnection: Boolean;

  LdbFileName, TempFileName: string;
  FailCount: Integer;
  FileHandle: Integer;
begin
  ReopenConnection := True;
  MdbFileName := 'backup.db';
  TempFileName := ChangeFileExt(MdbFileName, '.temp.mdb');
  if Assigned(ADOConnection) then
  begin
    JroRefreshCache;
    ADOConnection.Close;
  end;
  LdbFileName := ChangeFileExt(MdbFileName, '.ldb');
  if FileExists(LdbFileName) then
    DeleteFile(pAnsiChar(LdbFileName)); 
  if FileExists(TempFileName) then
    if not DeleteFile(pAnsiChar(TempFileName)) then
       RaiseLastOSError;
  FailCount := 0;
  repeat
    FileHandle := FileOpen(MdbFileName, fmShareExclusive);
    try
      if FileHandle = -1 then 
      begin
        Inc(FailCount);
        Sleep(100); // give the database engine time to close completely and unlock
      end
      else
      begin
        FailCount := 0;
        Break; // success
      end;
    finally
      FileClose(FileHandle);
    end;
  until FailCount = 10; // maximum 1 second of attempts
  // compact the db
  JroCompactDatabase(MdbFileName, TempFileName);
  // copy temp file to original mdb and delete temp file on success
  if Windows.CopyFile(PChar(TempFileName), PChar(MdbFileName), False) then
    DeleteFile(pansiChar(TempFileName))
  else
    RaiseLastOSError;
  // reopen ADOConnection
  if Assigned(ADOConnection) and ReopenConnection then begin
    ADOConnection.Open;
  end;
end;


function TmyConnection.ExecSQL(ASQL: string; isCloud : Boolean = true): integer;
var
  vTemp: String;
begin
  if isCloud then begin
    execute_cloud(ASQL);
  end else begin
    try
      ADOConnection.Execute(ASQL,Result);
      try
      except
      end;
      vTemp := Copy(ASQL, 1, 6);
      if UpperCase(vTemp) = 'DELETE' then
    except
      on E: Exception do begin
        WriteLog(ErrorFile,sLineBreak+FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' - '+Application.Title+sLineBreak+'>Message: '+E.Message+sLineBreak+'>Dump: '+ASQL);
        raise;
      end;
    end;
  end;
end;


function TmyConnection.OpenSQL(ASQL: string; isCLoud : Boolean = true): _RecordSet;
label Reload;
var isConProblem : boolean; 
begin
  if isCLoud then
    open_sql_cloud(Result, ASQL)
  else begin
    Reload :      
    isConProblem := false;    
    try
     Result := ADOConnection.Execute(ASQL);
    except
      on E: Exception do begin
        WriteLog(ErrorFile,sLineBreak+FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' - '+Application.Title+sLineBreak+'>Message: '+E.Message+sLineBreak+'>Dump: '+ASQL);
        if IsConnectionException(e.Message) then isConProblem := true    
        else raise;
      end;
    end;
    if (isConProblem) and (Reconnect) then goto Reload;   
  end;
end;

function TmyConnection.IsConnectionException(ErrorMsg : string) : boolean;
var ConErrors : string;
    Code : string;
begin
  ConErrors := 'ORA-12154: TNS:could not resolve the connect identifier specified'+#13+
                'ORA-12198: TNS:could not find path to destination'+#13+
                'ORA-12203: TNS:unable to connect to destination'+#13+
                'ORA-12500: TNS:listener failed to start a dedicated server process'+#13+
                'ORA-12545: TNS:name lookup failure'+#13+
                'ORA-12560: TNS:protocol adapter error'+#13+
                'ORA-03113: end-of-file on communication channel'+#13+
                'ORA-03114: Not connected to ORACLE'+#13+
                'ORA-03114: not connected to ORACLE'+#13+
                'ORA-03135: connection lost contact.'+#13+
                'ORA-12571: TNS:packet writer failure';


   Code := LeftStr(ErrorMsg,Pos(':',ErrorMsg));
   Result := ContainsStr(ConErrors,Code);
end;


procedure TmyConnection.DumpSQL(ASQL: string);
begin
end;

procedure TmyConnection.ExecStoredProc;
var i: integer; ASQL, DirectionStr, ValueStr: string;
begin
  ASQL := '';
  for i:=0 to ADOCommand.Parameters.Count-1 do
    with ADOCommand.Parameters[i] do begin

      DirectionStr:=' => ';
      if (Direction in [pdInput,pdInputOutput]) then begin
        case DataType of
          ftDateTime: ValueStr := FormatSQLDate(Value);
          ftFloat, ftInteger: ValueStr := FormatSQLNumber(Value);
          ftString: ValueStr := FormatSQLString(Value);
          
        end;
        ASQL := ASQL + ', '+Name+DirectionStr+ValueStr+' '
      end{ else
        ASQL := ASQL + ', '+Name+DirectionStr+' '};
    end;
  if (length(ASQL)>0) then delete(ASQL,1,1);

  ASQL := 'EXEC '+ADOCommand.CommandText+'('+ASQL+')';

  try
    ADOCommand.Execute;
  except
    on E: Exception do begin
      WriteLog(ErrorFile,sLineBreak+FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' - '+Application.Title+sLineBreak+'>Message: '+E.Message+sLineBreak+'>Dump: '+ASQL);
      raise;
    end;
  end;
end;

function TmyConnection.StoredProc(AName: string): TParameters;
begin
  ADOCommand.CommandType := cmdStoredProc;
  ADOCommand.CommandText := AName;
  ADOCommand.Parameters.Clear;
  Result := ADOCommand.Parameters;
end;

function TmyConnection.AddParamDateTime(AName: string; ADateTime: TDateTime;
  ADirection: TParameterDirection = pdInput): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftDateTime, ADirection, 0, ADateTime);
end;

function TmyConnection.AddParamFloat(AName: string; AFloat: real;
  ADirection: TParameterDirection = pdInput): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftFloat, ADirection, 0, AFloat);
end;

function TmyConnection.AddParamInteger(AName: string; AInteger: integer;
  ADirection: TParameterDirection = pdInput): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftInteger, ADirection, 0, AInteger);
end;

function TmyConnection.AddParamString(AName: string; AString: string;
  ADirection: TParameterDirection = pdInput): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftString, ADirection, length(AString)+1, AString);
end;

function TmyConnection.AddParamNull(AName: string): TParameter;
begin
  Result := ADOCommand.Parameters.CreateParameter(AName, ftUnknown, pdInput, 0, varNull);
end;

function TmyConnection.AddParamBoolean(AName: string; ABoolean: boolean;
  ADirection: TParameterDirection): TParameter;
begin
  if (ABoolean) then
    Result := AddParamInteger(AName,TRUE_VALUE,ADirection)
  else
    Result := AddParamInteger(AName,FALSE_VALUE,ADirection);
end;

function TmyConnection.GetParamOutput(AName: string): variant;
begin
  Result := ADOCommand.Parameters.ParamValues[AName];
end;
function TmyConnection.Reconnect : boolean;
label back;
begin
  back: 
  try
    ADOConnection.Connected := false;
  except
    ADOConnection.Create(nil);
    ADOConnection.LoginPrompt := false;
  end;
  try                  
    ADOConnection.Create(nil);
    ADOConnection.LoginPrompt := false;
    ADOConnection.Close;
    ADOConnection.open;    
    repeat Application.ProcessMessages; until ADOConnection.Connected;
    Inform('Koneksi ulang berhasil.');
    repeat Application.ProcessMessages; until ADOConnection.Connected;
    Result := true;
  except
    on e : Exception do begin
      Result := false;
      WriteLog(ErrorFile,sLineBreak+FormatDateTime('yy/mm/dd hh:nn:ss',Now)+' - '+Application.Title+sLineBreak+'>Message: '+e.Message);
    end;
  end;
  if not Result then begin
    if not Confirmed('Koneksi program terputus dengan server.'+#13+'Lakukan koneksi ulang?') then
      Application.Terminate
    else goto back;
  end;
end;


{ ---------------------------------------------------------------------------- }

  function FormatSQLDateTimeNow: string;
  begin
    Result := 'SYSDATE';
  end;

  function FormatSQLDateNow: string;
  begin
    Result := 'TRUNC(SYSDATE)';
  end;

  function FormatSQLNULL: string;
  begin
    Result := '';
  end;

  function FormatSQLNULL2: string;
  begin
    Result := 'NULL';
  end;

  function FormatSQLOperator(operator:TSQLOperator):string;
  begin
    case operator of
      soGreaterThan : result := '> ';
      soGreaterThanEqualsTo : result:= '>= ';
      soEquals : result := '= ';
      soLessThan : result:= '< ';
      soLessThanEqualsTo : result:= '<=';
      else result := '= ';
    end;
  end;

  function FormatSQLBooleanOperator(operator:TBooleanOperator):string;
  begin
    case operator of
      boNone : result := '';
      boTrue : Result := 'True';
      boFalse: Result := 'False';
      else result := '= ';
    end;
  end;

  function FormatSQLNumber(AInteger: integer): string; overload;
  begin
    Result := IntToStr(AInteger);
  end;

  function FormatSQLNumber(AFloat: real): string; overload;
  begin
    if DecimalSeparator = '.' then
      Result := FloatToStr(AFloat)
    else
      Result := ReplaceSubStr(FloatToStr(AFloat),DecimalSeparator,'.');
  end;

  function FormatSQLNumber(ANumber: string): string; overload;
  begin
    Result := ReplaceSubStr(ReplaceSubStr(ANumber,ThousandSeparator,''),DecimalSeparator,'.');
  end;

  function FormatSQLString(AString: string): string;
  begin
    Result := QuotedStr(AString);
  end;

  function FormatSQLString(AChar: char): string; overload;
  begin
    Result := QuotedStr(AChar);
  end;

  function FormatSQLDate(ADate: TDate): string;
  begin
    Result := FormatDateTime('"TO_DATE(''"dd/mm/yyyy"'', ''DD/MM/YYYY'')"', ADate);
  end;

  function FormatSQLTime(ATime: TTime): string;
  begin
    Result := FormatDateTime('"TO_DATE(''"hh:nn:ss"'', ''HH24:MI:SS'')"', ATime);
  end;

  function FormatSQLDateTime(ADateTime: TDateTime): string;
  begin
    Result := FormatDateTime('"TO_DATE(''"dd/mm/yyyy hh:nn:ss"'', ''DD/MM/YYYY HH24:MI:SS'')"', ADateTime);
  end;

  function FormatSQLBoolean(ABoolean: boolean): string;
  begin
    if (ABoolean) then Result := FormatSQLNumber(TRUE_VALUE) else Result := FormatSQLNumber(FALSE_VALUE);
  end;
  function FormatSQLDateTime2(ADateTime: TDateTime):string;
  var y,m,d,hh,nn,ss,ms:word;
  begin
    DecodeTime(Now,hh,nn,ss,ms);
    DecodeDate(ADateTime,y,m,d);
    result :=  FormatDateTime('"TO_DATE(''"dd/mm/yyyy hh:nn:ss"'', ''DD/MM/YYYY HH24:MI:SS'')"',EncodeDateTime(y,m,d,hh,nn,ss,ms));
  end;
{ ---------------------------------------------------------------------------- }

  function BufferToBoolean(ABuffer: variant): boolean;
  begin
    Result := BufferToBooleanDef(ABuffer,false);
  end;

  function BufferToBooleanDef(ABuffer: variant; ADefault: boolean): boolean;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := (BufferToInteger(ABuffer)=TRUE_VALUE)
    else
      Result := ADefault;
  end;

  function BufferToString(ABuffer: variant): string;
  begin
    Result := BufferToStringDef(ABuffer,'');
  end;

  function BufferToStringDef(ABuffer: variant; ADefault: string): string;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function BufferToInteger(ABuffer: variant): integer;
  begin
    Result := BufferToIntegerDef(ABuffer,0);
  end;

  function BufferToIntegerDef(ABuffer: variant; ADefault: integer): integer;
  begin
    try
      if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
        Result := ABuffer
      else
        Result := ADefault;
    except
      Result := ADefault;
    end;
  end;

  function BufferToFloat(ABuffer: variant): real;
  begin
    Result := BufferToFloatDef(ABuffer,0);
  end;

  function BufferToFloatDef(ABuffer: variant; ADefault: real): real;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function BufferToDateTime(ABuffer: variant): TDateTime;
  var temp : string;
      jam : string;
      tanggal : string;
      dd, mm, yy, hh, m24, ss : Integer;
  begin
    temp := BufferToString(ABuffer);
    if temp = '' then
      Result := 0
    else begin
      if ContainsText(temp, ':') then begin
        jam := EkstrakString(temp, ' ', 2);
        tanggal := EkstrakString(temp, ' ', 1);
        yy := StrToIntDef(EkstrakString(tanggal, '-', 1), 0);
        mm := StrToIntDef(EkstrakString(tanggal, '-', 2), 0);
        dd := StrToIntDef(EkstrakString(tanggal, '-', 3), 0);
        hh := StrToIntDef(EkstrakString(jam, ':', 1), 0);    
        m24 := StrToIntDef(EkstrakString(jam, ':', 2), 0);
        ss := StrToIntDef(EkstrakString(jam, ':', 3), 0);
        Result := EncodeDateTime(yy, mm, dd, hh, m24, ss, 0);
      end else begin
        tanggal := temp;
        yy := StrToIntDef(EkstrakString(tanggal, '-', 1), 0);
        mm := StrToIntDef(EkstrakString(tanggal, '-', 2), 0);
        dd := StrToIntDef(EkstrakString(tanggal, '-', 3), 0);
        Result := EncodeDate(yy, mm, dd);
      end;

    end;
  end;

  function BufferToDateTimeDef(ABuffer: variant; ADefault:TDateTime): TDateTime;
  begin
    if (VarType(ABuffer)=14) or (not VarIsNull(ABuffer)) then
      Result := ABuffer
    else
      Result := ADefault;
  end;

  function CreateADOObject(const ClassID: TGUID): IUnknown;
  var
    Status: HResult;
    FPUControlWord: Word;
  begin
    asm
      FNSTCW  FPUControlWord
    end;
    Status := CoCreateInstance(ClassID, nil, CLSCTX_INPROC_SERVER or
      CLSCTX_LOCAL_SERVER, IUnknown, Result);
    asm
      FNCLEX
      FLDCW FPUControlWord
    end;
    if (Status = REGDB_E_CLASSNOTREG) then
      raise Exception.CreateRes(@SADOCreateError) else
      OleCheck(Status);
  end;

class function TmyConnection.OpenRecordSet(ASQL: string): _RecordSet;
begin
  Result := CreateADOObject(CLASS_Recordset) as _Recordset;
  Result.Open(ASQL,myConnection.ADOConnection.ConnectionObject,
    adOpenForwardOnly,adLockPessimistic,adCmdText);

end;

function TmyConnection.AddParamDateTime2(AName: string;
  ADateTime: TDateTime; ADirection: TParameterDirection): TParameter;
var y,m,d,hh,nn,ss,ms:word;
begin
  DecodeTime(Now,hh,nn,ss,ms);
  DecodeDate(ADateTime,y,m,d);
  ADateTime := EncodeDateTime(y,m,d,hh,nn,ss,ms);
  Result := ADOCommand.Parameters.CreateParameter(AName, ftDateTime, ADirection, 0, ADateTime);
end;


function FormatSQLCompareOperator(operator:TCompareOperator):string;
begin
  case operator of
    coEquals  : Result := '=';
    coInEquals: Result := '<>';
    coInclude : Result := ' in ';
    else result := '= ';
  end;
end;

procedure SetingPrint(Asg: TAdvStringGrid);
begin
  with Asg.PrintSettings do begin
    Borders           := pbSingle;
    BorderStyle       := psSolid;
    FitToPage         := fpNever;
    Centered          := True;
    HeaderFont.Name   := 'Arial';
    HeaderFont.Size   := 10;
    HeaderFont.Style  := [fsBold];
    Font.Name         := 'Tahoma';
    Font.Size         := 8;

    FooterFont.Color  := clWhite;
    DateFormat        := 'dd/mmm/yyyy';
    Title             := ppTopCenter;
    PagePrefix        := 'Halaman : ';
    HeaderSize        := 200;
    FooterSize        := 200;
    LeftSize          := 75;
    RightSize         := 75;
    ColumnSpacing     := 5;
    RowSpacing        := 5;
    TitleSpacing      := 5;
    Asg.AutoSizeRows(True);
    RepeatFixedRows   := True;
    Centered          := True;
  end;

end;



function CreateNewSeq(ATableNama, ASequenceName: String): Integer;
begin
  Result := getIntegerFromSQL('SELECT MAX('+ASequenceName+') FROM '+ATableNama)+1;
end;

function GetCurrentSeq(NamaSeq, Table, ANomor : string) : integer;
var sql : string;
begin
  sql := 'SELECT MAX('+NamaSeq+') FROM ' + Table ;
  Result := getIntegerFromSQL(sql);
end;

procedure setKoneksiSqlServer(namaDB : string; pass : string; hostServer : string);
begin
  try
    if myconnection.ADOConnection.Connected = True then
      myconnection.ADOConnection.Connected := False;
    myconnection.ADOConnection.ConnectionString := 'Provider=SQLOLEDB.1;Password='+pass+';Persist Security Info=True;User ID=sa;'+
                                                   'Initial Catalog='+namaDB+';Data Source='+hostServer+'\SQLEXPRESS';
    myconnection.ADOConnection.Open;
    repeat Application.ProcessMessages; until myconnection.ADOConnection.Connected;
  except                  
    WriteLog('error koneksi profile.txt', 'Provider=SQLOLEDB.1;Password='+pass+';Persist Security Info=True;User ID=sa;'+
                                                   'Initial Catalog='+namaDB+';Data Source='+hostServer+'\SQLEXPRESS');
    Application.Terminate;
    repeat Application.ProcessMessages; until Application.Terminated;
    raise;
  end;
end;

procedure setKoneksiOracle11G(namaDB : string; pass : string; hostServer : string);
begin
  try
    if myconnection.ADOConnection.Connected = True then
      myconnection.ADOConnection.Connected := False;
    myconnection.ADOConnection.ConnectionString := 'Provider=MSDAORA.1;Password='+pass+';User ID='+namaDB+';Data Source='+hostServer;
    myconnection.ADOConnection.Open;
    repeat Application.ProcessMessages; until myconnection.ADOConnection.Connected;
  except
    Application.Terminate;
    repeat Application.ProcessMessages; until Application.Terminated;
    raise;
  end;
end;

end.
