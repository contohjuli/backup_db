unit UCreateForm;

interface

uses
  OracleConnection, UConst, strUtils, BaseGrid, Grids,
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Buttons, ImgList, Menus, FthCtrls, FthSBar, ExtCtrls, ComCtrls,
  ToolWin, AdvGrid, AdvEdit, Gauges, sPanel, ColCombo, IdTimeServer,
  IdBaseComponent, IdComponent, IdCustomTCPServer, IdDayTimeServer,
  AdvOfficeTabSet, AdvOfficeTabSetStylers, AdvMenus, AdvMenuStylers, AdvToolBar,
  AdvToolBarStylers, AdvAppStyler, MainMenu;

  procedure CreateFormApp(AMenuId: Integer);
  procedure RearrangeFormChild(Form: TForm; AIsIconMax : boolean = true);

implementation

uses
  USystemMenu,Login,
  ugeneral, AdvPanel, AdvGlowButton, AdvOfficePager, 
  AdvOfficeButtons;

procedure RearrangeFormChild(Form: TForm; AIsIconMax : boolean = true);
var  j: integer;
  Panel: TPanel;
  AdvStringGrid: TAdvStringGrid; AdvEdit: TAdvEdit; Button: TButton; //DateTimePicker: TDateTimePicker;
  ALabel : TLabel;
  ASttText: TStaticText;
  AMemo : TMemo;
  APanelAdv : TAdvPanel;
  AGlowButton : TAdvGlowButton;
  AGroupBox : TGroupBox;
  APager : TAdvOfficePager;
  ADate : TDateTimePicker;
  AdvCheckBox : TAdvOfficeCheckBox;
  Acombo : TComboBox;
begin
  if AIsIconMax = true then
    Form.BorderIcons := [biSystemMenu, biMinimize, biMaximize]
  else Form.BorderIcons := [biSystemMenu, biMinimize];
  Form.Color := $00EDD8C7;
    for j:=0 to Form.ComponentCount-1 do
    if (Form.Components[j] is TAdvStringGrid) then begin
      AdvStringGrid := (Form.Components[j] as TAdvStringGrid);
      with AdvStringGrid do begin
        Look:=glSoft;        
        HintColor:=clInfoBk;
        SelectionTextColor:=clBlack;
        URLColor:=clBlue;
        URLShow:=false;
        ScrollColor:=clNone;
        ActiveRowColor:=clInfoBk;
        ActiveRowShow:=false;
        ActiveCellShow:=false;
        Font.Color:=clWindowText;
        Font.Style:=[];
        Font.Size:=8;
        Font.Name:='Tahoma';
//        ActiveCellColor:=$00ACC3C3;
//        ActiveCellColorTo:=$00ACC3C3;
//        ActiveRowColor := $00ACC3C3;
//        ActiveRowColorTo := $00ACC3C3;
        ActiveCellFont.Color:=clWindowText;
        ActiveCellFont.Style:=[];
        ActiveCellFont.Size:=8;
        ActiveCellFont.Name:='Tahoma';
        FixedFont.Color:=clWindowText;
//        FixedFont.Style:=[fsBold];
        FixedFont.Style:=[];
        FixedFont.Size:=8;
        FixedFont.Name:='Tahoma';
        Balloon.BackgroundColor:=clNone;
        Balloon.TextColor:=clNone;
        Balloon.Transparency:=0;
        Bands.PrimaryColor:=$00FAF1E9;//clInfoBk;
        Bands.SecondaryColor:=clWindow;
        Bands.Active:=false;
        SortSettings.IndexColor:=clYellow;
        FloatingFooter.Color:=clBtnFace;
        ControlLook.CheckSize:=15;
        ControlLook.Color:=clBlack;
        ControlLook.CommentColor:=clCream;
        ControlLook.ControlStyle:=csWinXP;
        ControlLook.FixedGradientFrom:=$00F3F7F8;
        ControlLook.FixedGradientTo:=$00ACC3C3;
//        ControlLook.FixedGradientMirrorFrom := $003B8BD5;
//        ControlLook.FixedGradientMirrorTo := $00477CC7;
        ControlLook.RadioSize:=10;
        ControlLook.FlatButton:=false;
        ControlLook.ProgressBorderColor:=clGray;
        ControlLook.ProgressXP:=false;
        Look:=glXP;
        SearchFooter.Color:=$00F3F7F8;
        SearchFooter.ColorTo:=$00ACC3C3;
        GridLineColor:=clSilver;
        Grouping.HeaderColor:=$00F3F7F8;
        Grouping.HeaderColorTo:=$00ACC3C3;
        Grouping.HeaderTextColor:=clWhite;
        Grouping.HeaderUnderline:=true;
        Grouping.HeaderLineColor:=$00215F99;
        Grouping.HeaderLineWidth:=1;
        Grouping.SummaryColor:=$00F3F7F8;
        Grouping.SummaryColorTo:=$00ACC3C3;
        Grouping.SummaryTextColor:=clNone;
        Grouping.SummaryLine:=false;
        Grouping.SummaryLineColor:=clBlue;
        Grouping.SummaryLineWidth:=1;
        ExcelStyleDecimalSeparator := True;
        BackGround.Top:=0;
        BackGround.Left:=0;
        BackGround.Cells:=bcNormal;
        Navigation.AllowClipboardShortCuts := true;        
//        BackGround.Color:=$001595EE;
//        BackGround.ColorTo:=$001595EE;

        Flat := False;
        WordWrap := False;
      end;

      if AdvStringGrid.Tag = -1 then begin
        AdvStringGrid.Bands.Active          := False;
        AdvStringGrid.SortSettings.Show     := False;
      end else begin
        AdvStringGrid.SortSettings.Show     := True;
        AdvStringGrid.SortSettings.Column   := AdvStringGrid.ColCount-1;
        AdvStringGrid.Bands.Active          := True;
        AdvStringGrid.SortSettings.AutoFormat:= True;
        AdvStringGrid.SortSettings.NormalCellsOnly := true;
        AdvStringGrid.SearchFooter.Visible := False;
        AdvStringGrid.SearchFooter.AutoSearch := True;
        AdvStringGrid.SearchFooter.ShowClose := False;
        AdvStringGrid.SearchFooter.ShowMatchCase := False;
      end;
      AdvStringGrid.Font.Name := 'Tahoma';
      AdvStringGrid.DefaultRowHeight := 22;
//      AdvStringGrid.Color := $00CDF1F9;
      AdvStringGrid.MouseActions.AutoSizeColOnDblClick := False;
      AdvStringGrid.Navigation.AutoComboDropSize := true;
      AdvStringGrid.Invalidate;
    end else if (Form.Components[j] is TAdvEdit) then begin
      AdvEdit := (Form.Components[j] as TAdvEdit);
      AdvEdit.DisabledColor := AdvEdit.Color;
      AdvEdit.Font.Name := 'Tahoma';
      advedit.BevelKind := bkSoft;
      AdvEdit.BorderStyle := bsNone;                          
      if AdvEdit.EditType = etMoney then
        AdvEdit.ExcelStyleDecimalSeparator := True;
    end else if (Form.Components[j] is TButton) then begin
      Button := (Form.Components[j] as TButton);
      if (Button.Name='btnCancel') then
        Button.Cancel := true
      else if (Button.Name='btnFilter') then
        Button.Visible  := False
      else if (UpperCase(Button.Name)='BTNPRINT') or (UpperCase(Button.Name)='BTNCETAK') or (UpperCase(Button.Name)='BTNPRIN')then
        Button.Caption  := '&Print'
      else if (UpperCase(Button.Name)='BTNEKSPOR') or (UpperCase(Button.Name)='BTNEKSPORT') or (UpperCase(Button.Name)='BTNEXCEL')then
        Button.Caption  := '&Export';
      Button.Font.Name := 'Tahoma';
    end else if (Form.Components[j] is TPanel) then begin
      Panel := (Form.Components[j] as TPanel);
      Panel.Font.Name := 'Tahoma';
      Panel.ParentBackground := True;
      Panel.ParentColor := True;
      Panel.Color := $00EDD8C7;
    end else if (Form.Components[j] is TLabel) then begin
      ALabel := (Form.Components[j] as TLabel);
    	if ALabel.Tag = 0 then
        ALabel.Transparent := True
      else ALabel.Transparent:= False;
      if UpperCase(ALabel.Name) = 'LBLJURNAL' then
      ALabel.ParentColor := True;
      ALabel.Font.Color := clBlack;
      ALabel.Font.Name := 'Tahoma';
    end else if (Form.Components[j] is TStaticText) then begin
      ASttText := (Form.Components[j] as TStaticText);
      ASttText.Transparent:= True;
      ASttText.ParentColor := True;
      ASttText.Font.Name := 'Tahoma';
    end else if (Form.Components[j] is TGroupBox) then begin
      AGroupBox := (Form.Components[j] as TGroupBox);
      AGroupBox.Font.Name := 'Tahoma';
      AGroupBox.Color := $00EDD8C7;
    end else if (Form.Components[j] is TAdvOfficePager) then begin
      APager := (Form.Components[j] as TAdvOfficePager);
    end else if (Form.Components[j] is TMemo) then begin
      AMemo := (Form.Components[j] as TMemo);
      AMemo.Font.Name := 'Tahoma';
      AMemo.BevelKind := bkSoft;
      AMemo.BorderStyle := bsNone;
    end  else  if (form.Components[j] is TAdvPanel) then begin
      APanelAdv := (form.Components[j] as TAdvPanel);
      APanelAdv.Caption.Color := $00EDD8C7;
      APanelAdv.Caption.ColorTo := $00EDD8C7;
      APanelAdv.Color := $00EDD8C7;
      APanelAdv.Font.Name := 'Tahoma';
      APanelAdv.Caption.Font.Name := 'Tahoma';
    end else if (Form.Components[j] is TMemo) then begin
      AMemo := (Form.Components[j] as TMemo);
      AMemo.Font.Name := 'Tahoma';
    end else if (Form.Components[j] is TComboBox) then begin
      Acombo := (Form.Components[j] as TComboBox);
      Acombo.Font.Name := 'Tahoma';
    end else if (Form.Components[j] is TAdvOfficeCheckBox) then begin
      AdvCheckBox := (Form.Components[j] as TAdvOfficeCheckBox);
      AdvCheckBox.Font.Name := 'Tahoma';
    end else if (Form.Components[j] is TDateTimePicker) then begin
      ADate := (Form.Components[j] as TDateTimePicker);
      ADate.Font.Name := 'Tahoma';
    end  else  if (form.Components[j] is TAdvGlowButton) then begin
      AGlowButton := (form.Components[j] as TAdvGlowButton);
      if (ContainsStr(UpperCase(AGlowButton.Caption),'BATAL'))or
         (UpperCase(AGlowButton.Caption) = 'TUTUP') then begin
        AGlowButton.Cancel := true;
        AGlowButton.ModalResult := mrCancel;
      end;
      AGlowButton.Font.Style := [];
    end;
end;
procedure CreateFormApp(AMenuId: Integer);
begin
end;

end.

