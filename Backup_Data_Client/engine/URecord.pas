unit URecord;

interface
uses OracleConnection,ADOInt;

type
TR_master_project = Record
  seq : integer;
  Kode : string[50];
  nama : string[150];
  batas_backup : integer;
  keterangan : string[255];
  disable_date : TDateTime;
end;


//------------array of records--------
AR_master_project = array of TR_master_project;

implementation

end.
