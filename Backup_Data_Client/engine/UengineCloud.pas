unit UengineCloud;

interface
uses ADOInt, Controls, OracleConnection, UGeneral, Classes, Gauges, Variants, Math, urecord, IdMultipartFormData, IdHTTP, SysUtils,DateUtils;

{UMUM}
function FormatSQLDateTimeNowMysql: string;
function FormatSQLDateMySql(ADate: TDate): string;
function FormatSQLDateTimeMySQl(ADateTime: TDateTime): string;
function getListNameCaseSensitive(list : TStringList; cari : string) : string;
function decriptNew(input : string) : string;
procedure jsonToStringlist(var Alist : TStringList; json : string);
function sqlOracleToMysql(VSql : string) : string;
function replaceHasilJSon(input : string; isAdaNull : Boolean = False) : string;


//untuk akses ke cloud
procedure set_awal_table_temp(buffer : _Recordset; vsql : string);
procedure set_awal_table_temp_baru(buffer : _Recordset; jumField : integer);
function get_field_sql_for_create(sql : string; var tempSelect : string) : string;
procedure open_sql_cloud(var buffer : _Recordset; sql : string);
function execute_cloud(sql : string) : Boolean;
function JSONtoString(json : string; field : integer) : string;
function sqlSelectAddAlias(sql : string) : string;
function sqlSelectAddAliasToSubquery(sql : string) : string;


implementation


uses
  StrUtils, UConst;
var sql : string;

function FormatSQLDateTimeNowMysql: string;
begin
  Result := 'now()';
end;

function FormatSQLDateMySql(ADate: TDate): string;
begin
  Result := FormatSQLString( FormatDateTime('YYYY-MM-DD',ADate));
end;

function FormatSQLDateTimeMySQl(ADateTime: TDateTime): string;
begin
  Result := FormatSQLString( FormatDateTime('YYYY-MM-DD HH:MM:SS ',ADateTime));
end;

function getListNameCaseSensitive(list : TStringList; cari : string) : string;
var i : Integer;
begin
  Result := '';
  for i := 0 to list.count-1 do begin
    if list.Names[i] = cari then begin
      Result := list.ValueFromIndex[i];
      Exit;
    end;
  end;
end;


function decriptNew(input : string) : string;   
var list : TStringList;
    hasil, temp, cari : string;
    i, charke : Integer;
begin
  list := TStringList.create;
  list.Add('e9r=a');
  list.Add('asF=b');
  list.Add('ytH=c');
  list.Add('43y=d');
  list.Add('nSu=e');
  list.Add('fdQ=f');
  list.Add('uyo=g');
  list.Add('jhr=h');
  list.Add('vgb=i');
  list.Add('nm7=j');
  list.Add('cvF=k');
  list.Add('yKV=l');
  list.Add('k93=m');
  list.Add('fdk=n');
  list.Add('vfd=o');
  list.Add('34g=p');
  list.Add('320=q');
  list.Add('eds=r');
  list.Add('ehj=s');
  list.Add('ebv=t');
  list.Add('etr=u');
  list.Add('w94=v');
  list.Add('vcf=w');
  list.Add('pty=x');
  list.Add('j9f=y');
  list.Add('xje=z');
  list.Add('e92=A');
  list.Add('asD=B');
  list.Add('yFH=C');
  list.Add('4Xy=D');
  list.Add('n1u=E');
  list.Add('GdQ=F');
  list.Add('Yyo=G');
  list.Add('jJr=H');
  list.Add('v7b=I');
  list.Add('NM7=J');
  list.Add('c0F=K');
  list.Add('QeV=L');
  list.Add('kg3=M');
  list.Add('jhk=N');
  list.Add('cir=O');
  list.Add('dZi=P');
  list.Add('kR1=Q');
  list.Add('fdk=R');
  list.Add('jLi=S');
  list.Add('f9w=T');
  list.Add('DYx=U');
  list.Add('vde=V');
  list.Add('akb=W');
  list.Add('dsf=X');
  list.Add('gfv=Y');
  list.Add('jul=Z');
  list.Add('cHs=0');
  list.Add('dIe=1');
  list.Add('lgu=2');
  list.Add('mIe=3');
  list.Add('SuM=4');
  list.Add('heN=5');
  list.Add('32x=6');
  list.Add('efr=7');
  list.Add('fd8=8');
  list.Add('fUK=9');

  hasil := '';
  cari := '';
  charke := 0;
  for i := 1 to Length(input) do begin
    if (IsAlphaExist(input[i])) or (IsNumericExist(input[i])) then begin
      charke := charke + 1;
      cari := cari + input[i];
      if (charke = 3) then begin
        temp := getListNameCaseSensitive(list, cari);

        if temp = '' then
          temp := cari;
        hasil := hasil + temp;
        cari := '';
        charke := 0;
      end;
    end else hasil := hasil + input[i];

  end;
  Result := hasil;
  list.Destroy;
end;

procedure jsonToStringlist(var Alist : TStringList; json : string);
var i : Integer;
    pjg : Integer;
    temp : string;
    tempname : string;
    huruf : string;
begin
  pjg := Length(json);
  Alist.Clear;
  temp := '';
  tempname := '';
  for i := 1 to pjg do begin
    huruf := json[i];
    if tempname <> '' then begin
      if tempname = '},{' then begin
        Alist.Add(temp);
        temp := huruf;
        tempname := '';
      end else begin
        tempname := tempname + huruf;
      end;
    end else if huruf = '}' then begin
      tempname := tempname + huruf;
    end else begin
      temp := temp + huruf;
    end;
  end;
  if temp <> '' then
    Alist.Add(temp);
end;

function sqlOracleToMysql(VSql : string) : string;
var i,j : integer;
    SqlTemp : string;
    NamaTabel : string;
    path : string; 
    path2 : string;
    Input : boolean;
    TglString : string;
    IsTime : boolean;
    DateTimeData : TDateTime;
begin
  VSql := TrimAll(VSql);

  VSql := ReplaceText(VSql,'\','\\');
  VSql := ReplaceText(VSql,'^','SYMBOLPOWER');
  VSql := ReplaceText(VSql,'TRUNC(','(');       
  VSql := ReplaceText(VSql,'SYSDATE','NOW()');       
  VSql := ReplaceText(VSql,'TO_DATE(','~');
  VSql := ReplaceText(VSql,'TO_DATE (','~');
  VSql := ReplaceText(VSql,'TO_TIMESTAMP(','~~');
  VSql := ReplaceText(VSql,'TO_TIMESTAMP (','~~');
  VSql := ReplaceText(VSql,', ''DD/MM/YYYY'')','^');  
  VSql := ReplaceText(VSql,', ''DD/MM/YYYY HH24:MI:SS'')','^');  
  VSql := ReplaceText(VSql,',''DD/MM/YYYY'')','^');
  VSql := ReplaceText(VSql,',''DD/MM/YYYY HH24:MI:SS'')','^');

  Input := true;
  IsTime := false;
  DateTimeData := 0;
  path := '';
  for i := 1 to length(VSql) do begin
    if ((VSql[i] = '~')and (VSql[i-1] <> '~') )then begin
      Input := false;
      if i+1 < length(VSql) then begin
        if (VSql[i+1]) = '~' then IsTime := true
        else IsTime := false;
      end else IsTime := false;
      Continue;
    end else if VSql[i] = '^' then begin
      TglString := TrimAll(TglString);
      TglString := ReplaceStr(TglString,'.','/');
      TglString := ReplaceStr(TglString,':','/');
      TglString := ReplaceStr(TglString,' ','/');
      TglString := ReplaceStr(TglString,'''','');  
      TglString := ReplaceStr(TglString,'~','');


      TglString := ReplaceStr(TglString,TimeSeparator,'/');
      TglString := ReplaceStr(TglString,TimeSeparator,'/');
      TglString := ReplaceStr(TglString,DateSeparator,'/');  
      DateTimeData := -1;
      if IsTime then begin
        DateTimeData := EncodeDateTime(StrToInt(EkstrakString(TglString,'/',3)), //YYYY
                                       StrToInt(EkstrakString(TglString,'/',2)), //MM
                                       StrToInt(EkstrakString(TglString,'/',1)), //DD
                                       StrToInt(EkstrakString(TglString,'/',4)), //HH
                                       StrToInt(EkstrakString(TglString,'/',5)), //NN
                                       StrToInt(EkstrakString(TglString,'/',6)), //ss
                                       0,
                                      );

        if DateTimeData > -1 then begin
          path := path + FormatSQLDateTimeMySql(DateTimeData);
        end;
      end else begin
        DateTimeData := EncodeDate(StrToInt(EkstrakString(TglString,'/',3)), //YYYY
                                     StrToInt(EkstrakString(TglString,'/',2)), //MM
                                     StrToInt(EkstrakString(TglString,'/',1)) //DD
                                    );
        DateTimeData := trunc(DateTimeData);
        if DateTimeData > -1 then begin
          path := path + FormatSQLDateMySql(DateTimeData);
        end;
      end;

      TglString := '';
      Input := true;
      Continue;
    end;
    if Input then begin
      path := path + VSql[i]
    end else begin
      TglString := TglString+VSql[i];
    end;
  end;
  VSql := path;


  SqlTemp := UpperCase(VSql); 
  VSql := ReplaceText(VSql,'SYMBOLPOWER','^');

  Result := VSql;
end;


function replaceHasilJSon(input : string; isAdaNull : Boolean = False) : string;
begin
  Result := input;
  Result := ReplaceStr(Result, '^', 'SYMBOLPOWER');

  Result := ReplaceStr(Result, '","', '^');
  if isAdaNull then begin
    Result := ReplaceStr(Result, ',null', '^null');
    Result := ReplaceStr(Result, 'null,', 'null^');
  end;
  Result := ReplaceStr(Result, '[{"', '');
  Result := ReplaceStr(Result, '{"', '');
  Result := ReplaceStr(Result, '"', '"');
  Result := ReplaceStr(Result, '"}]', '');
  Result := ReplaceStr(Result, '"}', '');
  Result := ReplaceStr(Result, '":"', '~');
  if isAdaNull then begin
    Result := ReplaceStr(Result, ':null', '~');
  end;
  Result := ReplaceStr(Result, '"', '');
  Result := ReplaceStr(Result, '\/', '/');
end;

procedure set_awal_table_temp(buffer : _Recordset; vsql : string);
var field, fieldselect : string;
    i : Integer;
begin
  try
    ExecuteSQL('drop table temp', False);
  except

  end;
  vsql := ReplaceText(vsql, '.', '');
  field := get_field_sql_for_create(vsql, fieldselect);

  ExecuteSQL('create table temp ('+field+')', False);

  myConnection.ADOQuery1.SQL.Text := 'select '+fieldselect+' from temp';
end;

procedure set_awal_table_temp_baru(buffer : _Recordset; jumField : integer);
var field, fieldselect : string;
    i : Integer;
begin
  try
    ExecuteSQL('drop table temp', False);
  except

  end;
  field := '';
  fieldselect := '';
  for i := 1 to jumField do begin
    field := field + ifthen(field <> '', ', ')+'field_'+IntToStr(i)+' text';
    fieldselect := fieldselect + ifthen(fieldselect <> '', ', ')+'field_'+IntToStr(i)+' ';
  end;

  ExecuteSQL('create table temp (urutan integer,'+field+')', False);

  myConnection.ADOQuery1.SQL.Text := 'select '+fieldselect+' from temp';

end;

function get_field_sql_for_create(sql : string; var tempSelect : string) : string;
var temp : string;
    field : string;
    i : Integer;
    iskurungbuka : Boolean;
begin
  temp := '';
  iskurungbuka := false;
  tempSelect := '';
  for i := 8 to Length(sql) do begin
    if ContainsText((TrimAll(field)), 'FROM') then begin
      field := trimall(field);
      field := EkstrakString(field, ' ', 1);
      temp := temp + IfThen(temp <> '', ', ')+field+' varchar(255) ';
      tempSelect := tempSelect + IfThen(tempSelect <> '', ', ')+field;
      
  Result := temp;
      exit
    end else if (sql[i] = ',') then begin
      temp := temp + IfThen(temp <> '', ', ')+field+' varchar(255) ';
      tempSelect := tempSelect + IfThen(tempSelect <> '', ', ')+field;
      field := '';
    end else field := field + sql[i];
  end;
  Result := temp;
end;

procedure open_sql_cloud(var buffer : _Recordset; sql : string);
var jsonIsi, jSonPerBaris, objJson : string;
    http : TIdHttp;
    i : Integer;
    seq : string;
    temp : AnsiString;
    listData : tstringlist;
    last_dml_seq  : integer;
    link : string;
    seqAwal, seqAkhir : integer;
    tempGroupseq, GroupKirimSeq : Integer;
    Data : TIdMultiPartFormDataStream;
    j, jumField : integer;
    tempArr : array of TVarRec;
    field, valueisi : string;
    urutan : integer;
begin
  http := TIdHttp.Create(nil);
  listData := tstringlist.Create;
  Data := TIdMultiPartFormDataStream.Create;

                                                                                     
  http.HandleRedirects := true;
  http.ReadTimeout := 40000;
  http.Request.ContentType := 'application/json';
  http.Request.ContentEncoding := 'utf-8';

  sql := sqlSelectAddAlias(sql);
  sql := sqlOracleToMysql(sql);
  Data.AddFormField('sql', sql);

  link := HostServerCloud+'/dml/open_sql';
  jsonIsi := http.post(link, Data);
  jsonIsi := TrimAll(jsonIsi);
  if jsonIsi <> '[]' then begin
    tempGroupseq := 0;
    jsonToStringlist(listData, jsonIsi);
    for i := 0 to listData.Count-1 do begin
      jSonPerBaris := listData.Strings[i];
      jSonPerBaris := replaceHasilJSon(jSonPerBaris, True);
      objJson := jSonPerBaris + '^';
      if i = 0 then begin
        jumField := HitungChar(objJson, '^');
        set_awal_table_temp_baru(buffer, jumField);
        myConnection.AdoQuery1.Open;
      end;
      field := '';
      valueisi := '';
      for j := 1 to jumField do begin
        field := field + ifthen(j <> 1, ', ') + 'field_'+inttostr(j);
        valueisi := valueisi + ifthen(j <> 1, ', ') + FormatSQLString(ReplaceText(JSONtoString(objJson, j), 'SYMBOLPOWER', '^'));
      end;
      field := field + ', urutan';
      valueisi := valueisi + ', '+FormatSQLNumber(i+1);
      sql := 'insert into temp('+field+') values ('+valueisi+')';
      myConnection.ExecSQL(sql, false);
    end;
    buffer := myConnection.OpenSQL('select '+field+' from temp order by urutan', False);

  end else begin
    set_awal_table_temp_baru(buffer, 50);
    buffer := myConnection.OpenSQL('select * from temp order by urutan', False);
  end;                           
  myConnection.ADOQuery1.Close;

  http.Destroy;
  listData.Destroy;
end;

function execute_cloud(sql : string) : Boolean;
var http : TIdHttp;
    link : string;
    Data: TIdMultiPartFormDataStream;
    h, i, j : Integer;
    seq : string;
    list : TStringList;
    sqloracle, sqlMysql, temp, hasil : string;
begin
  sqloracle := sql;
  sqlMysql := sqlOracleToMysql(sql);
  hasil := '';
  list := TStringList.Create;
  Data := TIdMultiPartFormDataStream.Create;
  http := TIdHttp.Create(nil);
  try
    http.HandleRedirects := true;
    http.ReadTimeout := 40000;

    seq := '0';
    i := 0;
    h := 0;
    Data.AddFormField('sql_oracle', sqloracle);
    Data.AddFormField('sql_mysql', sqlMysql);
    http.HTTPOptions := [hoForceEncodeParams];
    http.Request.ContentType := 'application/x-www-form-urlencoded';
    http.HandleRedirects := true;
    link := hostServerCloud+'/dml/insert?';
    hasil := http.Post(link, Data);
    if Containstext(hasil, 'Success') then
      Result := true
    else Result := false;
  except
    Result := false;
  end;     
  Data.Destroy;
  http.Destroy;
  list.Destroy;
end;


function JSONtoString(json : string; field : integer) : string;
var objJson : string;
begin
  objJson := EkstrakString(jSon, '^',field);
  result := EkstrakString(objJson, '~',2);
  Result := ReplaceText(Result, '/r/n', #13);
  Result := ReplaceText(Result, '\r\n', #13);
  Result := ReplaceText(Result, '\r', #13);  
  Result := ReplaceText(Result, '\\', '\');
  Result := ReplaceText(Result, 'SYMBOLBACKSLASH', '\');
end;

function sqlSelectAddAlias(sql : string) : string;
var temp : string;
    field : string;
    i : Integer;
    jumKurBuka : integer;
    isKutip : boolean;
    fieldKe : integer;
    start : Integer;
begin
  temp := '';
  jumKurBuka := 0;
  isKutip := false;
  fieldKe := 1;
  start := 0;
  for i := 8 to Length(sql) do begin
    if (ContainsText((TrimAll(field)), 'FROM')) and (isKutip = false) and (jumKurBuka = 0) then begin
      field := trimall(field);

      if ContainsText(field, ' as ') then begin
        field := ReplaceText(field, 'FROM', '');
        temp := temp + IfThen(temp <> '', ', ')+field
      end else begin
        field := ReplaceText(field, 'FROM', '');
        temp := temp + IfThen(temp <> '', ', ')+field+' as field_'+inttostr(fieldKe);
      end;
      start := i;
      Break;
    end else if (sql[i] = ',') then begin
      if ContainsText(field, ' as ') then
        temp := temp + IfThen(temp <> '', ', ')+field
      else temp := temp + IfThen(temp <> '', ', ')+field+' as field_'+inttostr(fieldKe);
      field := '';
      inc(fieldKe);
    end else if (sql[i] = '''') then begin
      isKutip := not(isKutip);
      field := field + sql[i];
    end else if (sql[i] = '(') and not (iskutip) then begin
      Inc(jumKurBuka);
      field := field + sql[i];
    end else if (sql[i] = ')') and not (iskutip) then begin
      dec(jumKurBuka);
      field := field + sql[i];
    end else field := field + sql[i];
  end;
  Result := 'select '+temp + ' FROM '+Copy(sql, start, length(sql));
  Result := replaceText(result, 'FROMM', 'FROM');                   
  Result := replaceText(result, 'TRUNC(', 'DATE(');
end;

function sqlSelectAddAliasToSubquery(sql : string) : string;
var temp : string;
    i : Integer;
begin
  temp := '';
end;

end.
