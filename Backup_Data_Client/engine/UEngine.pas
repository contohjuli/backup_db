unit UEngine;

interface
uses OracleConnection,URecord,ADOInt, UGeneral, strutils, controls, Classes, UConst, SysUtils;

function Get_seq_by_nama(Anama:string; Field : string = 'nama'):integer;

{master project}
function Get_master_project (VKey : integer) : TR_master_project;
procedure Save_master_project (R_master_project : TR_master_project);
procedure Delete_master_project (VKey : integer);
procedure Update_master_project (R_master_project : TR_master_project);
procedure Get_daftar_Mst_Project(StatAktvasi :TBooleanOperator; var ar_project : AR_master_project);
procedure Get_List_Mst_Project(var alist :tstringlist; Nama : string = '');
procedure Get_List_Mst_Project_aktif_IsLain(var alist :tstringlist); 
procedure Get_List_Mst_Project_aktif(var alist :tstringlist);
function Get_seq_project(Anama:string):TR_master_project;
function Get_nama_project(ASeq:integer):string;
procedure Get_array_nama_project(namaPrj:string;var ar_project : AR_master_project);
function Get_seq_project_by_nama(Anama:string):integer;
procedure GetListMstProject(var Alist : TStringList);
procedure Get_List_Grup_Project_aktif(var alist :tstringlist);

function Is_Data_Exist(Field, TabName, vData : String; vKey : Integer = 0) : boolean;
function Is_Kode_Master_Exist(vKode, tab_name : string; vKey : integer = 0) : boolean;
function Is_Nama_Master_Exist(vNama, tab_name : string; vKey : integer = 0) : boolean;

function Aktivasi_Master(vKey : Integer; DisDate : TDate; tab_name : string) : boolean;
Function KonversiWaktu(Inputdet :Longint) : String;
Function KonversiWaktu_New(Inputdet :Longint) : String;

Function Get_Konversi_Jam_Dan_Hari(AWaktu : longint) : string;
Function Get_Umur_Request(tglMinta, HariIni : tdate) : integer;
function HitungMenit(Time1, Time2 : TDateTime) : longint;

implementation

uses
  USystemMenu, DateUtils, Math, UengineCloud;
var sqL : string;
    buffer : _Recordset;


function Get_seq_by_nama(Anama:string; Field : string = 'nama'):integer;
begin
  sql := 'SELECT seq FROM master_staff WHERE '+field+' = '+FormatSQLString(Anama);
  Result := getIntegerFromSQL(sqL);
end;

{master_project}
function Get_master_project (VKey : integer) : TR_master_project;
var buffer : _Recordset;
begin
  sql := 'SELECT seq, kode, nama, batas_backup, keterangan, disable_date FROM master_project '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    Result.seq := BufferToInteger(buffer.Fields[0].Value);
    Result.kode := BufferToString(buffer.Fields[1].Value);
    Result.nama := BufferToString(buffer.Fields[2].Value);
    Result.batas_backup := BufferToInteger(buffer.Fields[3].Value);
    Result.keterangan := BufferToString(buffer.Fields[4].Value);
    Result.disable_date := BufferToDateTime(buffer.Fields[5].Value);
  end;
  buffer.Close;
end;

procedure Save_master_project (R_master_project : TR_master_project);
begin
  sql := 'INSERT INTO master_project (seq, kode, nama, batas_backup, keterangan, disable_date)  '+
         'VALUES ('+FormatSQLNumber(R_master_project.seq)+', '+
                    FormatSQLString(R_master_project.Kode)+', '+
                    FormatSQLString(R_master_project.nama)+', '+
                    FormatSQLNumber(R_master_project.batas_backup)+', '+
                    FormatSQLString(R_master_project.keterangan)+', '+
                    FormatSQLNULL2+')';
  ExecTransaction3(sql);
end;

procedure Delete_master_project (VKey : integer);
begin
  sql := 'DELETE FROM master_project '+
         'WHERE seq = '+FormatSQLNumber(VKey);
  ExecTransaction3(sql);
end;

procedure Update_master_project (R_master_project : TR_master_project);
begin
  sql := 'UPDATE master_project SET '+
                'kode = '+FormatSQLString(R_master_project.Kode)+', '+
                'nama = '+FormatSQLString(R_master_project.nama)+', '+
                'keterangan = '+FormatSQLString(R_master_project.keterangan)+', '+
                'batas_backup = '+FormatSQLNumber(R_master_project.batas_backup)+' '+
         'WHERE seq = '+FormatSQLNumber(R_master_project.seq);
 ExecTransaction3(sql);
end;

procedure Get_daftar_Mst_Project(StatAktvasi :TBooleanOperator; var ar_project : AR_master_project);
var i:integer;
begin
  sql := 'SELECT seq, kode, nama, batas_backup, keterangan, disable_date '+
         'FROM master_project where seq <> 0 ';

  case StatAktvasi of
    bonone : sql := sql + ' ORDER BY nama ';
    botrue : sql := sql + ' and disable_date is null ORDER BY nama ';
    bofalse : sql := sql + ' and disable_date is not null ORDER BY nama ';
  end;

  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    setlength(ar_project,buffer.RecordCount);
    for i := 0 to buffer.RecordCount-1 do begin
      ar_project[i].seq := BufferToInteger(buffer.Fields[0].Value);
      ar_project[i].Kode := BufferToString(buffer.Fields[1].Value);
      ar_project[i].nama := BufferToString(buffer.Fields[2].Value);       
      ar_project[i].batas_backup := BufferTointeger(buffer.Fields[3].Value);
      ar_project[i].keterangan := BufferToString(buffer.Fields[4].Value);
      ar_project[i].disable_date := BufferToDateTime(buffer.Fields[5].Value);
      buffer.MoveNext;
    end;
  end else begin
       Ar_project := nil;
  end;

  buffer.Close;
end;

procedure Get_List_Mst_Project(var alist :tstringlist; Nama : string = '');
var i: integer;
begin
  AList.Clear;
  sqL := 'SELECT seq, nama FROM master_project '+IfThen(Nama <> '', ' where nama Like '+FormatSQLString('%'+Nama+'%'))+' '+
         'order by nama ';

  buffer := myConnection.OpenSQL(sqL);
  for i := 0 to buffer.RecordCount-1 do begin
    AList.Append(BufferToString(buffer.Fields[0].Value)+'='+
                 BufferToString(buffer.Fields[1].Value));
    buffer.MoveNext;
  end;
  buffer.Close;
end;

procedure Get_List_Mst_Project_aktif(var alist :tstringlist);
var i: integer;
begin
  AList.Clear;
  sqL := 'SELECT seq, nama FROM master_project WHERE disable_date is null ORDER BY nama';
  buffer := myConnection.OpenSQL(sqL);
  for i := 0 to buffer.RecordCount-1 do begin
    AList.Append(BufferToString(buffer.Fields[0].Value)+'='+
                 BufferToString(buffer.Fields[1].Value));
    buffer.MoveNext;
  end;
  buffer.Close;
end;

function Get_seq_project(Anama:string):TR_master_project;
begin
  sql := 'select seq from master_project where nama= '+FormatSQLString(Anama);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    Result.seq := BufferToInteger(buffer.Fields[0].Value);
  end;
  buffer.Close;
end;

function Get_nama_project(ASeq:integer):string;
begin
  sql := 'select nama from master_project where seq= '+FormatSQLNumber(Aseq);
  buffer := myConnection.OpenSQL(sql);
  if buffer.RecordCount > 0 then begin
    Result := BufferToString(buffer.Fields[0].Value);
  end;
  buffer.Close;
end;

procedure Get_array_nama_project(namaPrj: string; var ar_project : AR_master_project);
var i:integer;
begin
  sql := 'SELECT nama FROM master_project WHERE disable_date is null'+
         IfThen(namaPrj <> '', ' AND nama = '+FormatSQLString(namaPrj))+' ORDER BY nama';
  buffer := myConnection.OpenSQL(sql);
  setlength(ar_project,buffer.RecordCount);
   for i := 0 to buffer.RecordCount-1 do begin
    ar_project[i].nama := BufferToString(buffer.Fields[0].Value);
    buffer.MoveNext;
  end;
  buffer.Close;
end;

function Get_seq_project_by_nama(Anama:string):integer;
begin
  sqL := 'SELECT seq FROM master_project WHERE nama = '+FormatSQLString(Anama);
  Result := getIntegerFromSQL(sqL);
end;

procedure GetListMstProject(var Alist : TStringList);
var i : integer;
begin
  Sql := 'Select seq, nama, No_telp_1 from master_project where disable_date is null';
  Alist.Clear;
  buffer := myConnection.OpenSQL(sqL);
  for i := 0 to buffer.RecordCount-1 do begin
    AList.Append(BufferToString(buffer.Fields[0].Value)+'='+
                 BufferToString(buffer.Fields[1].Value)+'#'+
                 BufferToString(buffer.Fields[2].Value));
    buffer.MoveNext;
  end;
  buffer.Close;
end;

procedure Get_List_Grup_Project_aktif(var alist :tstringlist);
var i: integer;
begin
  AList.Clear;
  sql := 'SELECT Grup_seq, Nama FROM master_project WHERE disable_date is null and Grup_seq = Seq ORDER BY nama';
  buffer := myConnection.OpenSQL(sqL);
  for i := 0 to buffer.RecordCount-1 do begin
    AList.Append(BufferToString(buffer.Fields[0].Value)+'='+
                 BufferToString(buffer.Fields[1].Value));
    buffer.MoveNext;
  end;
  buffer.Close;
end;

function Is_Data_Exist(Field, TabName, vData : String; vKey : Integer = 0) : boolean;
begin
  sqL := 'SELECT COUNT(*) FROM '+TabName+' WHERE '+Field+' = '+IfThen(vKey <> 0, FormatSQLNumber(vKey), FormatSQLString(vData));
  Result := getIntegerFromSQL(sqL) > 0;
end;

function Is_Kode_Master_Exist(vKode, tab_name : string; vKey : integer = 0) : boolean;
begin
  sql := 'SELECT COUNT(*) FROM '+tab_name+' WHERE kode = '+FormatSQLString(vKode)+
         IfThen(vKey <> 0, ' AND seq <> '+FormatSQLNumber(vKey));
  Result := getIntegerFromSQL(sqL) > 0;
end;

function Is_Nama_Master_Exist(vNama, tab_name : string; vKey : integer = 0) : boolean;
begin
  sql := 'SELECT COUNT(*) FROM '+tab_name+' WHERE nama = '+FormatSQLString(vNama)+
         IfThen(vKey <> 0, ' AND seq <> '+FormatSQLNumber(vKey));
  Result := getIntegerFromSQL(sqL) > 0;
end;

function Aktivasi_Master(vKey : Integer; DisDate : TDate; tab_name : string) : boolean;
begin
  if DisDate = 0 then
    sql := 'UPDATE '+tab_name+' SET Disable_Date = ' + FormatSQLDate(ServerNow) +
           ' WHERE Seq = ' + FormatSQLNumber(VKey)
  else sql := 'UPDATE '+tab_name+' SET Disable_Date = ' + FormatSQLNULL2 +
              ' WHERE Seq = ' + FormatSQLNumber(VKey);
  Result := ExecAktivasi(sql, DisDate <> 0);
end;

function KonversiWaktu(Inputdet: Longint): String;
var Hari, jam, Menit,  sisa : Longint;  
begin
  result := '';
  if (InputDet >= 1) And (InputDet < 60) then begin
    Menit := Inputdet mod 60;
    result := intToStr(Menit)+' Menit'
  end else if (InputDet >= 60) AND (InputDet < 480) then begin
    jam   := Inputdet Div 60;
    sisa  := Inputdet Mod 60;
    Menit := Sisa;
    Result := intToStr(jam)+' Jam '+IfThen(Menit <> 0,  IntToStr(Menit)+' Menit');
  end else if (InputDet >= 480) then Begin
    hari := Inputdet div 480;
    Sisa := Inputdet mod 480;
    jam := Sisa div 60;
    menit := Sisa mod 60;
    Result := intToStr(Hari)+' Hari '+IfThen(jam <> 0, IntToStr(jam)+ ' Jam ')+IfThen(menit <> 0, IntToStr(Menit)+' Menit');
  End;
end;

Function KonversiWaktu_New(Inputdet :Longint) : String;
var Hari, jam, Menit,  sisa : Longint;  
begin
  result := '';
  if (InputDet >= 1) And (InputDet < 60) then begin
    Menit := Inputdet mod 60;
    result := intToStr(Menit)+' Menit'
  end else if (InputDet >= 60) {AND (InputDet < 1440) }then begin
    jam   := Inputdet Div 60;
    sisa  := Inputdet Mod 60;
    Menit := Sisa;
    Result := intToStr(jam)+' Jam '+IfThen(Menit <> 0,  IntToStr(Menit)+' Menit');
  End;
end;

procedure Get_List_Mst_Project_aktif_IsLain(var alist :tstringlist);
var i: integer;
begin
  AList.Clear;
  sqL := 'SELECT seq, nama, is_lain FROM master_project WHERE disable_date is null ORDER BY nama';
  buffer := myConnection.OpenSQL(sqL);
  for i := 0 to buffer.RecordCount-1 do begin
    AList.Append(BufferToString(buffer.Fields[0].Value)+'='+
                 BufferToString(buffer.Fields[1].Value)+'#'+
                 BufferToString(buffer.Fields[2].Value));
    buffer.MoveNext;
  end;
  buffer.Close;
end;

Function Get_Konversi_Jam_Dan_Hari(AWaktu : longint) : string;
var AHari : Longint;
    Sisa, jam : Double;
    vSisa : string;
    Menit : integer;
begin
  Ahari := Trunc(AWaktu/480);
  Sisa  := (AWaktu - (Ahari * 480));
  jam   := (Sisa / 60);
  vSisa := FloatToStr(sisa);
  Menit := StrToInt(vSisa) mod 60;
  Result := KonversiWaktu_New(AWaktu)+IfThen(AHari <> 0, ' | '+IntToStr(Ahari)+' Hari '+IfThen(jam > 0, FloatToStrFmt(Trunc(jam))+','+IntToStr(Menit)+' Jam'));
end;

Function Get_Umur_Request(tglMinta, HariIni : tdate) : integer;
var Umur, jmlLibur : integer;
    i : integer; hari : Word;
begin
  Umur := DaysBetween(HariIni,tglMinta);
  jmlLibur := 0;
  for i := 1 to Umur do begin
    hari := DayOfTheWeek(tglMinta+i);
    if (hari = DaySaturday) or (hari = DaySunday) then
      jmlLibur := jmlLibur + 1;
  end;
  Result := Umur - jmlLibur;
end;

function HitungMenit(Time1, Time2 : TDatetime) : longint;
var h1,h2,m1,m2,s1,s2,ms1,ms2,hari,Y,M : word;
    min1, min2 : word;
    Dt : TDateTime;
begin
  DecodeTime(Time1,h1,m1,s2,ms1); DecodeTime(Time2,h2,m2,s2,ms2);
  hari := DateUtils.DayOfTheYear(Time2)-DateUtils.DayOfTheYear(Time1);
  min1 := (h1 * 60) + m1;
  min2 := (h2 * 60) + m2;
  if min2 < min1 then begin
    Result := (hari * 1440) - (min1-min2);
  end else begin
    Result := (min2-min1)+(hari * 1440);
  end;
end;

end.


