unit MasterProjectInput;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, AdvEdit, ExtCtrls, Urecord, AdvGlowButton,
  AdvOfficeButtons;

type
  TfrmMasterProjectInput = class(TForm)
    Panel1: TPanel;
    lblKodeRelasi: TLabel;
    txtKode: TAdvEdit;
    Label5: TLabel;
    Label17: TLabel;
    mmKeterangan1: TMemo;
    Label22: TLabel;
    txtBatasBackup: TAdvEdit;
    AdvGlowButton1: TAdvGlowButton;
    AdvGlowButton2: TAdvGlowButton;
    Label10: TLabel;
    txtNama: TAdvEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormShow(Sender: TObject);
    procedure btnSimpanClick(Sender: TObject);
    procedure btnBatalClick(Sender: TObject);
  private
    { Private declarations }
    editMode : boolean;
    vSeq : Integer;
    statsimpan : boolean;
    procedure initform;
    function isvalid : boolean;
    function isSaved : boolean;
    procedure load();
  public
    { Public declarations }
    function execute(seq:integer): boolean;
  end;

var
  frmMasterProjectInput: TfrmMasterProjectInput;

implementation

uses MasterProjectRekap,ugeneral, OracleConnection, uengine, uconst, StrUtils, 
  MainMenu;

{$R *.dfm}

function TfrmMasterProjectInput.issaved : boolean;
var dataProject : TR_master_project;
begin
  try
    myConnection.BeginSQL;
    dataProject.seq  := vseq;
    dataProject.Kode := TrimAll(txtKode.Text);
    dataProject.Nama := TrimAll(txtNama.Text);
    dataProject.batas_backup := txtBatasBackup.IntValue;
    dataProject.keterangan := TrimAll(mmKeterangan1.Lines.Text);
    if editMode then  begin
      Delete_master_project (dataProject.seq);
      Save_master_project (dataProject);
    end else Save_master_project (dataProject);

    myConnection.EndSQL;
    result := true;
  except
    myConnection.UndoSQL;
    Inform(MSG_UNSUCCESS_SAVING);
    result := false;
  end;
end;


procedure TfrmMasterProjectInput.load();
var dataProject : TR_master_project;
begin
  dataProject := Get_master_project(vSeq);
  txtKode.Text          := dataProject.Kode;
  txtNama.Text          := dataProject.Nama;
  txtBatasBackup.IntValue    := dataProject.batas_backup;
  mmKeterangan1.Lines.Text    := dataProject.keterangan;
end;

function TfrmMasterProjectInput.isvalid : boolean;
begin
  result := false;
  if txtKode.Text = '' then begin
     Inform('Kode belum diisi.');
     txtKode.SetFocus;
     exit;
  end else if UEngine.Is_Kode_Master_Exist(TrimAll(txtKode.Text),'master_project',vseq) then begin
     Inform('Kode sudah ada.');
     txtNama.SetFocus;
     exit;
  end else if txtNama.Text = '' then begin
     Inform('Nama belum diisi.');
     txtNama.SetFocus;
     exit;
  end else if UEngine.Is_Nama_Master_Exist(TrimAll(txtNama.Text),'master_project',vseq) then begin
     Inform('Nama sudah ada.');
     txtNama.SetFocus;
     exit;
  end else result := true;
end;

procedure TfrmMasterProjectInput.FormShow(Sender: TObject);
begin
  txtNama.SetFocus;
end;

procedure TfrmMasterProjectInput.initform;
var i : integer;
begin
  txtNama.Text          := '';
  txtKode.Text          := '';
  txtBatasBackup.IntValue := 0;
  mmKeterangan1.Lines.Text := '';
end;

procedure TfrmMasterProjectInput.btnBatalClick(Sender: TObject);
begin
  ModalResult := mrCancel;
end;

procedure TfrmMasterProjectInput.btnSimpanClick(Sender: TObject);
begin
  if (isvalid) and Confirmed(MSG_SAVE_CONFIRMATION) then begin
    if (isSaved) then begin
       if EditMode then begin
         ModalResult := mrOk ;
       end else
       if (not editMOde) then begin
          if Confirmed(MSG_ADD_DATA) then begin
            statsimpan := true;
            initform;
            txtNama.SetFocus;
          end else ModalResult := mrOk ;
       end;
    end;
  end;
end;

function TfrmMasterProjectInput.execute(seq:integer) : boolean;
begin
  statsimpan := false;
  vSeq := seq;
  initform;
  Position := poDesktopCenter;
  editMode := (seq <> 0);
  if editMode then begin
     caption := 'Edit Master Project';
     load;
  end else caption := 'Tambah Master Project';
  Run(Self, true);
  result := ModalResult = mrOk;
end;

end.
