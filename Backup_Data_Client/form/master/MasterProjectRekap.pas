unit MasterProjectRekap;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ComCtrls, StdCtrls, AdvEdit, ExtCtrls, Grids, BaseGrid, AdvGrid, OracleConnection ,
  Gauges, AdvGlowButton, AdvObj, AdvPanel;

type
  TfrmMasterProjectRekap = class(TForm)
    MainPanel: TPanel;
    pnlFilter: TPanel;
    btnLoad: TAdvGlowButton;
    btnReset: TAdvGlowButton;
    Panel3: TPanel;
    Label3: TLabel;
    Label4: TLabel;
    Gauge1: TGauge;
    btnTambah: TAdvGlowButton;
    btnEdit: TAdvGlowButton;
    btnAktivasi: TAdvGlowButton;
    asgRekap: TAdvStringGrid;
    btnFilter: TAdvGlowButton;
    chbSFooter: TCheckBox;
    Gauge2: TGauge;
    btnCetak: TAdvGlowButton;
    btnEkspor: TAdvGlowButton;
    Shape3: TShape;
    Shape1: TShape;
    PnlJenisSaldo: TAdvPanel;
    cmbAktivasi: TComboBox;
    procedure btnFilterClick(Sender: TObject);
    procedure btnTambahClick(Sender: TObject);
    procedure btnEditClick(Sender: TObject);
    procedure btnLoadClick(Sender: TObject);
    procedure btnResetClick(Sender: TObject);
    procedure btnAktivasiClick(Sender: TObject);
    procedure asgRekapGetAlignment(Sender: TObject; ARow, ACol: Integer;
      var HAlign: TAlignment; var VAlign: TVAlignment);
    procedure chbSFooterClick(Sender: TObject);
    procedure btnEksporClick(Sender: TObject);
    procedure btnCetakClick(Sender: TObject);
    procedure asgRekapClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure asgRekapDblClickCell(Sender: TObject; ARow, ACol: Integer);
    procedure FormShow(Sender: TObject);
    procedure asgRekapGetCellColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
    procedure asgRekapGetCellPrintColor(Sender: TObject; ARow, ACol: Integer;
      AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
  private
    { Private declarations }
    aNama, a_alamat, aPeriode : boolean;
    Astatus, isNewPrj: tbooleanOperator;
    ListGrup : TStringList;
    GrupSeq: integer;
    procedure arrangeColSize;
    procedure setfilter;
    procedure setgrid;
    procedure load;
    procedure initform;
  public
    { Public declarations }
    procedure execute;
  end;

var
  frmMasterProjectRekap: TfrmMasterProjectRekap;

implementation

uses masterProjectInput,UEngine, URecord, ugeneral, USystemMenu, UConst, MainMenu;
const colNo          = 0;
      colKode        = 1;
      colNama        = 2;
      colBatasBackup = 3;
      colKeterangan  = 4;
      colAktif       = 5;
      colSeq         = 6;

{$R *.dfm}

procedure TfrmMasterProjectRekap.setfilter;
begin
  case cmbAktivasi.ItemIndex  of
    0: Astatus := bonone;
    1: Astatus := botrue;
    2: Astatus := bofalse;
  end;
end;

procedure TfrmMasterProjectRekap.setgrid;
begin
  asgRekap.ClearNormalCells;
  asgRekap.RowCount := 2;
  asgRekap.ColCount := 8;
  asgRekap.FixedCols:= 1;
  asgRekap.FixedRows:= 1;
  arrangeColSize;
end;

procedure TfrmMasterProjectRekap.arrangeColSize;
begin
  asgRekap.FixedFont.Style := [fsBold];
  asgRekap.AutoSizeColumns(true,3);
  if GlobalSystemUser.AccessLevel <> LEVEL_DEVELOPER then begin
    asgRekap.ColWidths[colAktif] := 0;
    asgrekap.ColWidths[colSeq]   := 0;
  end;
  asgRekap.AutoNumberCol(colNo);
end;

procedure TfrmMasterProjectRekap.load;
var daftarProject : AR_master_project;
    i, row : integer;
begin
  setgrid;
  setFilter;
  Get_daftar_Mst_Project(astatus, daftarProject);
  Gauge2.Show;
  for i := 0 to Length(daftarProject)-1 do begin
    Gauge2.Progress:= round((i+1)/(Length(daftarProject) +1)*100);
    if i>0 then asgRekap.AddRow;
    row := asgRekap.RowCount-1;
    asgRekap.Cells[colKode,row]     := daftarProject[i].Kode;
    asgRekap.Cells[colNama,row]     := daftarProject[i].nama;
    asgRekap.Ints[colBatasBackup,row]     := daftarProject[i].batas_backup;
    asgRekap.Cells[colKeterangan,row]     := daftarProject[i].keterangan;
    asgRekap.ints[colSeq,row]       := daftarProject[i].seq;
    if daftarProject[i].disable_date <> 0 then begin
      asgRekap.Dates[colAktif,row]  := daftarProject[i].disable_date;
      UGeneral.SetCellFontColor(asgRekap, clRed, 0, Row);
    end;
    asgRekap.FontColors[colNo,row]:= clBlack;
  end;
  Gauge2.Hide;
  arrangeColSize;
end;

procedure TfrmMasterProjectRekap.initform;
var i : integer;
begin
  chbSFooter.Checked := false;
  cmbAktivasi.ItemIndex := 1;
  setgrid;
end;

procedure TfrmMasterProjectRekap.btnTambahClick(Sender: TObject);
begin
  Application.CreateForm(TfrmMasterProjectInput, frmMasterProjectInput);
  if frmMasterProjectInput.execute(0) then
    btnLoad.Click;
end;

procedure TfrmMasterProjectRekap.chbSFooterClick(Sender: TObject);
begin
  asgRekap.SearchFooter.Visible := chbSFooter.Checked;
end;

procedure TfrmMasterProjectRekap.execute;
begin
  caption := 'Rekap Master Project';
  ListGrup := TStringList.Create;
  initform;
  Position := poDesktopCenter;
  btnLoad.Click;
  Run(Self);
end;

procedure TfrmMasterProjectRekap.FormShow(Sender: TObject);
begin
  Execute;
end;

procedure TfrmMasterProjectRekap.asgRekapClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if(asgRekap.cells[colAktif,asgRekap.Row]<>'') or (asgRekap.cells[colNama,asgRekap.Row]='') then
    btnEdit.enabled  := false
  else btnEdit.Enabled := (GlobalSystemUser.AccessLevel >= LEVEL_ADMIN);
end;

procedure TfrmMasterProjectRekap.asgRekapDblClickCell(Sender: TObject; ARow,
  ACol: Integer);
begin
  if (arow > 0) and (GlobalSystemUser.AccessLevel >= LEVEL_ADMIN) then btnEdit.Click;
end;

procedure TfrmMasterProjectRekap.asgRekapGetAlignment(Sender: TObject;
  ARow, ACol: Integer; var HAlign: TAlignment; var VAlign: TVAlignment);
begin
  if arow=0 then  HAlign := taCenter
  else if acol in [colNo, colBatasBackup] then HAlign := taRightJustify;
end;

procedure TfrmMasterProjectRekap.asgRekapGetCellColor(Sender: TObject; ARow,
  ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ACol > 0) and (arow > 0) and (asgRekap.Cells[colAktif, ARow] <> '') then
    AFont.Color := clRed
  else AFont.Color := clBlack;
end;

procedure TfrmMasterProjectRekap.asgRekapGetCellPrintColor(Sender: TObject;
  ARow, ACol: Integer; AState: TGridDrawState; ABrush: TBrush; AFont: TFont);
begin
  if (ACol > 0) and (arow > 0) and (asgRekap.Cells[colAktif, ARow] <> '') then
    AFont.Color := clRed
  else AFont.Color := clBlack;
end;

procedure TfrmMasterProjectRekap.btnAktivasiClick(Sender: TObject);
var Aproject : TR_master_project;
begin
  if asgRekap.Cells[ColAktif, asgRekap.Row]= '' then
    AProject.disable_date := 0
  else
    AProject.disable_date := asgRekap.Dates[ColAktif, asgRekap.Row];
  if Confirmed('Data akan di aktif/non aktif kan ?') then begin
    UEngine.Aktivasi_Master(asgRekap.Ints[ColSeq, asgRekap.Row], AProject.disable_date, 'master_project');
    Setgrid;
    Load;
  end;
end;

procedure TfrmMasterProjectRekap.btnCetakClick(Sender: TObject);
begin
  myConnection.advPrint.Grid:= asgRekap;
  asgRekap.PrintSettings.TitleLines.Clear;
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.PrintSettings.TitleLines.Add('Rekap Master Project');
  asgRekap.PrintSettings.TitleLines.Add('');
  asgRekap.ColumnSize.Stretch := False;
  asgRekap.ColCount := 6;
  SetingPrint(asgRekap);
  myConnection.advPrint.Execute;
  asgRekap.ColCount := 8;
  asgRekap.ColWidths[colSeq]:=0;
  asgRekap.ColWidths[colAktif]:=0;
  asgRekap.ColumnSize.Stretch := True;
end;

procedure TfrmMasterProjectRekap.btnEditClick(Sender: TObject);
var seq:integer;
begin
   if (asgRekap.Ints[colSeq,asgRekap.Row] <> 0) and (asgRekap.cells[colAktif,asgRekap.Row]='') then  begin
    seq := asgRekap.Ints[colSeq,asgRekap.Row] ;
    Application.CreateForm(TfrmMasterProjectInput, frmMasterProjectInput);
    if frmMasterprojectInput.Execute(seq) then
      btnLoad.Click;
  end;
end;

procedure TfrmMasterProjectRekap.btnEksporClick(Sender: TObject);
begin
  asgExportToExcell(asgRekap, myConnection.SaveToExcell);
end;

procedure TfrmMasterProjectRekap.btnFilterClick(Sender: TObject);
begin
  SetFilterSizeAdv(pnlFilter,btnFilter, 100);
end;

procedure TfrmMasterProjectRekap.btnLoadClick(Sender: TObject);
begin
 load;
  if asgRekap.Ints[colSeq,asgRekap.Row] <> 0 then  begin
    if asgRekap.cells[colAktif,asgRekap.Row] <> '' then
        btnEdit.enabled  := false
    else  btnEdit.enabled  := true;
    btnAktivasi.Enabled := (GlobalSystemUser.AccessLevel >= LEVEL_ADMIN);
    btnEkspor.Enabled := true;
    btnCetak.Enabled := true;
  end;
  asgRekap.OnClickCell(asgRekap, 1, colNama);
end;

procedure TfrmMasterProjectRekap.btnResetClick(Sender: TObject);
begin
  initform;
  asgRekap.OnClickCell(asgRekap, 1, colNama);
  btnAktivasi.enabled := false;
end;

end.
