unit Menu;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, AdvEdit, DateUtils, AdvDateTimePicker, AdvCombo, IdMultipartFormData;

type
  TfrmMenu = class(TForm)
    btnEncode: TButton;
    btnDecode: TButton;
    txtHostCloud: TAdvEdit;
    Label2: TLabel;
    Label3: TLabel;
    mmPathFlashdisk: TMemo;
    cmbTipeDB: TAdvComboBox;
    Label1: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    mmNamaDB: TMemo;
    btnHelp: TButton;
    Label6: TLabel;
    txtHari: TAdvEdit;
    Label7: TLabel;
    txtHariCloud: TAdvEdit;
    cmbNamaProject: TAdvComboBox;
    btnRefresh: TButton;
    procedure FormCreate(Sender: TObject);
    procedure btnEncodeClick(Sender: TObject);
    procedure btnDecodeClick(Sender: TObject);
    procedure btnHelpClick(Sender: TObject);
    procedure cmbTipeDBKeyPress(Sender: TObject; var Key: Char);
    procedure btnRefreshClick(Sender: TObject);
  private
    { Private declarations }
    function NumberToString(Number,Panjang : integer) : string;
    procedure sqltonamevaluelistOnline(var AStringList: TStringList; ASQL: string);
    procedure get_json_list_from_url_by_post(var AList : tstringlist; url : string; Data : TIdMultiPartFormDataStream);
    procedure jsonToStringlist(var Alist : TStringList; json : string);
    function replaceHasilJSon(input : string; isAdaNull : Boolean = False) : string;
    function JSONtoString(json : string; field : integer) : string;
  public
    { Public declarations }
  end;

var
  frmMenu: TfrmMenu;

implementation

uses
  UGeneral, StrUtils, IdHTTP;

{$R *.dfm}

procedure TfrmMenu.btnDecodeClick(Sender: TObject);
var tempint : integer;
    AHasil, tempstr : string;
  AListHasil: TStringList;
  APath, AUser, APaswd, AServis: string;
  i: Integer;
  isPath : Boolean;
begin
  AListHasil:= TStringList.Create;
  AListHasil.Clear;
  APath := ExtractFilePath(Application.ExeName);
  mmPathFlashdisk.Lines.Clear;
  mmNamaDB.Lines.Clear;
  if FileExists(APath+'Profile.dll') then
    AListHasil.LoadFromFile(APath+'Profile.dll');
  isPath := False;
  if AListHasil.Count > 0 then begin
    for i := 0 to AListHasil.Count-1 do begin
    	if (i = 0) then begin
        AHasil := decriptNew(AListHasil.Strings[i]);
      end else AHasil := AListHasil.Strings[i];
      case i of
//  AListHasil.Add(AHasil);
//  AListHasil.Add(txtNamaProject.Text);
//  AListHasil.Add(inttostr(cmbTipeDB.itemindex));
        0 : begin
          txtHostCloud.Text  := AHasil;
          btnRefresh.Click;
        end;
        1 : cmbNamaProject.Itemindex := cmbNamaProject.Items.IndexOf(AHasil);
        2 : cmbTipeDB.ItemIndex := StrToIntDef(AHasil, 0);
        3 : txtHari.IntValue := StrToIntDef(AHasil, 0);
        4 : txtHariCloud.IntValue := StrToIntDef(AHasil, 0);
        5 : continue;
        else begin
          if isPath = False then begin
            if AHasil = '-' then
              isPath := True
            else mmNamaDB.Lines.Add(AHasil);
          end else begin
            mmPathFlashdisk.Lines.Add(AHasil);
          end;
        end;
      end;
    end;
  end;
  AListHasil.Destroy;
end;

procedure TfrmMenu.btnEncodeClick(Sender: TObject);
var tempint : integer;
    AHasil, tempstr, tempstr2 : string;
    AListHasil: TStringList;
    i : Integer;
begin
  AListHasil:= TStringList.Create;
  AListHasil.Clear;

  tempstr := TrimAll(txtHostCloud.Text);
  AHasil := encriptNew(tempstr);
  AListHasil.Add(AHasil);
  AListHasil.Add(cmbNamaProject.Items.Strings[cmbNamaProject.itemindex]);
  AListHasil.Add(inttostr(cmbTipeDB.itemindex));
  AListHasil.Add(inttostr(txtHari.IntValue));
  AListHasil.Add(inttostr(txtHariCloud.IntValue));
  AListHasil.Add('-');
  mmNamaDB.Lines.text := trimall(mmNamaDB.Lines.text);
  mmPathFlashdisk.Lines.text := trimall(mmPathFlashdisk.Lines.text);
  for i := 0 to mmNamaDB.Lines.Count-1 do begin
    if (mmNamaDB.Lines[i] <> '') then
      AListHasil.Add(mmNamaDB.Lines[i]);
  end;
  AListHasil.Add('-');
  for i := 0 to mmPathFlashdisk.Lines.Count-1 do begin
    if (mmPathFlashdisk.Lines[i] <> '') then
      AListHasil.Add(mmPathFlashdisk.Lines[i]);
  end;
  AListHasil.SaveToFile('Profile.dll');
  AListHasil.Destroy;               
end;

procedure TfrmMenu.btnHelpClick(Sender: TObject);
begin
  Inform('Keterangan Format Nama DB : '+#13+
         'Oracle         : username/password@host'+#13+
         'Oracle 11G : username/password@host'+#13+
         'Postgre       : IpAdresserver/nama_db/port'+#13+
         'Sql Server   : nama_db/pass@host'
         );
end;

procedure TfrmMenu.btnRefreshClick(Sender: TObject);
var i : integer; filter : string;
    sql : string;
    listData : TStringList;
begin
  listData := TStringList.Create;
  sql := 'SELECT Seq, nama ' +
         'FROM master_project order by nama';
  sqltonamevaluelistOnline(listData, sql);
  cmbNamaProject.items.Clear;
  for i  := 0 to listData.Count-1 do begin
    cmbNamaProject.Items.Add(listData.ValueFromIndex[i]);
  end;
  listData.Destroy;
end;

procedure TfrmMenu.cmbTipeDBKeyPress(Sender: TObject; var Key: Char);
begin
  if Key = #13 then
    txtHari.setfocus;
end;

procedure TfrmMenu.FormCreate(Sender: TObject);
begin
  txtHostCloud.Clear;
  cmbTipeDB.ItemIndex := -1;
  cmbNamaProject.items.clear;
  
  mmNamaDB.Lines.Clear;
  mmPathFlashdisk.Lines.Clear;
  txtHari.IntValue := 0;
  txtHariCloud.IntValue := 0;
end;

procedure TfrmMenu.get_json_list_from_url_by_post(var AList: tstringlist;
  url: string; Data: TIdMultiPartFormDataStream);
var jsonIsi, jSonPerBaris, objJson : string;
    http : TIdHttp;
    i : Integer;
    seq : string;
    sql : string;
begin
  http := TIdHttp.Create(nil);
  http.HandleRedirects := true;
  http.ReadTimeout := 60000;
  http.Request.ContentType := 'application/json';
  http.Request.ContentEncoding := 'utf-8';
  jsonIsi := http.Post(url, data);
  jsonIsi := trimall(jsonIsi);
  if (jsonIsi <> '[]') and not(containstext(jsonisi, 'Tidak ada data')) then begin
    jsonToStringlist(AList, jsonIsi);
  end else AList.Clear;

  http.free;
end;

function TfrmMenu.JSONtoString(json: string; field: integer): string;
var objJson : string;
begin
  objJson := EkstrakString(jSon, '^',field);
  result := EkstrakString(objJson, '~',2);
end;

procedure TfrmMenu.jsonToStringlist(var Alist: TStringList; json: string);
var i : Integer;
    pjg : Integer;
    temp : string;
    tempname : string;
    huruf : string;
begin
  pjg := Length(json);
  Alist.Clear;
  temp := '';
  tempname := '';
  for i := 1 to pjg do begin
    huruf := json[i];
    if tempname <> '' then begin
      if tempname = '},{' then begin
        Alist.Add(temp);
        temp := huruf;
        tempname := '';
      end else begin
        tempname := tempname + huruf;
      end;
    end else if huruf = '}' then begin
      tempname := tempname + huruf;
    end else begin
      temp := temp + huruf;
    end;
  end;
  if temp <> '' then
    Alist.Add(temp);

end;

function TfrmMenu.NumberToString(Number, Panjang: integer): string;
var temp : string;
begin
  temp := IntToStr(Number);
  while length(temp) < Panjang do temp := '0' + temp;
  Result := temp;
end;

function TfrmMenu.replaceHasilJSon(input: string; isAdaNull: Boolean): string;
begin
  Result := input;
  Result := ReplaceText(Result, ':",', ':"KOMAORION');//hendry 280120, kalo value depan nya ", nanti nya error
  Result := ReplaceStr(Result, '","', '^');
  Result := ReplaceStr(Result, ',"', '^');
  Result := ReplaceStr(Result, '",', '^');
  if isAdaNull then begin
    Result := ReplaceStr(Result, ',null', '^null');
    Result := ReplaceStr(Result, 'null,', 'null^');
  end;
  Result := ReplaceStr(Result, '[{"', '');
  Result := ReplaceStr(Result, '{"', '');
  Result := ReplaceStr(Result, '"', '"');
  Result := ReplaceStr(Result, '"}]', '');
  Result := ReplaceStr(Result, '"}', '');
  Result := ReplaceStr(Result, '":"', '~');
  Result := ReplaceStr(Result, '"', '');
  Result := ReplaceStr(Result, '\/', '/');
  Result := ReplaceText(Result, 'KOMAORION', ',');//hendry 280120, kalo value depan nya ", nanti nya error

end;

procedure TfrmMenu.sqltonamevaluelistOnline(var AStringList: TStringList;
  ASQL: string);
var i: integer;
    url : string;
    jSonPerBaris, parameter : string;
    tempList : TStringList;
    objJson : string;
    data : TIdMultiPartFormDataStream;
begin
  data := TIdMultiPartFormDataStream.Create;
  tempList := TStringList.Create;
  data.AddFormField('sql', asql);
  url := txtHostCloud.text+'/dml/open_sql?'+parameter;
  get_json_list_from_url_by_post(tempList, url, data);
  AStringList.Clear;
  for i := 0 to tempList.Count-1 do begin
    jSonPerBaris := tempList.Strings[i];
    jSonPerBaris := replaceHasilJSon(jSonPerBaris);
    objJson := jSonPerBaris + '~';
    AStringList.Add(JSONtoString(objJson, 1)+'='+JSONtoString(objJson, 2));
  end;
  tempList.Destroy;
  data.Destroy;
end;

end.
