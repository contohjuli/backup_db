object frmMenu: TfrmMenu
  Left = 376
  Top = 181
  BorderStyle = bsDialog
  Caption = 'Menu'
  ClientHeight = 353
  ClientWidth = 469
  Color = clWindow
  TransparentColor = True
  TransparentColorValue = clMaroon
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poDesktopCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 12
    Top = 12
    Width = 52
    Height = 13
    Caption = 'Host Cloud'
  end
  object Label3: TLabel
    Left = 12
    Top = 235
    Width = 62
    Height = 13
    Caption = 'Path Flasdisk'
  end
  object Label1: TLabel
    Left = 12
    Top = 39
    Width = 64
    Height = 13
    Caption = 'Nama Project'
  end
  object Label4: TLabel
    Left = 12
    Top = 66
    Width = 36
    Height = 13
    Caption = 'Tipe DB'
  end
  object Label5: TLabel
    Left = 12
    Top = 140
    Width = 43
    Height = 13
    Caption = 'Nama DB'
  end
  object Label6: TLabel
    Left = 12
    Top = 93
    Width = 125
    Height = 13
    Caption = 'Jumlah Hari Hapus Backup'
  end
  object Label7: TLabel
    Left = 12
    Top = 119
    Width = 144
    Height = 13
    Caption = 'Jumlah Hari Hapus Data Cloud'
  end
  object btnEncode: TButton
    Left = 349
    Top = 320
    Width = 113
    Height = 25
    Caption = '&Encode'
    TabOrder = 7
    OnClick = btnEncodeClick
  end
  object btnDecode: TButton
    Left = 233
    Top = 320
    Width = 110
    Height = 25
    Caption = '&Decode'
    TabOrder = 6
    OnClick = btnDecodeClick
  end
  object txtHostCloud: TAdvEdit
    Left = 162
    Top = 8
    Width = 300
    Height = 21
    LabelAlwaysEnabled = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'Tahoma'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    TabOrder = 0
    Text = 'txtKode'
    Visible = True
    Version = '2.9.2.1'
  end
  object mmPathFlashdisk: TMemo
    Left = 162
    Top = 230
    Width = 300
    Height = 84
    Lines.Strings = (
      'mmCabang')
    ScrollBars = ssBoth
    TabOrder = 5
    WordWrap = False
  end
  object cmbTipeDB: TAdvComboBox
    Left = 162
    Top = 62
    Width = 145
    Height = 21
    Color = clWindow
    Version = '1.4.0.0'
    Visible = True
    Style = csDropDownList
    DropWidth = 0
    Enabled = True
    ItemIndex = -1
    ItemHeight = 13
    Items.Strings = (
      'Oracle 10g'
      'Oracle 11g'
      'Postgre'
      'Sql Server')
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'Tahoma'
    LabelFont.Style = []
    TabOrder = 1
    OnKeyPress = cmbTipeDBKeyPress
  end
  object mmNamaDB: TMemo
    Left = 162
    Top = 143
    Width = 300
    Height = 81
    Lines.Strings = (
      'mmCabang')
    ScrollBars = ssBoth
    TabOrder = 4
    WordWrap = False
  end
  object btnHelp: TButton
    Left = 17
    Top = 320
    Width = 106
    Height = 25
    Caption = 'Help'
    TabOrder = 8
    OnClick = btnHelpClick
  end
  object txtHari: TAdvEdit
    Left = 162
    Top = 89
    Width = 47
    Height = 21
    EditAlign = eaRight
    EditType = etNumeric
    LabelAlwaysEnabled = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'Tahoma'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    TabOrder = 2
    Text = '0'
    Visible = True
    Version = '2.9.2.1'
  end
  object txtHariCloud: TAdvEdit
    Left = 162
    Top = 116
    Width = 47
    Height = 21
    EditAlign = eaRight
    EditType = etNumeric
    LabelAlwaysEnabled = True
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'Tahoma'
    LabelFont.Style = []
    Lookup.Separator = ';'
    Color = clWindow
    TabOrder = 3
    Text = '0'
    Visible = True
    Version = '2.9.2.1'
  end
  object cmbNamaProject: TAdvComboBox
    Left = 162
    Top = 35
    Width = 296
    Height = 21
    Color = clWindow
    Version = '1.4.0.0'
    Visible = True
    Style = csDropDownList
    DropWidth = 0
    Enabled = True
    ItemIndex = -1
    ItemHeight = 13
    Items.Strings = (
      'PELABUHAN'
      'KANDANG'
      'RPH'
      'CIJAPATI'
      'BANDUNG'
      'AMPEL')
    LabelFont.Charset = DEFAULT_CHARSET
    LabelFont.Color = clWindowText
    LabelFont.Height = -11
    LabelFont.Name = 'Tahoma'
    LabelFont.Style = []
    TabOrder = 9
  end
  object btnRefresh: TButton
    Left = 129
    Top = 320
    Width = 98
    Height = 25
    Caption = '&Refresh Project'
    TabOrder = 10
    OnClick = btnRefreshClick
  end
end
