unit UGeneral;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms, Math,
  StrUtils, Dialogs, StdCtrls, ExtCtrls, Gauges, AdvEdit, Grids, BaseGrid, AdvGrid,
  DateUtils, ComCtrls, QuickRpt, QRCtrls, ADODB,
  AdvGlowButton, AdvGroupBox, ColCombo;

  {BERHUBUNGAN DENGAN ANGKA ATAU HURUF}
  function IsAlphaExist(AString: string): boolean;
  function IsNumericExist(AString: string): boolean;
  function IsNonAlphaNumericExist(AString: string): boolean;
  function IntToStrFixed(MinLen: integer; Input: integer): string;
  function CekInteger(AString: string): boolean;
  function CekFloat(AString: string): boolean;
  function ReplaceSubStr(Input, Find, ReplaceWith: string): string;
  function TrimAll(Input: string): string;
  procedure FillString(var Input: string; Count: integer; Value: char);
  function CekIntegerFmt(AString: string): boolean;
  function StrFmtToIntDef(AString: string; ADefault: integer): integer;
  function StrFmtToInt(AString: string): integer;
  function StrFmtToFloatDef(AString: string; ADefault: real): real;
  function StrFmtToFloat(AString: string): real;
  function CekFloatFmt(AString: string): boolean;
  function IntToStrFmt(AInteger: integer): string;
  function FloatToStrFmt(AFloat: real): string;
  function FloatToStrFmt1(AFloat: real): string;
  function FloatToStrFmt2(AFloat: real): string;
  function FloatToStrFmt3(AFloat: real): string;
  function IntToStrCurrency(AInteger: integer): string;
  function MoneyToIndonesianText(input: String): String;
  function EkstrakString(Input: string; pemisah: char; partisi: integer): string;
  function SetStrLen(Input: string; Len: integer; FillChar: char; FillPos: TAlignment): string;
  function HitungChar(Input: string; Karakter: Char): integer;
  function NumberToString(Number,Panjang : integer) : string;

  {TENTANG KONEKSI DENGAN USER --> PER PESANAN}
  function Confirmed(prompt: string): Boolean;
  function SaveConfirmed: Boolean;
  procedure Alert(AlertMsg: string);
  procedure Inform(Information: string);
  procedure FailSave;
  procedure SuccessSave;
  procedure FailUpdate;
  procedure SuccessUpdate;
  procedure AccessDenied;

  {PER TANGGALAN}
  function getNamaBulan(aBulan:integer):string;
  function LastMonth(adate: TDate): TDate;
  function GetMonthYear(aMonth: Integer; aYear: TDate): string;
  procedure getMonths(var target:TStringList);
  procedure getHari(var target:TStringList; ABulanThn: TDate);
  procedure getDayOfWeekList(var target:TStringList);
  function CekTanggal(Input: string): boolean;
  function ReverseFormatDate(Tanggal:string): TDate;
  function getPeriode(aDate:TDate):string;
  function getPeriode2(aDate:TDate; YearType: integer=1):string;
  function getPeriodeBaru(aDate:TDate):string;
  function TambahTanggal(Tgl : TDate; Tambahan : Integer) : TDate;
  function RecodeTheMonth(AValue: TDate; AMonth: integer): TDate;



  {BERHUBUNGAN DENGAN TStringList}
  procedure tslRemoveName(var Target:TStringList;NameToRemove:string);
  procedure tslRemoveValue(var Target:TStringList;ValueToRemove:string);
  function tslIsExist(T: Tstringlist; search: string): boolean;
  function tslMaxRealValue(b: tstringlist): real;
  function tslSUMRealValue(b: tstringlist): real;
  procedure StringListToStrings(AValueList: TStringList; AList: TStrings);
  procedure tslNameValueListToNameList(AValueList: TStringList; AList: TStrings);
  procedure NameValueListToValueList(AValueList: TStringList; AList: TStrings);
//  procedure NameToValueListComboboxStdJurnal(AValueList: TStringList; AList: TColumnComboBox);

  function tslIndexofValue(aList:TStringList;aValue:string):integer;

//  procedure SQLToStringList(var AStringList: TStringList; ASQL: string);
//  procedure SQLToNameValueList(var AStringList: TStringList; ASQL: string);
//  procedure SQLToNameValueListAll(var AStringList: TStringList; ASQL: string);
//  procedure SQLToNameValueList2(var AStringList: TStringList; ASQL: string;
//    ANamesIndex, AValueIndex1, AValueIndex2: integer);

  {TENTANG AdvStringGrid}
   procedure asgDeleteLastRows(aGrid : TAdvStringGrid; LimitRow: Integer = 2; DeleteRow : Integer = 1);
   procedure asgSetTitleText(aGrid : TAdvStringGrid; aText: TStringList = nil; aColCount: Integer = 1; aRow: Integer = 0; aFont: TFont = nil; anAlignment : TAlignMent = taCenter);
   procedure asgSetAlignmentTitle(aGrid : TAdvStringGrid; aRow: Integer = 0; anAlignment : TAlignMent = taCenter);
   procedure asgSetAlignmentRow(aGrid : TAdvStringGrid; aRow: Integer = 0; anAlignment : TAlignMent = taCenter);
   procedure asgSetAlignmentNumber(aGrid : TAdvStringGrid; aCol: Integer = 0; anAlignment : TAlignMent = taCenter);
   procedure asgSetAlignmentCol(aGrid : TAdvStringGrid; aCol: Integer = 0; anAlignment : TAlignMent = taCenter);
   procedure asgSetGridColor(aGrid : TAdvStringGrid; aPrimaryColor : TColor = $00E4E4E4; aSecondaryColor : TColor = $00EFEFEF; aFixedColor : TColor = clMenu; aFixedText: TColor = clWhite; aGridColor : TColor = clWhite; aSelectionColor : TColor = clNavy; aSelectionText: TColor = clBlack);
   procedure asgSetAllGridColor(aForm : TForm; aPrimaryColor : TColor = $00E4E4E4; aSecondaryColor : TColor = $00EFEFEF);
   procedure asgSetTextHeaderBold(aParent : TForm; aRow : Integer = 0);
   procedure asgSetTextNumberBold(aParent : TForm; aCol : Integer = 0);
   procedure SetCellFontColor(aGrid : TAdvStringGrid;FontColor:TColor;InitCol,Row:integer);
   procedure asgSetColumnSize(aGrid : TAdvStringGrid; aColCount: Integer; aText: TStringList = nil);
   procedure asgSetColumnFitToWidth(aGrid : TADVStringGrid; aBeginCol : Integer = 0);
   procedure asgSetColumnAutoSize(aGrid : TAdvStringGrid; aPadding : Integer = 2; aColNoAffected : Boolean = FALSE);
   procedure asgSetRowColor(aMasterGrid: TAdvStringGrid; aRow: Integer; aColor: TColor);
   procedure asgSetSortLast(aGrid : TAdvStringGrid);
   procedure asgClearFooter(aGrid : TAdvStringGrid);
   procedure asgGetGridColor(var MasterColor: TColor; var SecondaryColor : TColor; aThemes : TColor);
   function asgGetSummary(aGrid: TAdvStringGrid; aCol: Integer):Real;
   function asgGetSummaryMultiple(aGrid: TAdvStringGrid; aCol1, aCol2: Integer):Real;
   function asgFindTextOnGrid(var aRow: Integer; aBeginRow: Integer; aSearchCol: Integer; aGrid: TAdvStringGrid; aFindText: String):Boolean;
   procedure asgSetInputCellColor(aForm: TForm; aGrid: TAdvStringGrid; aCols: array of integer; aColsClick: array of integer;
             aRowBegin: Integer = -1; aRowEnd: Integer = -1; aColorClick: TColor = clWhite; aColorInput: TColor = clWhite); overload;
   function asgGetSummaryCols(aGrid: TAdvStringGrid; aCol: Integer; aRowFrom, aRowTo: Integer): Real;
   function asgGetSummaryRows(aGrid: TAdvStringGrid; aRow: Integer; aColFrom, aColTo: Integer): Real;
   function asgGetSummaryRowsEx(aGrid: TAdvStringGrid; aRow: Integer; aColFrom, aColTo: Integer; FirstOpr: String; ReverseOprIn: Integer): Real;
   procedure asgFillGridBuffer(buffer: _RecordSet; var Grid: TAdvStringGrid; StartCol,StartRow: integer; isFLoatFoot : Boolean = False);
   procedure asgEditToGrid(adv: TAdvStringGrid; kolom: integer; baris: integer; editbox: tadvedit);
   procedure asgTextToGrid(adv: TAdvStringGrid; kolom: integer; baris: integer; Text: string);
   procedure asgDateToGrid(adv: TAdvStringGrid; kolom: integer; baris: integer; InputDate: TDateTimePicker);
   procedure asgDeleteRow(grid: TAdvStringGrid; row: integer; idcol: integer);
//   procedure ExportGridToExcell(var aGrid : TAdvStringGrid; aDialogs : TSaveDialog; ACellFormat: Boolean = False);
   procedure PreparePrintDataGrid(aGrid : TAdvStringGrid; AIsHide: boolean);

   procedure ResetGrid(Sender: TAdvStringGrid; DefaultRowCount,DefaultColCount,
                      DefaultFixedRows, DefaultFixedCols: integer);
   //handy tuk ekport grid
   procedure asgSetCellColor(aMasterGrid: TAdvStringGrid; aRow: Integer; aColor: TColor);
   //procedure SetRowFontStyle(aMasterGrid: TAdvStringGrid; aRow: Integer; aStyle: TFontStyle);
   procedure SetRowFontStyle(aMasterGrid: TAdvStringGrid; aFromCol, aRow: Integer; aStyle: TFontStyle);

   procedure asgSetRowFontStyleNormal(aMasterGrid: TAdvStringGrid; aRow: Integer; AColor: TColor = 0);
   procedure asgSetFixedRowFontStyleNormal(aMasterGrid: TAdvStringGrid; AFixRow: Integer);
   procedure SetRowColor(aMasterGrid: TAdvStringGrid; aFromCol, aRow: Integer; aColor: TColor);
   procedure HapusBaris(grid: TAdvStringGrid; DefoultRowCount, row: integer);

   {KHUSUS RADIO BUTTON}
   procedure rbtSetAllCheckedFalse(aForm : TForm; aChecked : Boolean = FALSE); {-- membuat semua radio checkednya jadi false}
   procedure rbtSetAllEnabledFalse(aForm : TForm; aEnabled : Boolean = FALSE); {-- membuat semua radio enablednya jadi false}

   {KHUSUS CHECK BOX}
   procedure cbxSetAllCheckedFalse(aForm : TForm; aChecked : Boolean = FALSE); {-- membuat semua checkbox checkednya jadi false}
   procedure cbxSetAllEnabledFalse(aForm : TForm; aEnabled : Boolean = FALSE); {-- membuat semua checkbox enablednya jadi false}
   procedure SetFilterSize(Sender: TPanel; AButton: TButton; AShowSize:integer);
   procedure SetFilterSizeAdv(Sender: TPanel; AButton: TAdvGlowButton; AShowSize:integer);
   procedure SetFilterGroupSize(Sender: TAdvGroupBox; AButton: TAdvGlowButton; AShowSize:integer);

   {KHUSUS EDIT}
   procedure txtSetAllEmpty(aForm : TForm); {-- membuat semua edit jadi kosong}

   {KHUSUS COMBO BOX}
   function cmbSetListValue(Combo: TComboBox; NewValue: string) : Integer;
   procedure cmbSetAllItemsClear(aForm : TForm); {-- membuat semua items combo jd clear}
   procedure cmbSetAllItemIndex(aForm : TForm; Idx : integer = -1);
   procedure SetComboboxSize(Sender: TComboBox; NewWidth:integer);

   {KHUSUS GROUP BOX}
   procedure grbSetAllEnabledFalse(aForm : TForm; aEnabled : Boolean = FALSE); {-- membuat semua groupbox enablednya jadi false}

//   function getStringFromSQL(aSql:string):string;
//   function getFloatFromSQL(aSql:string):double;
//   function getIntegerFromSQL(aSql:string):integer;
//   function getBooleanFromSQL(aSql:string):boolean;
//   function getTDateFromSQL(aSql:string):TDate;
//   function ExecuteSQL(aSql:string):boolean;
//
//   function ExecTransaction(SQL: string; InsertMode: boolean=true): boolean;
//   function ExecTransaction2(SQL: string; InsertMode: boolean=true): boolean;
//   function ExecTransaction3(SQL: string): boolean;
//   function ExecTransaction4(SQL: string): boolean;
//   //function ExecAktivasi(SQL: string; InsertMode: boolean=true): boolean;
//   function ExecDelete(SQL: string): boolean;
//   function ExecAktivasi(SQL: string; isAktif: boolean=true): boolean;
//
//   function getNextIDNumPeriode(fieldName:string;tableName:string;condition:String;prefix,suffix:string;reversedOrderNum:string='';minlength:integer=5):string;
   procedure LimitDecimal(Sender: TObject; aCol: Integer; var aKey: Char); overload;
//   function CaptionPeriode(op1:TSQLOperator ;Date1:TDate;Date2:TDate):string;
   function InputNote: string;
   procedure SetAutoMoney(Sender: TObject; var aKey: Char);

//  function getNextIDNumPeriodeNonPrefix(fieldName:string;tableName:string;condition:String;prefix,suffix:string;reversedOrderNum:string='';minlength:integer=5):string;

  function AutoThousandSep(s: string): string;
  function SetText(value: string): String;

  function FindTextOnGrid(var aRow: Integer; aBeginRow: Integer; aSearchCol: Integer; aGrid: TAdvStringGrid; aFindText: String):Boolean;
//  function getNextIDNum(fieldName:string;tableName:string;condition:String;prefix:string;reversedOrderNum:string='';minlength:integer=5):string;
//  function getNextNumPajakBru(ADate: TDate): string;

//  function ExecTransactionKhusus(SQL: string): boolean;
  procedure SetColumnComboboxSize(Sender: TObject; NewWidth:integer); overload;

  function setAccountView(accNum :integer):string;
  function getAccountView(accNum : string):integer;

  procedure DeleteRowTerakhir(sender : TAdvStringGrid;LimitRow,RowDelete:integer);
  procedure DeleteRowKosong(sender : TAdvStringGrid;FixedRow,KolKriteria:integer;IsNumber,isFloatingFooter:boolean);
  //procedure SetCellFontColor(aGrid : TAdvStringGrid;FontColor:TColor;InitCol,Row:integer);
  function IsClear(AGrid: TAdvStringGrid; ARow, ACol: integer): boolean;
  function GetMonthYearOnDate(aMonth: Integer; aYear: TDate): String;
  procedure txtSetAllReadOnly(aForm : TForm; AReadOnly: Boolean); {-- membuat semua edit jadi disable}
  function IsiNotes(ACaption: String): String;
  function CurrToIndonesianSpeech(nl : string):string;
  function ConvertNumberTerbilang(nl : string):string;
  function getListNameCaseSensitive(list : TStringList; cari : string) : string;
  function encriptNew(input : string) : string;
  function decriptNew(input : string) : string;

//  function CreateNewSeq(ASeqName, ATableName: String): Integer;
//  function GetCurrentSeq(ASeqName, ATableName: String): Integer;

//  function getNextNIP(fieldName:string;tableName:string; condition:String;
//        prefix,suffix:string;minlength:integer=5):string;

implementation

uses Uconst;


//uses UMaster;

//uses OracleConnection;
  {BERHUBUNGAN DENGAN ANGKA ATAU HURUF}
  function IsAlphaExist(AString: string): boolean;
  var i: integer;
  begin
    Result := false;
    for i:=1 to length(AString) do
      if (AString[i] in ['a'..'z','A'..'Z']) then Result := true;
  end;

  function IsNumericExist(AString: string): boolean;
  var i: integer;
  begin
    Result := false;
    for i:=1 to length(AString) do
      if (AString[i] in ['0'..'9']) then Result := true;
  end;

  function IsNonAlphaNumericExist(AString: string): boolean;
  var i: integer;
  begin
    Result := false;
    for i:=1 to length(AString) do
      if not (AString[i] in ['0'..'9','a'..'z','A'..'Z']) then Result := true;
  end;

  function IntToStrFixed(MinLen: integer; Input: integer): string;
  var TmpStr: string;
    i: integer;
  begin
    TmpStr:=IntToStr(Input);
    for i:=1 to MinLen-length(TmpStr) do
      TmpStr:='0'+TmpStr;
    result:=TmpStr;
  end;

  function CekInteger(AString: string): boolean;
  begin
    try
      StrToInt(AString);
      Result:=true
    except
      on EConvertError do
        Result:=false;
    end;
  end;

  function CekFloat(AString: string): boolean;
  begin
    try
      StrToFloat(AString);
      Result:=true
    except
      on EConvertError do
        Result:=false;
    end;
  end;

  function ReplaceSubStr(Input, Find, ReplaceWith: string): string;
  { I.S. : Input, Find, dan ReplaceWith terdefinisi
    F.S. : menghasilkan string Input di mana substring Find diganti dengan
           substring ReplaceWith }
  var i: integer;
      Tmp: string;
  begin
    while (Pos(Find,Input)>0) do begin
      i:=Pos(Find,Input);
      Delete(Input,i,Length(Find));
      Tmp:=Copy(Input,i,Length(Input)-i+2);
      Delete(Input,i,Length(Input)-i+2);
      Input:=Input+ReplaceWith+Tmp;
    end;

    Result:=Input;
  end;

  function TrimAll(Input: string): string;
  { I.S. : Input terdefinisi
    F.S. : menghasilkan string dengan spasi awal dan akhir dihilangkan
           dan spasi antar kata sebanyak satu spasi }
  var i: integer;
    TmpStr: string;
  begin
    Input:=Trim(Input);
    TmpStr:='';
    for i:=1 to Length(Input) do
      if (Input[i]<>' ') or ((Input[i]=' ') and (TmpStr[Length(TmpStr)]<>' ')) then
        TmpStr:=TmpStr+Input[i];

    Result:=TmpStr;
  end;

  procedure FillString(var Input: string; Count: integer; Value: char);
  var i: integer;
  begin
    Input := '';
    for i:=1 to Count do
      Input := Input + Value;
  end;

  function CekIntegerFmt(AString: string): boolean;
  begin
    Result := CekInteger(ReplaceSubStr(AString,ThousandSeparator,''));
  end;

  function CekFloatFmt(AString: string): boolean;
  begin
    Result := CekFloat(ReplaceSubStr(AString,ThousandSeparator,''));
  end;

  function StrFmtToIntDef(AString: string; ADefault: integer): integer;
  begin
    if (CekIntegerFmt(AString)) then
      Result := StrToInt(ReplaceSubStr(AString,ThousandSeparator,''))
    else
      Result := ADefault;
  end;

  function StrFmtToInt(AString: string): integer;
  begin
    Result := StrFmtToIntDef(AString, 0);
  end;

  function StrFmtToFloatDef(AString: string; ADefault: real): real;
  begin
    if (CekFloatFmt(AString)) then
      Result := StrToFloat(ReplaceSubStr(AString,ThousandSeparator,''))
    else
      Result := ADefault;
  end;

  function StrFmtToFloat(AString: string): real;
  begin
    Result := StrFmtToFloatDef(AString,0.00);
  end;

  function IntToStrFmt(AInteger: integer): string;
  begin
    Result := FormatFloat('#,##0',AInteger);
  end;

  function FloatToStrFmt(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0',AFloat);
  end;

  function FloatToStrFmt1(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0.0',AFloat);
  end;

  function FloatToStrFmt2(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0.00',AFloat);
  end;

  function FloatToStrFmt3(AFloat: real): string;
  begin
    Result := FormatFloat('#,##0.000',AFloat);
  end;

  function IntToStrCurrency(AInteger: integer): string;
  begin
    Result := 'Rp '+FormatFloat('#,##0.00',AInteger);
  end;

  function MoneyToIndonesianText(input: String): String;
  const vlstep : array [0..4] of string = (' ','ribu ','juta ','milyar ','trilyun ');
  var i,j,k,panjang,m : integer;
      stemp,stemp2,addstr : string;
      nl2,nltemp,qtemp : string;
      good,belas : boolean;
  begin
    good := false;
    for i:=1 to Length(input) do
    begin
      if (input[i] <> '0') then
         good := true;
      if (good) then
         nl2 := nl2 + input[i];
    end;
    if (length(nl2) > 15) then
       nltemp:=Copy(nl2,Length(nl2)-15+1,15)
    else nltemp := nl2;
    stemp2 := '';
    for i:=0 to 4 do
    begin
      k := Length(nltemp);
      if (k = 0) then
         break;
      if (k >= 3) then
         qtemp := Copy(nltemp,Length(nltemp)-2,3)
      else qtemp := nltemp;
      nltemp := Copy(nltemp,1,Length(nltemp)-3);
      stemp := '';
      belas := false;
      if (k >= 3) then
         panjang := 3
      else panjang:=k;
      m := 4-panjang;
      for j:=1 to panjang do
      begin
        addstr := '';
        if (qtemp[j] = '9') then
           addstr := 'sembilan ';
        if (qtemp[j] = '8') then
           addstr := 'delapan ';
        if (qtemp[j] = '7') then
           addstr := 'tujuh ';
        if (qtemp[j] = '6') then
           addstr := 'enam ';
        if (qtemp[j] = '5') then
           addstr := 'lima ';
        if (qtemp[j] = '4') then
           addstr := 'empat ';
        if (qtemp[j] = '3') then
           addstr := 'tiga ';
        if (qtemp[j] = '2') then
           addstr := 'dua ';
        if (qtemp[j] = '1') then
        begin
          case m of
          1,2: case m of
                1:addstr := 'se';
                2:belas := true;
               end;
          3: if (not belas) then
             begin
                if (i = 1) then
                begin
                  if (stemp = '') then
                     addstr := 'se'
                  else addstr := 'satu ' ;
                end else addstr := 'satu ';
             end else addstr := 'se';
          end;
        end;
        if (qtemp[j] = '0') then
           if (belas) then
           begin
             addstr := 'sepuluh ';
             belas := false;
           end;
        if ((addstr <> '') or (belas)) then
        begin
          case m of
          1: addstr := addstr + 'ratus ';
          2: if (not belas) then
                addstr := addstr+ 'puluh ';
          3: if (belas) then
                addstr := addstr+ 'belas ';
          end;
        end;
        stemp := stemp + addstr;
        inc(m);
      end;
      if (stemp <> '') then
         stemp2 := stemp + vlstep[i] + stemp2;
    end;
    result := stemp2+'rupiah';
    if (result <> '') then
       result[1] := upcase(result[1]);
  end;

  function EkstrakString(Input: string; pemisah: char; partisi: integer): string;
  { I.S. : Input = string yang akan di-extract, pemisah = karakter yang
           dipakai sebagai tanda pemisah, partisi = bagian ke berapa yang
           akan diambil, paling kiri adalah bagian ke-1
    F.S. : menghasilkan substring ke-partisi dari Input dengan batas pemisah }
  var Ctr, Posisi: integer;
      TmpString: string;
  begin
    TmpString:='';
    Ctr:=0;
    while (Ctr<partisi) do
    begin
      Posisi:=Pos(pemisah,Input);
      if (Posisi=0) then
        Posisi:=Length(Input)+1;
      TmpString:=Copy(Input,1,Posisi-1);
      Delete(Input,1,Posisi);
      Ctr:=Ctr+1;
    end;

    Result:=TmpString;
  end;

  function SetStrLen(Input: string; Len: integer; FillChar: char; FillPos: TAlignment): string;
  { I.S. : Input, Len, FillChar, dan FillPos terdefinisi.
           FillPos diisi dengan tpKiri atau tpKanan
    F.S. : jika panjang Input lebih kecil dari Len maka akan menghasilkan
           string Input yang ditambahkan dengan karakter FillChar pada posisi
           kiri atau kanan string (sesuai FillPos) hingga Input memiliki
           panjang=Len, jika panjang Input>=Len maka akan menghasilkan string
           Input tanpa perubahan }
  begin
    if (FillPos=taRightJustify) or (FillPos=taLeftJustify) then
      while (Length(Input)<Len) do begin
        if (FillPos=taRightJustify) then
          Input:=FillChar+Input
        else if (FillPos=taLeftJustify) then
          Input:=Input+FillChar;
      end;

    Result:=Input;
  end;

  function HitungChar(Input: string; Karakter: Char): integer;
  { I.S. : Input dan Karakter terdefinisi
    F.S. : menghasilkan jumlah Karakter yang terdapat pada input }
  var i, Count: integer;
  begin
    Count:=0;
    for i:=1 to Length(Input) do begin
      if (Input[i] = Karakter) then
        Inc(Count);
    end;

    Result := Count;
  end;

  function NumberToString(Number,Panjang : integer) : string;
  var temp : string;
  begin
    temp := IntToStr(Number);
    while length(temp) < Panjang do temp := '0' + temp;
    Result := temp;
  end;
{ ---------------------------------------------------------------------------- }
  {TENTANG KONEKSI DENGAN USER --> PER PESANAN}
  function Confirmed(prompt: string): Boolean;
  begin
    Result := (MessageDlg(prompt, mtConfirmation, [mbYes, mbNo], 0) = mrYes );
  end;

  function SaveConfirmed: Boolean;
  begin
    Result := Confirmed(MSG_SAVE_CONFIRMATION);
  end;

  procedure Alert(AlertMsg: string);
  begin
    MessageDlg(AlertMsg, mtWarning, [mbOK], 0);
  end;

  procedure Inform(Information: string);
  begin
    MessageDlg(Information, mtInformation, [mbOK], 0);
  end;

  procedure FailSave;
  begin
    ShowMessage(MSG_UNSUCCESS_SAVING);
  end;

  procedure SuccessSave;
  begin
    ShowMessage(MSG_SUCCESS_SAVING);
  end;

  procedure FailUpdate;
  begin
    ShowMessage(MSG_UNSUCCESS_UPDATE);
  end;

  procedure SuccessUpdate;
  begin
    ShowMessage(MSG_SUCCESS_UPDATE);
  end;

  procedure AccessDenied;
  begin
    ShowMessage(MSG_UNAUTHORISED_ACCESS);
  end;
{ ---------------------------------------------------------------------------- }
  {PER TANGGALAN}
  function getNamaBulan(aBulan:integer):string;
    var listBulan : TStringList;
  begin
    listBulan := TStringList.Create;
    getMonths(listBulan);
    Result := listBulan.Values[IntToStr(aBulan)];
    listBulan.Free;
  end;

  function LastMonth(adate: TDate): Tdate;
    var
      d,m,y:Word;
      val : tdate;
  begin
    DecodeDate(adate,y,m,d);
    if m>1 then
      m := m-1
    else begin
      m := 12;
      y := y-1;
    end;
    If m=2 then begin
      If (d=29) or (d=30) Or (d=31) then
        If y Mod 4 = 0 then
          d:=29
        else
          d:=28;
    end
    else
      If m<6 then begin
        If m mod 2 = 0 then
          d:=31
        else
          d:=30;
      end
      else begin
        If m mod 2 <> 0 then
          d:=31
        else
          d:=30;
      end;
    val := EncodeDate(y,m,d);
    if not IsValidDate(y,m,d) then begin
      Val := Endofthemonth(EncodeDate(y,m,1))
    end;
    Result:=val;
  end;

  function GetMonthYear(aMonth: Integer; aYear: TDate): string;
  var Month: string;
  begin
    if aMonth < 10 then
      Month := '0' + IntToStr(aMonth)
    else Month := IntToStr(aMonth);
    Result:= Month + '/' + Copy(EkstrakString(DateToStr(aYear),'/',3),3,2);
  end;

  procedure getMonths(var target:TStringList);
  begin
    target.Clear;
    target.Add('1=Januari');
    target.Add('2=Februari');
    target.Add('3=Maret');
    target.Add('4=April');
    target.Add('5=Mei');
    target.Add('6=Juni');
    target.Add('7=Juli');
    target.Add('8=Agustus');
    target.Add('9=September');
    target.Add('10=Oktober');
    target.Add('11=November');
    target.Add('12=Desember');
  end;

  procedure getHari(var target:TStringList; ABulanThn: TDate);
  var
  	CountHari, i: Integer;
  begin
    CountHari := DaysInMonth(ABulanThn);
    target.Clear;
    for i := 1 to CountHari do begin
      target.Add(IntToStr(i));
    end;
  end;

  procedure getDayOfWeekList(var target:TStringList);
  begin
    target.Clear;
    target.Add('1=Senin');
    target.Add('2=Selasa');
    target.Add('3=Rabu');
    target.Add('4=Kamis');
    target.Add('5=Jumat');
    target.Add('6=Sabtu');
    target.Add('7=Minggu');
  end;

  function CekTanggal(Input: string): boolean;
  { I.S. : Input terdefinisi
    F.S. : menghasilkan true jika Input merupakan format tanggal yang benar
           sesuai dengan ShortDateFormat }
  var TmpString, Tgl, Bln, Thn : string;
      Valid: boolean;
      i: integer;
  begin
    for i:=1 to 3 do begin
      TmpString:=EkstrakString(ShortDateFormat,DateSeparator,i);
      if (Pos('D',UpperCase(TmpString))<>0) then
        Tgl:=EkstrakString(Input,DateSeparator,2)
      else if (Pos('M',UpperCase(TmpString))<>0) then
        Bln:=EkstrakString(Input,DateSeparator,i)
      else if (Pos('Y',UpperCase(TmpString))<>0) then
        Thn:=EkstrakString(Input,DateSeparator,3);
    end;

    if (not CekInteger(Trim(Tgl))) or (not CekInteger(Trim(Bln))) or (not CekInteger(Trim(Thn))) then
      Valid:=false
    else
      Valid:=IsValidDate(StrToInt(Thn),StrToInt(Bln),StrToInt(Tgl));

    Result:=Valid;
  end;

  function ReverseFormatDate(Tanggal:string): TDate;
  { I.S. : Tanggal terdefinisi dengan format "d mmm yyyy"
    F.S. : menghasilkan TDate sesuai dengan Tanggal }
  var Hasil: TDate;
      i: integer;
  begin
    Tanggal:=TrimAll(UpperCase(Tanggal));
    for i:=1 to 12 do
    begin
      Hasil:=EncodeDate(1,i,1);
      Tanggal:=ReplaceSubStr(Tanggal,UpperCase(FormatDateTime('mmm',Hasil)),IntToStr(i));
    end;
    Hasil:=EncodeDate(StrToInt(EkstrakString(Tanggal,' ',3)),StrToInt(EkstrakString(Tanggal,' ',2)),StrToInt(EkstrakString(Tanggal,' ',1)));

    Result:=Hasil;
  end;

  function getPeriode(aDate:TDate):string;
    var tmp : string;
  begin
     case MonthOf(aDate) of
       1    : tmp:= 'I/';
       2    : tmp:= 'II/';
       3    : tmp:= 'III/';
       4    : tmp:= 'IV/';
       5    : tmp:= 'V/';
       6    : tmp:= 'VI/';
       7    : tmp:= 'VII/';
       8    : tmp:= 'VIII/';
       9    : tmp:= 'IX/';
       10   : tmp:= 'X/';
       11   : tmp:= 'XI/';
       12   : tmp:= 'XII/';
     end;
     Result := '/'+tmp + IntToStr(YearOf(aDate));
  end;

  function getPeriode2(aDate:TDate; YearType: integer):string;
    var tmp : string;
  begin
     case MonthOf(aDate) of
       1    : tmp:= '01/';
       2    : tmp:= '02/';
       3    : tmp:= '03/';
       4    : tmp:= '04/';
       5    : tmp:= '05/';
       6    : tmp:= '06/';
       7    : tmp:= '07/';
       8    : tmp:= '08/';
       9    : tmp:= '09/';
       10   : tmp:= '10/';
       11   : tmp:= '11/';
       12   : tmp:= '12/';
     end;
     if YearType= 1 then
       Result := '/'+tmp + FormatDateTime('yy',aDate)
     else if YearType= 2 then
       Result := '/'+tmp + IntToStr(YearOf(aDate));
  end;

  function getPeriodeBaru(aDate:TDate):string;
    var tmp : string;
  begin
     case MonthOf(aDate) of
       1    : tmp:= '01.';
       2    : tmp:= '02.';
       3    : tmp:= '03.';
       4    : tmp:= '04.';
       5    : tmp:= '05.';
       6    : tmp:= '06.';
       7    : tmp:= '07.';
       8    : tmp:= '08.';
       9    : tmp:= '09.';
       10   : tmp:= '10.';
       11   : tmp:= '11.';
       12   : tmp:= '12.';
     end;
     Result := '.'+tmp + IntToStr(YearOf(aDate));
  end;

  function TambahTanggal(Tgl : TDate; Tambahan : Integer) : TDate;
  var
    tgl_awal, bln_awal, thn_awal, jarak : word;
    tgl_akhir, bln_akhir, thn_akhir : word;
  begin
    //Tahun Kabisat : 366 hari and February 29 hari
    DecodeDate(Tgl, thn_awal, bln_awal, tgl_awal);
    DecodeDate(EndOfTheMonth(Tgl), thn_akhir, bln_akhir, tgl_akhir);
    jarak := tgl_akhir - tgl_awal;
    if Tambahan > jarak then begin
      repeat
        begin
          Tambahan := Tambahan - jarak;
          Inc(bln_awal);
          if bln_awal = 13 then begin
            bln_awal := 1;
            Inc(thn_awal);
          end;
          tgl_awal := 1;
          Tgl := EncodeDate(thn_awal, bln_awal, tgl_awal);
          DecodeDate(EndOfTheMonth(Tgl), thn_akhir, bln_akhir, tgl_akhir);
          jarak := tgl_akhir - tgl_awal;
        end;
      until (Tambahan <= jarak);
    end;
    if tgl_awal = 1 then
      Result := EncodeDate(thn_awal, bln_awal, Tambahan)
    else
      Result := EncodeDate(thn_awal, bln_awal, tgl_awal+Tambahan);
  end;

  function RecodeTheMonth(AValue: TDate; AMonth: integer): TDate;
  var vYear, vMonth, vDay: word;
     vNewMonth: Integer;

  const vMonthInYear = 12;
  begin
    DecodeDate(AValue, vYear, vMonth, vDay);
    vNewMonth:= vMonth + AMonth;

    if vNewMonth < 1 then begin
      vNewMonth:= vMonthInYear + vNewMonth;
      vYear:= vYear - 1;
    end else
    if vNewMonth > vMonthInYear then begin
      vNewMonth:= vNewMonth - vMonthInYear;
      vYear:= vYear + 1;
    end;

    Result:=  EncodeDate(vYear, vNewMonth, 1);
  end;
{ ---------------------------------------------------------------------------- }
  {BERHUBUNGAN DENGAN TStringList}
  procedure tslRemoveName(var Target:TStringList;NameToRemove:string);
  var i:integer;
  begin
    for i:= 0 to Target.Count-1 do begin
      if UpperCase(Target.Names[i]) = UpperCase(NameToRemove) then begin
        Target.Delete(i);
        break;
      end;
    end;
  end;

  procedure tslRemoveValue(var Target:TStringList;ValueToRemove:string);
  var i:integer;
  begin
    for i:= 0 to Target.Count-1 do begin
      if UpperCase(Target.Values[Target.Names[i]]) = UpperCase(valueToRemove) then begin
        Target.Delete(i);
        break;
      end;
    end;
  end;

  function tslIsExist(T: Tstringlist; search: string): boolean;
  begin
    if t.IndexOf(search)<>-1 then
      Result := true
    else
      Result := false;
  end;

  function tslMaxRealValue(b: tstringlist): real;
    var max: real;
        i: integer;
  begin
    max := 0;
    for i := 0 to b.Count-1 do begin
      if i=0 then
        max := strtofloat(b.strings[i])
      else begin
        if (max<strtofloat(b.strings[i])) then
          max := strtofloat(b.strings[i]);
      end;
    end;
    Result := max;
  end;

  function tslSUMRealValue(b: tstringlist): real;
    var sum: real;
        i: integer;
  begin
    sum := 0;
    for i := 0 to b.Count-1 do begin
      if (b.Values[b.Names[i]] <> '') then
        sum := sum + strtofloat(b.Values[b.Names[i]])
      else
        sum := sum + 0;
    end;
    Result := sum;
  end;

  procedure StringListToStrings(AValueList: TStringList; AList: TStrings);
  var i: integer;
  begin
    AList.Clear;
    for i :=0 to AValueList.Count-1 do
      AList.Add(AValueList.Strings[i]);
  end;

  procedure tslNameValueListToNameList(AValueList: TStringList; AList: TStrings);
  var i: integer;
  begin
    AList.Clear;
    for i :=0 to AValueList.Count-1 do
      AList.Add(AValueList.Names[i]);
  end;

  procedure NameValueListToValueList(AValueList: TStringList; AList: TStrings);
  var i: integer;
  begin
    AList.Clear;
    for i :=0 to AValueList.Count-1 do
      AList.Add(AValueList.Values[AValueList.Names[i]]);
  end;

//  procedure NameToValueListComboboxStdJurnal(AValueList: TStringList; AList: TColumnComboBox);
//    var i: integer;
//  begin
//    with AList do begin
//      Columns.Clear;
//      ComboItems.Clear;
//
//      with Columns.Add do
//        begin
//          Width:=75;
//          Font.Style:=[fsBold];
//        end;
//      //with Columns.Add do Width:=50;
//      with Columns.Add do
//        begin
//          Width:=250;
//          Alignment:=taLeftJustify;
//          Font.Color:=clBlue;
//          Font.Style:=[fsBold];
//          //Color     := clMedGray;
//        end;
//    end;
//    for i := 0 to AValueList.Count-1 do begin
//      with AList.ComboItems.Add do begin
//        Strings.Add(TAccSetupMaster.GetKodeStd(StrToInt(AValueList.Names[i])));
//        Strings.Add(AValueList.Values[AValueList.Names[i]]);
//      end;
//    end;
//
//
//  end;

  function tslIndexofValue(aList:TStringList;aValue:string):integer;
  var i:integer;
  begin
    Result := -1;
    for i:= 0 to aList.Count-1 do begin
      if aValue = aList.Values[aList.Names[i]] then begin
        Result := i;
        break; //alin tambah
      end;
    end;
  end;

//  procedure SQLToStringList(var AStringList: TStringList; ASQL: string);
//  var buffer: _Recordset;
//    i: integer;
//  begin
//    AStringList.Clear;
//    buffer := myConnection.OpenSQL(ASQL);
//    for i :=1 to buffer.RecordCount do begin
//      AStringList.Add(VarToStr(buffer.Fields[0].Value));
//      buffer.MoveNext;
//    end;
//    buffer.Close;
//  end;

//  procedure SQLToNameValueList(var AStringList: TStringList; ASQL: string);
//  var buffer: _Recordset;
//    i: integer;
//  begin
//    AStringList.Clear;
//    buffer := myConnection.OpenSQL(ASQL);
//    for i :=1 to buffer.RecordCount do begin
//      AStringList.Add(VarToStr(buffer.Fields[0].Value)+'='+VarToStr(buffer.Fields[1].Value));
//      buffer.MoveNext;
//    end;
//    buffer.Close;
//  end;

//  procedure SQLToNameValueListAll(var AStringList: TStringList; ASQL: string);
//  var buffer: _Recordset;
//    i: integer;
//  begin
//    AStringList.Clear;
//    buffer := myConnection.OpenSQL(ASQL);
//    AStringList.Add('0=Semua');
//    for i :=1 to buffer.RecordCount do begin
//      AStringList.Add(VarToStr(buffer.Fields[0].Value)+'='+VarToStr(buffer.Fields[1].Value));
//      buffer.MoveNext;
//    end;
//    buffer.Close;
//  end;

//  procedure SQLToNameValueList2(var AStringList: TStringList; ASQL: string;
//    ANamesIndex, AValueIndex1, AValueIndex2: integer);
//  var buffer: _Recordset; i: integer;
//  begin
//    AStringList.Clear;
//    buffer := myConnection.OpenSQL(ASQL);
//    for i :=1 to buffer.RecordCount do begin
//      AStringList.Add(VarToStr(buffer.Fields[ANamesIndex].Value)+'='+
//      VarToStr(buffer.Fields[AValueIndex1].Value) + ' ['+ VarToStr(buffer.Fields[AValueIndex2].Value)+']');
//      buffer.MoveNext;
//    end;
//    buffer.Close;
//   end;
   
{ ---------------------------------------------------------------------------- }
  {TENTANG AdvStringGrid}
  procedure asgSetGridColor(aGrid : TAdvStringGrid; aPrimaryColor : TColor; aSecondaryColor : TColor; aFixedColor : TColor; aFixedText: TColor; aGridColor : TColor; aSelectionColor : TColor; aSelectionText: TColor);
  begin
    with aGrid.Bands do begin
       Active:=False;
       PrimaryColor   := aPrimaryColor;
       SecondaryColor := aSecondaryColor;
       Active:=True;
    end;
    aGrid.Color := aGridColor;
    aGrid.FixedColor := aFixedColor;
    aGrid.SelectionColor := aSelectionColor;
    aGrid.FixedFont.Color := aFixedText;
    aGrid.SelectionTextColor := aSelectionText;
  end;

  procedure asgSetAllGridColor(aForm : TForm; aPrimaryColor : TColor = $00E4E4E4; aSecondaryColor : TColor = $00EFEFEF);
  var i : integer;
  begin
   with aForm do begin
     for i:=0 to ComponentCount-1 do begin
       if Components[i] is TAdvStringGrid then
         with (Components[i] as TAdvStringGrid).Bands do begin
           PrimaryColor   := aPrimaryColor;
           SecondaryColor := aSecondaryColor;
           Active:=True;
         end;
     end;
   end;
  end;

  procedure asgSetColumnFitToWidth(aGrid : TADVStringGrid; aBeginCol : Integer = 0);
  var WidthCompare, WidthAll, WidthToSet, WidthTheLast, i,j : Integer;
  begin
    WidthCompare := 0;
    WidthAll     := aGrid.Width - 4;
    WidthToSet   := WidthAll div aGrid.ColCount;
    WidthTheLast := WidthAll mod aGrid.ColCount;
    for i:=aBeginCol to aGrid.ColCount -1 do begin
       if i = aGrid.ColCount-1 then begin
          aGrid.ColWidths[i] := WidthToSet + WidthTheLast;
          for j:=0 to aGrid.ColCount-1 do begin
              WidthCompare := WidthCompare + aGrid.ColWidths[j];
          end;
          if WidthCompare < WidthAll then begin
             WidthTheLast := WidthAll - WidthCompare;
             aGrid.ColWidths[i]:= aGrid.ColWidths[i] + WidthTheLast;
          end;
       end else
          aGrid.ColWidths[i] := WidthToSet;
    end;
  end;

  procedure asgSetTitleText(aGrid : TAdvStringGrid; aText: TStringList = nil; aColCount: Integer = 1; aRow: Integer = 0; aFont: TFont = nil; anAlignment : TAlignMent = taCenter);
  var i:Integer;
  begin
    for i:=0 to aGrid.ColCount-1 do begin
      if aFont = nil then aFont:=TFont.Create;
      try aGrid.Cells[i,aRow]:=aText[i]; except {-- silence} end;
        with aGrid do begin
          Alignments[i,aRow] := anAlignMent;
          FontNames[i,aRow]  := aFont.Name;
          FontStyles[i,aRow] := aFont.Style;
          FontColors[i,aRow] := aFont.Color;
          FontSizes[i,aRow]  := aFont.Size;
        end;
    end;
  end;

  procedure asgSetColumnSize(aGrid : TAdvStringGrid; aColCount: Integer; aText: TStringList = nil);
  var i:Integer;
  begin
    aGrid.ColCount := aColCount;
    for i:=0 to aColCount-1 do begin
      try aGrid.ColWidths[i]:=StrToInt(aText[i]); except {-- silence} end;
    end;
  end;

  procedure asgSetAlignmentCol(aGrid : TAdvStringGrid; aCol: Integer = 0; anAlignment : TAlignMent = taCenter);
  var i:Integer;
  begin
    for i:=0 to aGrid.RowCount-1 do
      aGrid.Alignments[aCol,i]:=anAlignMent;
  end;

  procedure asgSetAlignmentNumber(aGrid : TAdvStringGrid; aCol: Integer = 0; anAlignment : TAlignMent = taCenter);
  var i:Integer;
  begin
    for i:=0 to aGrid.RowCount-1 do
      aGrid.Alignments[aCol,i]:=anAlignMent;
  end;

  procedure asgSetAlignmentTitle(aGrid : TAdvStringGrid; aRow: Integer = 0; anAlignment : TAlignMent = taCenter);
  var i:Integer;
  begin
    for i:=0 to aGrid.ColCount-1 do
      aGrid.Alignments[i,aRow]:=anAlignMent;
  end;

  procedure asgSetAlignmentRow(aGrid : TAdvStringGrid; aRow: Integer = 0; anAlignment : TAlignMent = taCenter);
  var i:Integer;
  begin
    for i:=0 to aGrid.ColCount-1 do
      aGrid.Alignments[i,aRow]:=anAlignMent;
  end;

  procedure asgDeleteLastRows(aGrid : TAdvStringGrid; LimitRow: Integer = 2; DeleteRow : Integer = 1);
  begin
    if (aGrid.RowCount>2) and (aGrid.RowCount>LimitRow) then
        aGrid.RowCount:=aGrid.RowCount-DeleteRow;
    //else aGrid.RowCount:=2;
  end;

  procedure asgSetTextHeaderBold(aParent : TForm; aRow : Integer = 0);
  var i,j : integer;
  begin
    with aParent do
     for i:=0 to ComponentCount-1 do begin
        if Components[i] is TAdvStringGrid then begin
          for j:=0 to (Components[i] as TAdvStringGrid).ColCount-1 do
            (Components[i] as TAdvStringGrid).FontStyles[j,aRow] := [fsBold];
        end;
     end;
  end;

  procedure asgSetTextNumberBold(aParent : TForm; aCol : Integer = 0);
  var i,j : integer;
  begin
    with aParent do
     for i:=0 to ComponentCount-1 do begin
        if Components[i] is TAdvStringGrid then begin
          for j:=0 to (Components[i] as TAdvStringGrid).RowCount-1 do
            (Components[i] as TAdvStringGrid).FontStyles[aCol,j] := [fsBold];
        end;
     end;
  end;

  procedure SetCellFontColor(aGrid : TAdvStringGrid;FontColor: TColor;InitCol,Row:integer);
  var i:integer;
  begin
  for i := InitCol to aGrid.ColCount - 1 do
    aGrid.FontColors[i,Row] := FontColor;
  end;

  procedure asgSetColumnAutoSize(aGrid : TAdvStringGrid; aPadding : Integer = 2; aColNoAffected : Boolean = FALSE);
  var i : integer;
      //vCek, vEmpty : string;
  begin
    //vEmpty := '';
    //for i:=1 to aPadding do vEmpty := vEmpty + ' ';
    for i:=0 to aGrid.ColCount - 1 do begin
      //vCek := aGrid.Cells[i,0];
      //if vCek[1] <> ' ' then
      //if (i <> 0) or (aColNoAffected = TRUE) then
          //aGrid.Cells[i,0] := vEmpty + aGrid.Cells[i,0] + vEmpty;
      aGrid.AutoSizeCol(i);
    end;
  end;

  procedure asgSetRowColor(aMasterGrid: TAdvStringGrid; aRow: Integer; aColor: TColor);
  var
    i : integer;
  begin
    for i:=1 to aMasterGrid.ColCount-1 do
      aMasterGrid.Colors[i, aRow] := aColor;
  end;

  procedure asgSetSortLast(aGrid: TAdvStringGrid);
  begin
    aGrid.SortSettings.Column := aGrid.ColCount - 1;
  end;

  procedure asgClearFooter(aGrid: TAdvStringGrid);
  var i : Integer;
  begin
    if aGrid.FloatingFooter.Visible then
      for i:=0 to aGrid.ColCount-1 do begin
        aGrid.Cells[i,aGrid.RowCount-1] := '';
      end;
  end;

  procedure asgGetGridColor(var MasterColor,
    SecondaryColor: TColor; aThemes: TColor);
  begin
    case aThemes of
      clBlack : begin
         MasterColor    := $00DEFAF0;
         SecondaryColor := clWhite;
      end;
      clBtnFace : begin
         MasterColor    := clWhite;
         SecondaryColor := clWhite;
      end;
      clBlue : begin
         MasterColor    := $00FBCAC6;
         SecondaryColor := $00FDE3E6;
      end;
      clGreen : begin
         MasterColor    := $00BDEAD0;
         SecondaryColor := $00DEFAF0;
      end;
      clPurple : begin
         MasterColor    := $00EDD8FE;
         SecondaryColor := $00FCE7FE;
      end;
      clYellow : begin
         MasterColor    := $00AFCFFA;
         SecondaryColor := $00DAF0FC;
      end;
      clMaroon : begin
         MasterColor    := $00CFCFE7;
         SecondaryColor := $00EFEFF8;
      end;
      clSilver : begin
         MasterColor    := $00D2D2D2;
         SecondaryColor := $00F8F8F8;
      end;
      Else begin
         MasterColor    := clWhite;
         SecondaryColor := clWhite;
      end;
    end;
  end;

  function asgGetSummary(aGrid: TAdvStringGrid; aCol: Integer): Real;
  var i: integer;
      vTotal: Real;
  begin
    vTotal := 0;
    for i:=0 to aGrid.RowCount-1 do begin
      if aGrid.Floats[aCol, i]<>0 then begin
        vTotal := vTotal + StrFmtToFloat(aGrid.Cells[aCol, i]);
      end;
    end;
    Result := vTotal;
  end;

  function asgGetSummaryMultiple(aGrid: TAdvStringGrid; aCol1, aCol2: Integer): Real;
  var i: integer;
      vTotal: Real;
  begin
    vTotal := 0;
    for i:=0 to aGrid.RowCount-1 do begin
      if aGrid.Floats[aCol1, i]<>0 then begin
        if aGrid.Floats[aCol2, i]<>0 then begin
          //vTotal := vTotal + (StrFmtToFloat(aGrid.Cells[aCol1, i]) * StrFmtToFloat(aGrid.Cells[aCol2, i]));
          vTotal := vTotal + (aGrid.Floats[aCol1, i] * aGrid.Floats[aCol2, i]);
        end;
      end;
    end;
    Result := vTotal;
  end;


  function asgFindTextOnGrid(var aRow: Integer; aBeginRow: Integer; aSearchCol: Integer; aGrid: TAdvStringGrid; aFindText: String):Boolean;
  var i, j: Integer;
      Found: Boolean;
      TmpStr, Check: String;
  begin
    TmpStr:=UpperCase(aFindText);
    Found := False;
    for i := aBeginRow to aGrid.RowCount do begin
      for j := 1 to Length(aGrid.Cells[aSearchCol, i-1]) do begin
        Check := UpperCase(Copy(aGrid.Cells[aSearchCol, i-1],j,Length(TmpStr)));
        if Check = TmpStr then begin
          Found := True;
          aRow := i;
          if Found then Break;
        end;
      end;
      if Found then Break;
    end;
    Result:= Found;
  end;

  procedure asgSetInputCellColor(aForm: TForm; aGrid: TAdvStringGrid; aCols: array of integer;
    aColsClick: array of integer; aRowBegin: Integer; aRowEnd: Integer; aColorClick, aColorInput: TColor);
  var i,j,k: integer;
  begin
    with aForm do begin
      if aRowBegin = -1 then aRowBegin := 1;
      if aRowEnd = -1 then aRowEnd := aGrid.RowCount-1;
      aGrid.Bands.Active := False;
      for i:=aRowBegin to aRowEnd do begin
        for k:=0 to aGrid.ColCount-1 do begin
         if k >= aGrid.FixedCols then
           aGrid.Colors[k,i] := aGrid.Bands.PrimaryColor;
        end;
      end;
      for i:=aRowBegin to aRowEnd do begin
        if length(aCols) <> 0 then begin
          for j:=0 to length(aCols)-1 do begin
            for k:=0 to aGrid.ColCount-1 do begin
              if k = aCols[j] then
                aGrid.Colors[aCols[j],i] := aColorInput
            end;
          end;
        end;
        if length(aColsClick) <> 0 then begin
          for j:=0 to length(aColsClick)-1 do begin
            aGrid.Colors[aColsClick[j],i] := aColorClick;
          end;
        end;
      end;
    end;
  end;

  function asgGetSummaryCols(aGrid: TAdvStringGrid; aCol: Integer; aRowFrom, aRowTo: Integer): Real;
  var i: integer;
      vTotal: Real;
  begin
    vTotal := 0;
    for i:=aRowFrom to aRowTo do begin
      if aGrid.Floats[aCol, i]<>0 then begin
        vTotal := vTotal + StrFmtToFloat(aGrid.Cells[aCol, i]);
      end;
    end;
    Result := vTotal;
  end;

  function asgGetSummaryRows(aGrid: TAdvStringGrid; aRow: Integer; aColFrom, aColTo: Integer): Real;
  var i: integer;
      vTotal: Real;
  begin
    vTotal := 0;
    for i:=aColFrom to aColTo do begin
      if aGrid.Floats[i, aRow]<>0 then begin
        vTotal := vTotal + StrFmtToFloat(aGrid.Cells[i, aRow]);
      end;
    end;
    Result := vTotal;
  end;

  function asgGetSummaryRowsEx(aGrid: TAdvStringGrid; aRow: Integer; aColFrom, aColTo: Integer; FirstOpr: String; ReverseOprIn: Integer): Real;
  var i: integer;
      vTotal: Real;
      vOpr: String;
  begin
    vTotal := 0;
    vOpr   := FirstOpr;
    for i:=aColFrom to aColTo do begin
      if aGrid.Floats[i, aRow]<>0 then begin
        if i = ReverseOprIn then vOpr := IfThen(vOpr = '+','-','+');
        if vOpr = '+' then vTotal := vTotal + StrFmtToFloat(aGrid.Cells[i, aRow]);
        if vOpr = '-' then vTotal := vTotal - StrFmtToFloat(aGrid.Cells[i, aRow]);
      end;
    end;
    Result := vTotal;
  end;

  procedure asgFillGridBuffer(buffer: _RecordSet; var Grid: TAdvStringGrid; StartCol,StartRow: integer; isFLoatFoot : Boolean = False);
  { I.S. : buffer terdefinisi (hasil query), Grid sembarang, StartCol
           adalah kolom paling kiri untuk diisi, StartRow adalah baris paling
           atas untuk diisi
    F.S. : grid terisi data dari buffer sesuai dengan urutan buffer }
  var i, j: integer;
  begin
    Grid.ClearNormalCells;
    if isFLoatFoot then
      Grid.RowCount := StartRow + buffer.RecordCount + 1
    else Grid.RowCount := StartRow + buffer.RecordCount;

    for i:=0 to buffer.RecordCount-1 do begin
      for j:=0 to buffer.Fields.Count-1 do begin
        case VarType(buffer.Fields[j].Value) of
          varNull: Grid.Cells[StartCol+j,StartRow+i]:='';
          varDate: Grid.Dates[StartCol+j,StartRow+i]:=buffer.Fields[j].Value;
          varSmallint, varInteger, varShortInt, varByte, varWord, varLongword, varInt64: Grid.Ints[StartCol+j,StartRow+i]:=buffer.Fields[j].Value;
          varSingle, varDouble, varCurrency: Grid.Floats[StartCol+j,StartRow+i]:=buffer.Fields[j].Value;
          else Grid.Cells[StartCol+j,StartRow+i]:=buffer.Fields[j].Value;
        end;
      end;
      buffer.MoveNext;
    end;
  end;

  procedure asgEditToGrid(adv: TAdvStringGrid; kolom, baris: integer;
    editbox: tadvedit);
  begin
    adv.Cells[kolom, baris] := editbox.Text;
  end;

  procedure asgTextToGrid(adv: TAdvStringGrid; kolom, baris: integer;
    Text: string);
  begin
    adv.Cells[kolom, baris] := Text;
  end;

  procedure asgDateToGrid(adv: TAdvStringGrid; kolom, baris: integer;
    InputDate: TDateTimePicker);
  begin
    adv.Dates[kolom, baris] := InputDate.date;
  end;

  procedure asgDeleteRow(grid: TAdvStringGrid; row: integer; idcol: integer);
  begin
    if (grid.Cells[idcol, row]<>'') then begin
      grid.Clearrows(row, 1);
      if (grid.RowCount<>2) then begin
        grid.Clearrows(row, 1);
        grid.RemoveRows(row, 1);
      end;
    end;
  end;

  procedure PreparePrintDataGrid(aGrid : TAdvStringGrid; AIsHide: Boolean);
  var i: Integer;
  begin
    	if AIsHide then begin
      	aGrid.ColumnSize.Stretch := False;
      	aGrid.ColumnSize.StretchColumn := 1;
        for i := aGrid.ColCount downto 1 do
          if aGrid.ColWidths[i] = 0 then
          	aGrid.HideColumn(i);
        //aGrid.HideColumn(aGrid.ColCount-1);
      end else begin

      	for i := aGrid.ColCount downto 1 do
          if aGrid.ColWidths[aGrid.RealColIndex(i)] = 0 then
          	aGrid.HideColumn(aGrid.RealColIndex(i));
        //aGrid.UnHideColumn(aGrid.ColCount-1);
      	aGrid.ColumnSize.StretchColumn := aGrid.ColCount-1;
       	aGrid.ColumnSize.Stretch := True;
      end;
  end;

//  procedure ExportGridToExcell(var aGrid : TAdvStringGrid; aDialogs : TSaveDialog; ACellFormat: Boolean);
//    procedure SetGridColum(AIsHide: Boolean);
//    var i: Integer;
//    begin
//    	if AIsHide then begin
//      	aGrid.ColumnSize.Stretch := False;
//      	aGrid.ColumnSize.StretchColumn := 1;
//        for i := aGrid.ColCount downto 1 do
//          if aGrid.ColWidths[aGrid.RealColIndex(i)] = 0 then
//          	aGrid.HideColumn(aGrid.RealColIndex(i));
//      end else begin
//      	for i := aGrid.ColCount downto 1 do
//          if aGrid.ColWidths[aGrid.RealColIndex(i)] = 0 then
//          	aGrid.HideColumn(aGrid.RealColIndex(i));
//      	aGrid.ColumnSize.StretchColumn := aGrid.ColCount-1;
//       	aGrid.ColumnSize.Stretch := True;
//      end;
//    end;
//
//  begin
//    if aDialogs.Execute then begin
//      myConnection.AdcXls.AdvStringGrid := aGrid;
//      myConnection.AdcXls.GridStartRow := 0;
//      myConnection.AdcXls.GridStartCol := 0;
//      myConnection.AdcXls.Options.ExportCellSizes := True;
//      myConnection.AdcXls.Options.ExportHiddenColumns := False;
//      myConnection.AdcXls.Options.ExportCellProperties := False;
//      myConnection.AdcXls.Options.ExportCellFormats := ACellFormat;
//      myConnection.AdcXls.Options.ExportFormulas := True;
//      //myConnection.AdcXls.Options.ExportShowInExcel := True;
//      myConnection.AdcXls.AutoResizeGrid := False;
//      if FileExists(aDialogs.FileName) then begin
//        if Confirmed(aDialogs.FileName+' already exist.'+#13+'Do you want to replace it?') then begin
//            DeleteFile(aDialogs.FileName);
//            SetGridColum(True);
//            myConnection.AdcXls.XLSExport(aDialogs.FileName);
//            SetGridColum(False);
//        end else exit;
//      end else begin
//        SetGridColum(True);
//        myConnection.AdcXls.XLSExport(aDialogs.FileName);
//        SetGridColum(False);
//    	end;
//
//    end;
//  end;

  procedure ResetGrid(Sender: TAdvStringGrid; DefaultRowCount,DefaultColCount,
                      DefaultFixedRows, DefaultFixedCols: integer);
  begin
    Sender.Update;
    Sender.ClearNormalCells;
    Sender.RowCount                 := DefaultRowCount;
    Sender.ColCount                 := DefaultColCount;
    Sender.FixedRows                := DefaultFixedRows;
    Sender.RemoveAllNodes;
    Sender.AutoSize                 := False;
    Sender.ColumnSize.Stretch       := False;
    //Sender.FixedFont.Style         := [fsBold];
    Sender.FixedCols                := DefaultFixedCols;
    Sender.ColumnSize.StretchColumn := DefaultColCount-1;
    Sender.ColumnSize.Stretch       := True;
    Sender.ClearRows(Sender.RowCount-1, 1);
    Sender.AutoSizeColumns(true,2);
  end;

  procedure asgSetCellColor(aMasterGrid: TAdvStringGrid; aRow: Integer; aColor: TColor);
  var
    i : integer;
  begin
    for i:=1 to aMasterGrid.ColCount-1 do
      aMasterGrid.Colors[i, aRow] := aColor;
  end;

  procedure SetRowFontStyle(aMasterGrid: TAdvStringGrid; aFromCol, aRow: Integer; aStyle: TFontStyle);
  var
    i : integer;
  begin
    for i:= aFromCol to aMasterGrid.ColCount-1 do
      aMasterGrid.FontStyles[i, aRow] := [aStyle];
  end;

  procedure asgSetRowFontStyleNormal(aMasterGrid: TAdvStringGrid; aRow: Integer; AColor: TColor);
  var
    i : integer;
    vFont : TFont;
  begin
    vFont := TFont.Create;
    vFont.Style := [];
    if AColor <> 0 then
      vFont.color := AColor
    else
      vFont.Color := clBlack;
    for i:=0 to aMasterGrid.ColCount-1 do begin
      aMasterGrid.FontStyles[i, aRow] := vFont.Style;
      aMasterGrid.FontColors[i, aRow] := vFont.Color;
    end;
    vFont.Free;
  end;

  procedure asgSetFixedRowFontStyleNormal(aMasterGrid: TAdvStringGrid; AFixRow: Integer);
  begin
        aMasterGrid.FixedFont.Style:= [];
  end;

  procedure SetRowColor(aMasterGrid: TAdvStringGrid; aFromCol, aRow: Integer; aColor: TColor);
  var
  i : integer;
  begin
  for i:= aFromCol to aMasterGrid.ColCount-1 do
    aMasterGrid.Colors[i, aRow] := aColor;
  end;

  procedure HapusBaris(grid: TAdvStringGrid; DefoultRowCount, row: integer);
  begin
    if Confirmed('Hapus Baris ke - '+IntToStr(row)) then begin
      if grid.RowCount = DefoultRowCount then
        grid.ClearRows(row,1)
      else begin
        grid.ClearRows(row,1);
        grid.RemoveRows(row,1);
      end;
    end;

  end;

{ ---------------------------------------------------------------------------- }
  {KHUSUS RADIO BUTTON}
  procedure rbtSetAllCheckedFalse(aForm : TForm; aChecked : Boolean = FALSE);
  var i: Integer;
  begin
   with aForm do begin
     for i:=0 to ComponentCount-1 do begin
       if (Components[i] is TRadioButton) then begin
           TRadioButton(Components[i]).Checked:=aChecked;
       end;
     end;
   end;
  end;

  procedure rbtSetAllEnabledFalse(aForm : TForm; aEnabled : Boolean = FALSE);
  var i: Integer;
  begin
   with aForm do begin
     for i:=0 to ComponentCount-1 do begin
       if (Components[i] is TRadiobutton) then begin
           TRadiobutton(Components[i]).Enabled := aEnabled;
       end;
     end;
   end;
  end;
{ ---------------------------------------------------------------------------- }
  {KHUSUS CHECK BOX}
  procedure cbxSetAllCheckedFalse(aForm : TForm; aChecked : Boolean = FALSE);
  var i: Integer;
  begin
   with aForm do begin
     for i:=0 to ComponentCount-1 do begin
       if (Components[i] is TCheckbox) then begin
           TCheckbox(Components[i]).Checked:=aChecked;
       end;
     end;
   end;
  end;

  procedure cbxSetAllEnabledFalse(aForm : TForm; aEnabled : Boolean = FALSE);
  var i: Integer;
  begin
   with aForm do begin
     for i:=0 to ComponentCount-1 do begin
       if (Components[i] is TCheckbox) then begin
           TCheckbox(Components[i]).Enabled := aEnabled;
       end;
     end;
   end;
  end;

  procedure SetFilterSize(Sender: TPanel; AButton: TButton; AShowSize:integer);
  begin
    Sender.Height:= IfThen(Sender.Height <> 0, 0, AShowSize);
    AButton.Caption:= IfThen(Sender.Height <> 0, 'Tutup &Filter', 'Tampil &Filter');
  end;

  procedure SetFilterSizeAdv(Sender: TPanel; AButton: TAdvGlowButton; AShowSize:integer);
  begin
    Sender.Height:= IfThen(Sender.Height <> 0, 0, AShowSize);
    AButton.Caption:= IfThen(Sender.Height <> 0, 'Tutup &Filter', 'Tampil &Filter');
  end;

  procedure SetFilterGroupSize(Sender: TAdvGroupBox; AButton: TAdvGlowButton; AShowSize:integer);
  begin
    Sender.Height:= IfThen(Sender.Height <> 0, 0, AShowSize);
    AButton.Caption:= IfThen(Sender.Height <> 0, 'Tutup &Filter', 'Tampil &Filter');
  end;
{ ---------------------------------------------------------------------------- }
  {KHUSUS EDIT}
  procedure txtSetAllEmpty(aForm : TForm);
  var i: Integer;
  begin
   with aForm do begin
     for i:=0 to ComponentCount-1 do begin
       if (Components[i] is TEdit) then TEdit(Components[i]).Clear;
       if (Components[i] is TAdvEdit) then begin
          case TAdvedit(Components[i]).EditType of
            etNumeric  : TAdvedit(Components[i]).IntValue := 0;
            etFloat, etMoney : TAdvedit(Components[i]).FloatValue := 0;
            else TAdvedit(Components[i]).Clear;
          end;
       end;
       if (Components[i] is TMemo) then TMemo(Components[i]).Clear;
     end;
   end;
  end;
{ ---------------------------------------------------------------------------- }
  {KHUSUS COMBO BOX}
  function cmbSetListValue(Combo: TComboBox;
    NewValue: string): Integer;
  begin
    Result := Combo.Items.IndexOf(NewValue);
    Combo.ItemIndex := Result;
  end;


  procedure cmbSetAllItemsClear(aForm : TForm); {-- membuat semua items combo jd clear}
  var i: Integer;
  begin
   with aForm do begin
     for i:=0 to ComponentCount-1 do begin
       if (Components[i] is TComboBox) then begin
           TComboBox(Components[i]).Clear;
       end;
     end;
   end;
  end;

  procedure cmbSetAllItemIndex(aForm : TForm; Idx : integer = -1);
  var i: Integer;
  begin
   with aForm do begin
     for i:=0 to ComponentCount-1 do begin
       if (Components[i] is TComboBox) then begin
           TComboBox(Components[i]).ItemIndex := idx;
       end;
     end;
   end;
  end;

  procedure SetComboboxSize(Sender: TComboBox; NewWidth:integer);
  begin
    Sender.Width:= NewWidth;
  end;
{ ---------------------------------------------------------------------------- }
 {KHUSUS GROUP BOX}
  procedure grbSetAllEnabledFalse(aForm : TForm; aEnabled : Boolean = FALSE); {-- membuat semua groupbox enablednya jadi false}
  var i: Integer;
  begin
   with aForm do begin
     for i:=0 to ComponentCount-1 do begin
       if (Components[i] is TGroupBox) then begin
           TGroupBox(Components[i]).Enabled := aEnabled;
       end;
     end;
   end;
  end;

//function getStringFromSQL(aSql:string):string;
//var buffer :_Recordset;
// {} begin
////    buffer := myConnection.OpenSQL(aSql);
////    Result := '';
////    if buffer.RecordCount > 0 then
////      Result := BufferToString(buffer.Fields[0].Value);
////    buffer.Close;
//  end;

//function getFloatFromSQL(aSql:string):double;
//var buffer :_Recordset;
//  begin
//    buffer := myConnection.OpenSQL(aSql);
//    Result := 0;
//   if buffer.RecordCount > 0 then
//     Result := BufferToFloat(buffer.Fields[0].Value);
//   buffer.Close;
//  end;
//
//function getIntegerFromSQL(aSql:string):integer;
//var buffer :_Recordset;
//  begin
//    buffer := myConnection.OpenSQL(aSql);
//    Result := 0;
//    if buffer.RecordCount > 0 then
//      Result := BufferToInteger(buffer.Fields[0].Value);
//    buffer.Close;
//  end;
//
//function getBooleanFromSQL(aSql:string):boolean;
//var buffer :_Recordset;
//  begin
//    buffer := myConnection.OpenSQL(aSql);
//    Result := False;
//   if buffer.RecordCount > 0 then
//     Result := BufferToBoolean(buffer.Fields[0].Value);
//   buffer.Close;
//  end;
//
//function getTDateFromSQL(aSql:string):TDate;
//var buffer :_Recordset;
//  begin
//    buffer := myConnection.OpenSQL(aSql);
//    Result := 0;
//    if buffer.RecordCount > 0 then
//      Result := BufferToDateTime(buffer.Fields[0].Value);
//    buffer.Close;
//  end;
//
//function ExecuteSQL(aSql:string):boolean;
// begin
//    try myConnection.BeginSQL;
//      myConnection.ExecSQL(aSql);
//      myConnection.EndSQL;
//      Result := True;
//    except
//      myConnection.UndoSQL;
//      Result := False;
//    end;
// end;
//
//function ExecTransaction(SQL: string;
//  InsertMode: boolean): boolean;
//begin
//  try
//    myConnection.BeginSQL;
//    myConnection.ExecSQL(SQL);
//    myConnection.EndSQL;
//    Inform(IfThen(InsertMode,MSG_SUCCESS_SAVING,MSG_SUCCESS_UPDATE));
//    Result:= true;
//    except
//      myConnection.UndoSQL;
//      Inform(IfThen(InsertMode,MSG_UNSUCCESS_SAVING,MSG_UNSUCCESS_UPDATE));
//      Result:= false;
//  end;
//end;
//
//function ExecTransaction2(SQL: string;
//  InsertMode: boolean): boolean;
//begin
//  try
//    myConnection.BeginSQL;
//    myConnection.ExecSQL(SQL);
//    myConnection.EndSQL;
//    Result:= true;
//    except
//      myConnection.UndoSQL;
//      Result:= false;
//  end;
//end;
//
//function ExecTransaction3(SQL: string): boolean;
//begin
//  try
//    myConnection.ExecSQL(SQL);
//    Result:= true;
//    except
//      raise;
//      Result:= false;
//  end;
//end;
//
//function ExecTransaction4(SQL: string): boolean;
//begin
//  try
//    myConnection.BeginSQL;
//    myConnection.ExecSQL(SQL);
//    myConnection.EndSQL;
//    Inform(MSG_SUCCESS_DELETING);
//    Result:= true;
//    except
//      myConnection.UndoSQL;
//      Inform(MSG_UNSUCCESS_DELETING);
//      Result:= false;
//  end;
//end;
//function ExecAktivasi(SQL: string;
//  isAktif: boolean): boolean;
//begin
//  try
//    myConnection.BeginSQL;
//    myConnection.ExecSQL(SQL);
//    myConnection.EndSQL;
//    //Inform(IfThen(isAktif, MSG_SUCCESS_AKTIVASI,MSG_SUCCESS_NOT_AKTIVASI));
//    Result:= true;
//    except
//      myConnection.UndoSQL;
//      //Inform(IfThen(isAktif, MSG_UNSUCCESS_AKTIVASI, MSG_UNSUCCESS_NOT_AKTIVASI));
//      Result:= false;
//  end;
//end;
//
//function ExecDelete(SQL: string): boolean;
//begin
//  try
//    myConnection.BeginSQL;
//    myConnection.ExecSQL(SQL);
//    myConnection.EndSQL;
//   // Inform(MSG_SUCCESS_DELETING);
//    Result:= true;
//    except
//      myConnection.UndoSQL;
//    //  Inform(MSG_UNSUCCESS_DELETING);
//      Result:= false;
//  end;
//end;


//function getNextIDNumPeriode(fieldName:string;tableName:string;condition:String;prefix,suffix:string;reversedOrderNum:string='';minlength:integer=5):string;
//var sql:string;buffer:_RecordSet;code:string; codeNum:integer;codeChar:Char; cond: string;
//begin
//  if reversedOrderNum <> '' then begin
//    //get the last character in the number if it is non-number
//    codeChar := (copy(reversedOrderNum,length(reversedOrderNum)-1,1))[1];
//    if StrToIntDef(codeChar,0) = 0 then begin//if not-number then
//      inc(codeChar);
//      result := reversedOrderNum + codeChar;
//    end else //if number than append 'a'
//      result := reversedOrderNum + 'a';
//  end else begin
//    sql := 'SELECT MAX('+fieldName+') FROM '+tableName;
//    cond := '';
//    if condition <> '' then
//      cond := cond + ' AND ' + copy(condition,5,length(condition));
//    if prefix <> '' then
//      cond := cond + ' AND ' + fieldname + ' LIKE ' + FormatSQLString(prefix+'%'+suffix);
//    if cond <> '' then begin
//      delete(cond,1,5);
//      sql := sql + ' WHERE ' + cond;
//    end;
//    buffer := myConnection.OpenSQL(sql);
//    if buffer.RecordCount > 0 then begin
//    //'STL/MFG/PPC/POI00001/V/2004'
//      code := BufferToString(buffer.Fields[0].Value);
//      //cut off the prefix the last digit if it is a character
//      code := copy(code,length(prefix)+1,length(code)-length(prefix));
//      code := EkstrakString(code,'/',1);
//      codeNum := StrToIntDef(code,0);
//      inc(codeNum);
//      result := prefix+IntToStrFixed(minlength,codeNum);
//    end else
//      result := prefix+IntToStrFixed(minlength,1);
//    buffer.Close;
//  end;
//end;
//
//function getNextIDNumPeriode2(fieldName:string;tableName:string;condition:String;prefix,suffix:string;reversedOrderNum:string='';minlength:integer=5):string;
//var sql:string;buffer:_RecordSet;code:string; codeNum:integer;codeChar:Char; cond: string;
//begin
//  if reversedOrderNum <> '' then begin
//    //get the last character in the number if it is non-number
//    codeChar := (copy(reversedOrderNum,length(reversedOrderNum)-1,1))[1];
//    if StrToIntDef(codeChar,0) = 0 then begin//if not-number then
//      inc(codeChar);
//      result := reversedOrderNum + codeChar;
//    end else //if number than append 'a'
//      result := reversedOrderNum + 'a';
//  end else begin
//                      //010.000.09.00000018
//                      //010.000-08.00000022
//    sql := 'SELECT MAX(SUBSTRING('+fieldName+',12,8)) FROM '+tableName;
//    cond := '';
//    if condition <> '' then
//      cond := cond + ' AND ' + copy(condition,5,length(condition));
//    if prefix <> '' then
//      cond := cond + ' AND ' + fieldname + ' LIKE ' + FormatSQLString('%'+suffix);//+' and Length(Inv_Num)=19';
//    if cond <> '' then begin
//      delete(cond,1,5);
//      sql := sql + ' WHERE ' + cond;
//    end;
//    buffer := myConnection.OpenSQL(sql);
//    if buffer.RecordCount > 0 then begin
//    //'STL/MFG/PPC/POI00001/V/2004'
//      code := BufferToString(buffer.Fields[0].Value);
//      //cut off the prefix the last digit if it is a character
//     // code := copy(code,length(prefix)+1,length(code)-length(prefix));
//      code := EkstrakString(code,'/',1);
//      codeNum := StrToIntDef(code,0);
//      inc(codeNum);
//      result := prefix+IntToStrFixed(minlength,codeNum);
//    end else
//      result := prefix+IntToStrFixed(minlength,1);
//    buffer.Close;
//  end;
//end;

procedure LimitDecimal(Sender: TObject; aCol: Integer; var aKey: Char);
begin
  if Sender is TAdvStringGrid then
    if (Sender as TAdvStringGrid).Col = aCol then begin
      if aKey = ThousandSeparator then begin aKey := DecimalSeparator; Exit; end;
      if not (aKey in [DecimalSeparator,'0'..'9',#8]) then aKey := #0;
    end;
  if Sender is TAdvEdit then
    if not (aKey in [DecimalSeparator,'0'..'9',#8]) then
      aKey := #0;
end;

//function CaptionPeriode(op1:TSQLOperator;Date1:TDate;Date2:TDate):string;
//var hasil : string;
//begin
//   hasil := 'Periode Global';
//   if (Date1 <> 0) and (Date2 <> 0) then
//      hasil := 'Periode '+FormatDateTime(ShortDateFormat,Date1) + ' s/d '+FormatDateTime(ShortDateFormat,Date2)
//   else if Date1<> 0 then begin
//     {soGreaterThan,
//     soGreaterThanEqualsTo,soEquals,soLessThan, soLessThanEqualsTo);}
//      if (op1 = soEquals) or (Trunc(Date1) = Trunc(Date2)) then
//        hasil := 'Per Tanggal '+ FormatDateTime(ShortDateFormat,Date1)
//      else if (op1 = soGreaterThanEqualsTo) or (op1 = soGreaterThan) then
//        hasil := 'Periode Dari '+ FormatDateTime(ShortDateFormat,Date1)+' Sampai Sekarang'
//      else if (op1 = soLessThanEqualsTo) or (op1=soLessThan) then
//        hasil := 'Periode Awal s/d '+ FormatDateTime(ShortDateFormat,Date1);
//   end;
//   Result := hasil;
//end;

function InputNote: string;
begin
  Result:= InputBox('Notes', 'Entry Notes','');
end;

procedure SetAutoMoney(Sender: TObject; var aKey: Char);
begin
  if Sender is TAdvEdit then
    if not (aKey in [DecimalSeparator,'0'..'9',#8]) then
      aKey := #0;
end;

//function getNextIDNumPeriodeNonPrefix(fieldName:string;tableName:string;condition:String;prefix,suffix:string;reversedOrderNum:string='';minlength:integer=5):string;
//var sql:string;buffer:_RecordSet;code:string; codeNum:integer;codeChar:Char; cond: string;
//begin
//  if reversedOrderNum <> '' then begin
//    //get the last character in the number if it is non-number
//    codeChar := (copy(reversedOrderNum,length(reversedOrderNum)-1,1))[1];
//    if StrToIntDef(codeChar,0) = 0 then begin//if not-number then
//      inc(codeChar);
//      result := reversedOrderNum + codeChar;
//    end else //if number than append 'a'
//      result := reversedOrderNum + 'a';
//  end else begin
//    sql := 'SELECT MAX('+fieldName+') FROM '+tableName;
//    cond := '';
//    if condition <> '' then
//      cond := cond + ' AND ' + copy(condition,5,length(condition));
//    if prefix <> '' then
//      cond := cond + ' AND ' + fieldname + ' LIKE ' + FormatSQLString(prefix+'%'+suffix);
//    if cond <> '' then begin
//      delete(cond,1,5);
//      sql := sql + ' WHERE ' + cond;
//    end;
//    buffer := myConnection.OpenSQL(sql);
//    if buffer.RecordCount > 0 then begin
//    //'STL/MFG/PPC/POI00001/V/2004'
//      code := BufferToString(buffer.Fields[0].Value);
//      //cut off the prefix the last digit if it is a character
//      code := copy(code,length(prefix)+1,length(code)-length(prefix));
//      code := EkstrakString(code,'/',1);
//      codeNum := StrToIntDef(code,0);
//      inc(codeNum);
//      result := IntToStrFixed(minlength,codeNum);
//    end else
//      result := IntToStrFixed(minlength,1);
//    buffer.Close;
//  end;
//end;

function AutoThousandSep(s: string): string;
begin
  while (Pos(ThousandSeparator, s) > 0) do
    Delete(s, Pos(ThousandSeparator, s), 1);
  Result := s;
end;

function SetText(value: string): String;
var
  fmt, neg: string;
  f: extended;
begin
{  if (value <> '') then begin
    if (Pos('-', value) > 0) then neg := '-' else neg := '';
      fmt := '%.' + IntToStr(FPrecision) + 'n';
      Value := Format(fmt, [EStrToFloat(Value)]);
    end;

    if (FEditType in [etFloat]) then
    begin
      fmt := '%.' + inttostr(FPrecision) + 'f';
      f := EStrToFloat(value);
      Value := Format(fmt, [f]);
    end;
  end;

  if (FEditType in [etHex]) then
    Value := AnsiUpperCase(value);

  inherited Text := FPrefix + Value + FSuffix;

  SetModified(False);

  if FShowURL then ApplyURL(TestURL);      }
end;

function FindTextOnGrid(var aRow: Integer; aBeginRow: Integer; aSearchCol: Integer; aGrid: TAdvStringGrid; aFindText: String):Boolean;
var i, j: Integer;
    Found: Boolean;
    TmpStr, Check: String;
begin
  TmpStr:=UpperCase(aFindText);
  Found := False;
  for i := aBeginRow to aGrid.RowCount do begin
    for j := 1 to Length(aGrid.Cells[aSearchCol, i-1]) do begin
      Check := UpperCase(Copy(aGrid.Cells[aSearchCol, i-1],j,Length(TmpStr)));
      if Check = TmpStr then begin
        Found := True;
        aRow := i;
        if Found then Break;
      end;
    end;
    if Found then Break;
  end;
  Result:= Found;
end;

//function getNextIDNum(fieldName:string;tableName:string;condition:String;prefix:string;reversedOrderNum:string='';minlength:integer=5):string;
//var sql:string;buffer:_RecordSet;code:string; codeNum:integer;codeChar:Char; cond: string;
//begin
//  if reversedOrderNum <> '' then begin
//    //get the last character in the number if it is non-number
//    codeChar := (copy(reversedOrderNum,length(reversedOrderNum)-1,1))[1];
//    if StrToIntDef(codeChar,0) = 0 then begin//if not-number then
//      inc(codeChar);
//      result := reversedOrderNum + codeChar;
//    end else //if number than append 'a'
//      result := reversedOrderNum + 'a';
//  end else begin
//    sql := 'SELECT MAX('+fieldName+') FROM '+tableName;
//    cond := '';
//    if condition <> '' then
//      cond := cond + ' AND ' + copy(condition,5,length(condition));
//    if prefix <> '' then
//      cond := cond + ' AND ' + fieldname + ' LIKE ' + FormatSQLString(prefix+'%');
//    if cond <> '' then begin
//      delete(cond,1,5);
//      sql := sql + ' WHERE ' + cond;
//    end;
//    buffer := myConnection.OpenSQL(sql);
//    if buffer.RecordCount > 0 then begin
//      code := BufferToString(buffer.Fields[0].Value);
//      //cut off the prefix the last digit if it is a character
//      code := copy(code,length(prefix)+1,length(code)-length(prefix));
//      code := EkstrakString(code,'/',1);
//      codeNum := StrToIntDef(code,0);
//      inc(codeNum);
//      result := prefix+IntToStrFixed(minlength,codeNum);
//    end else
//      result := prefix+IntToStrFixed(minlength,1);
//    buffer.Close;
//  end;
//end;
//
//function getNextNumPajakBru(ADate: TDate): string;
//var prefix, tmp: string;
//begin
//    prefix := '010.000.'+FormatDateTime('yy',ADate)+'.';
//    tmp    := getNextIDNumPeriode2('no_ref','trx_histori',' AND tanggal >= '+FormatSQLDate(StartOfTheYear(ADate))+
//                                                          ' AND tanggal <= '+FormatSQLDate(EndOfTheYear(ADate))+
//                                    ' and id_kategori = 29 ', prefix, prefix,'',8);
//    Result := tmp;
//end;
//
//function ExecTransactionKhusus(SQL: string): boolean;
//begin
//  try
//    myConnection.ExecSQLKhusus(SQL);
//    Result:= true;
//  except raise;
//  end;
//
//end;

procedure SetColumnComboboxSize(Sender: TObject; NewWidth:integer); overload;
begin
  (Sender as TColumnComboBox).Width:= NewWidth;
end;

function setAccountView(accNum :integer):string;
  var vtmp : string;
  begin
     vtmp := IntToStr(accNum);
     Result := copy(vtmp,1,2)+'.'+copy(vtmp,3,2)+'.'+copy(vtmp,5,3);
//     Result := copy(vtmp,1,3)+'.'+copy(vtmp,4,3);
end;

function getAccountView(accNum : string):integer;
  var tmp : string;
  begin
     tmp := EkstrakString(accNum,'.',1)+EkstrakString(accNum,'.',2);
     Result := StrToInt(tmp);
end;

procedure DeleteRowTerakhir(sender : TAdvStringGrid;LimitRow,RowDelete:integer);
begin
  if sender.RowCount > LimitRow then
    sender.RowCount := sender.RowCount - RowDelete;

end;

procedure DeleteRowKosong(sender : TAdvStringGrid;FixedRow,KolKriteria:integer;IsNumber,isFloatingFooter:boolean);
var i,row:integer;Kriteria : string;
begin
  row := IfThen(isFloatingFooter,sender.RowCount-2,sender.RowCount-1);
  for i:= FixedRow to row do begin
    Kriteria := IfThen(IsNumber,IntToStr(0),'');
    If sender.Cells[KolKriteria,i] = Kriteria then begin
       sender.RemoveRows(i,1);
       sender.Update;

    end;
  end;
end;


function IsClear(AGrid: TAdvStringGrid; ARow, ACol: integer): boolean;
begin
    result := False;
    if (AGrid.Cells[ACol,ARow] = '') then
      result := True;
end;

function GetMonthYearOnDate(aMonth: Integer; aYear: TDate): String;
  var Month, vTemp, vTemp1: string;
begin
  if aMonth < 10 then
    Month := '0' + IntToStr(aMonth)
  else Month := IntToStr(aMonth);
  vTemp := EkstrakString(DateToStr(aYear),'/',3);
  vTemp1:= '01/' + Month + '/' +vTemp;
  Result:= vTemp1;//+ '-' + Month + '-01' ;
end;

procedure txtSetAllReadOnly(aForm : TForm; AReadOnly: Boolean); {-- membuat semua edit jadi disable}
var i: Integer;
begin
 with aForm do begin
   for i:=0 to ComponentCount-1 do begin
     if (Components[i] is TEdit) then TEdit(Components[i]).ReadOnly := AReadOnly;
     if (Components[i] is TAdvEdit) then TAdvedit(Components[i]).ReadOnly := AReadOnly;
     if (Components[i] is TMemo) then TMemo(Components[i]).ReadOnly := AReadOnly;
     if (Components[i] is TComboBox) then TComboBox(Components[i]).Enabled := not AReadOnly;
   end;
 end;
end;

function IsiNotes(ACaption: String): String;
begin
  Result := InputBox('Catatan',ACaption, '');
end;

function CurrToIndonesianSpeech(nl : string):string;
const vlstep : array [0..4] of string = (' ','ribu ','juta ','milyar ','trilyun ');
var i, j, k, panjang, m  : integer;
    stemp, stemp2, addstr : string;
    nl2, nltemp, qtemp : string;
    good, belas : boolean;
begin
  good := false;
  for i:=1 to Length(nl) do
  begin
    if (nl[i] <> '0') then
       good := true;
    if (good) then
       nl2 := nl2 + nl[i];
  end;
  if (length(nl2) > 15) then
     nltemp:=Copy(nl2,Length(nl2)-15+1,15)
  else nltemp := nl2;
  stemp2 := '';
  for i:=0 to 4 do
  begin
    k := Length(nltemp);
    if (k = 0) then
       break;
    if (k >= 3) then
       qtemp := Copy(nltemp,Length(nltemp)-2,3)
    else qtemp := nltemp;
    nltemp := Copy(nltemp,1,Length(nltemp)-3);
    stemp := '';
    belas := false;
    if (k >= 3) then
       panjang := 3
    else panjang:=k;
    m := 4-panjang;
    for j:=1 to panjang do
    begin
      addstr := '';
      if (qtemp[j] = '9') then
         addstr := 'sembilan ';
      if (qtemp[j] = '8') then
         addstr := 'delapan ';
      if (qtemp[j] = '7') then
         addstr := 'tujuh ';
      if (qtemp[j] = '6') then
         addstr := 'enam ';
      if (qtemp[j] = '5') then
         addstr := 'lima ';
      if (qtemp[j] = '4') then
         addstr := 'empat ';
      if (qtemp[j] = '3') then
         addstr := 'tiga ';
      if (qtemp[j] = '2') then
         addstr := 'dua ';
      if (qtemp[j] = '1') then
      begin
        case m of
        1,2: case m of
              1:addstr := 'se';
              2:belas := true;
             end;
        3: if (not belas) then
           begin
              if (i = 1) then
              begin
                if (stemp = '') then
                   addstr := 'se'
                else addstr := 'satu ' ;
              end else addstr := 'satu ';
           end else addstr := 'se';
        end;
      end;
      if (qtemp[j] = '0') then
         if (belas) then
         begin
           addstr := 'sepuluh ';
           belas := false;
         end;
      if ((addstr <> '') or (belas)) then
      begin
        case m of
        1: addstr := addstr + 'ratus ';
        2: if (not belas) then
              addstr := addstr+ 'puluh ';
        3: if (belas) then
              addstr := addstr+ 'belas ';
        end;
      end;
      stemp := stemp + addstr;
      inc(m);
    end;
    if (stemp <> '') then
       stemp2 := stemp + vlstep[i] + stemp2;
  end;
  result := stemp2+'rupiah';
  if (result <> '') then
     result[1] := upcase(result[1]);
end;

function ConvertNumberTerbilang(nl : string):string;
const vlstep : array [0..4] of string = (' ','ribu ','juta ','milyar ','trilyun ');
var i, j, k, panjang, m  : integer;
    stemp, stemp2, addstr : string;
    nl2, nltemp, qtemp : string;
    good, belas : boolean;
begin
  good := false;
  for i:=1 to Length(nl) do
  begin
    if (nl[i] <> '0') then
       good := true;
    if (good) then
       nl2 := nl2 + nl[i];
  end;
  if (length(nl2) > 15) then
     nltemp:=Copy(nl2,Length(nl2)-15+1,15)
  else nltemp := nl2;
  stemp2 := '';
  for i:=0 to 4 do
  begin
    k := Length(nltemp);
    if (k = 0) then
       break;
    if (k >= 3) then
       qtemp := Copy(nltemp,Length(nltemp)-2,3)
    else qtemp := nltemp;
    nltemp := Copy(nltemp,1,Length(nltemp)-3);
    stemp := '';
    belas := false;
    if (k >= 3) then
       panjang := 3
    else panjang:=k;
    m := 4-panjang;
    for j:=1 to panjang do
    begin
      addstr := '';
      if (qtemp[j] = '9') then
         addstr := 'sembilan ';
      if (qtemp[j] = '8') then
         addstr := 'delapan ';
      if (qtemp[j] = '7') then
         addstr := 'tujuh ';
      if (qtemp[j] = '6') then
         addstr := 'enam ';
      if (qtemp[j] = '5') then
         addstr := 'lima ';
      if (qtemp[j] = '4') then
         addstr := 'empat ';
      if (qtemp[j] = '3') then
         addstr := 'tiga ';
      if (qtemp[j] = '2') then
         addstr := 'dua ';
      if (qtemp[j] = '1') then
      begin
        case m of
        1,2: case m of
              1:addstr := 'se';
              2:belas := true;
             end;
        3: if (not belas) then
           begin
              if (i = 1) then
              begin
                if (stemp = '') then
                   addstr := 'se'
                else addstr := 'satu ' ;
              end else addstr := 'satu ';
           end else addstr := 'se';
        end;
      end;
      if (qtemp[j] = '0') then
         if (belas) then
         begin
           addstr := 'sepuluh ';
           belas := false;
         end;
      if ((addstr <> '') or (belas)) then
      begin
        case m of
        1: addstr := addstr + 'ratus ';
        2: if (not belas) then
              addstr := addstr+ 'puluh ';
        3: if (belas) then
              addstr := addstr+ 'belas ';
        end;
      end;
      stemp := stemp + addstr;
      inc(m);
    end;
    if (stemp <> '') then
       stemp2 := stemp + vlstep[i] + stemp2;
  end;
  result := stemp2;
  if (result <> '') then
     result[1] := upcase(result[1]);
end;


function getListNameCaseSensitive(list : TStringList; cari : string) : string;
var i : Integer;
begin
  Result := '';
  for i := 0 to list.count-1 do begin
    if list.Names[i] = cari then begin
      Result := list.ValueFromIndex[i];
      Exit;
    end;
  end;
end;

function encriptNew(input : string ) : string;
var list : TStringList;
    hasil, temp : string;
    i : Integer;
begin
  list := TStringList.create;
  list.Add('a=e9r');
  list.Add('b=asF');
  list.Add('c=ytH');
  list.Add('d=43y');
  list.Add('e=nSu');
  list.Add('f=fdQ');
  list.Add('g=uyo');
  list.Add('h=jhr');
  list.Add('i=vgb');
  list.Add('j=nm7');
  list.Add('k=cvF');
  list.Add('l=yKV');
  list.Add('m=k93');
  list.Add('n=fdk');
  list.Add('o=vfd');
  list.Add('p=34g');
  list.Add('q=320');
  list.Add('r=eds');
  list.Add('s=ehj');
  list.Add('t=ebv');
  list.Add('u=etr');
  list.Add('v=w94');
  list.Add('w=vcf');
  list.Add('x=pty');
  list.Add('y=j9f');
  list.Add('z=xje');
  list.Add('A=e92');
  list.Add('B=asD');
  list.Add('C=yFH');
  list.Add('D=4Xy');
  list.Add('E=n1u');
  list.Add('F=GdQ');
  list.Add('G=Yyo');
  list.Add('H=jJr');
  list.Add('I=v7b');
  list.Add('J=NM7');
  list.Add('K=c0F');
  list.Add('L=QeV');
  list.Add('M=kg3');
  list.Add('N=jhk');
  list.Add('O=cir');
  list.Add('P=dZi');
  list.Add('Q=kR1');
  list.Add('R=fdk');
  list.Add('S=jLi');
  list.Add('T=f9w');
  list.Add('U=DYx');
  list.Add('V=vde');
  list.Add('W=akb');
  list.Add('X=dsf');
  list.Add('Y=gfv');
  list.Add('Z=jul');
  list.Add('0=cHs');
  list.Add('1=dIe');
  list.Add('2=lgu');
  list.Add('3=mIe');
  list.Add('4=SuM');
  list.Add('5=heN');
  list.Add('6=32x');
  list.Add('7=efr');
  list.Add('8=fd8');
  list.Add('9=fUK');

  hasil := '';
  for i := 1 to Length(input) do begin
    temp := getListNameCaseSensitive(list, input[i]);
    if temp = '' then
      temp := input[i];
    hasil := hasil + temp;
  end;
  Result := hasil;
  list.Destroy;
end;

function decriptNew(input : string) : string;   
var list : TStringList;
    hasil, temp, cari : string;
    i, charke : Integer;
begin
  list := TStringList.create;
  list.Add('e9r=a');
  list.Add('asF=b');
  list.Add('ytH=c');
  list.Add('43y=d');
  list.Add('nSu=e');
  list.Add('fdQ=f');
  list.Add('uyo=g');
  list.Add('jhr=h');
  list.Add('vgb=i');
  list.Add('nm7=j');
  list.Add('cvF=k');
  list.Add('yKV=l');
  list.Add('k93=m');
  list.Add('fdk=n');
  list.Add('vfd=o');
  list.Add('34g=p');
  list.Add('320=q');
  list.Add('eds=r');
  list.Add('ehj=s');
  list.Add('ebv=t');
  list.Add('etr=u');
  list.Add('w94=v');
  list.Add('vcf=w');
  list.Add('pty=x');
  list.Add('j9f=y');
  list.Add('xje=z');
  list.Add('e92=A');
  list.Add('asD=B');
  list.Add('yFH=C');
  list.Add('4Xy=D');
  list.Add('n1u=E');
  list.Add('GdQ=F');
  list.Add('Yyo=G');
  list.Add('jJr=H');
  list.Add('v7b=I');
  list.Add('NM7=J');
  list.Add('c0F=K');
  list.Add('QeV=L');
  list.Add('kg3=M');
  list.Add('jhk=N');
  list.Add('cir=O');
  list.Add('dZi=P');
  list.Add('kR1=Q');
  list.Add('fdk=R');
  list.Add('jLi=S');
  list.Add('f9w=T');
  list.Add('DYx=U');
  list.Add('vde=V');
  list.Add('akb=W');
  list.Add('dsf=X');
  list.Add('gfv=Y');
  list.Add('jul=Z');
  list.Add('cHs=0');
  list.Add('dIe=1');
  list.Add('lgu=2');
  list.Add('mIe=3');
  list.Add('SuM=4');
  list.Add('heN=5');
  list.Add('32x=6');
  list.Add('efr=7');
  list.Add('fd8=8');
  list.Add('fUK=9');

  hasil := '';
  cari := '';
  charke := 0;
  for i := 1 to Length(input) do begin
    if (IsAlphaExist(input[i])) or (IsNumericExist(input[i])) then begin
      charke := charke + 1;
      cari := cari + input[i];
      if (charke = 3) then begin
  //      temp := list.Values[cari];
        temp := getListNameCaseSensitive(list, cari);

        if temp = '' then
          temp := cari;
        hasil := hasil + temp;
        cari := '';
        charke := 0;
      end;
    end else hasil := hasil + input[i];

  end;
  Result := hasil;
  list.Destroy;
end;


//function CreateNewSeq(ASeqName, ATableName: String): Integer;
//begin
//  Result := getIntegerFromSQL('SELECT MAX('+ASeqName+') FROM '+ATableName)+1;
//end;
//
//function GetCurrentSeq(ASeqName, ATableName: String): Integer;
//begin
//  Result := getIntegerFromSQL('SELECT MAX('+ASeqName+') FROM '+ATableName);
//end;


//function getNextNIP(fieldName:string;tableName:string; condition:String;
//        prefix,suffix:string;minlength:integer=5):string;
//var sql:string;buffer:_RecordSet;code:string; codeNum:integer; {codeChar:Char;} cond: string;
//begin
//    sql := 'SELECT MAX('+fieldName+') FROM '+tableName;
//    cond := '';
//    if condition <> '' then
//      cond := cond + ' AND ' + copy(condition,5,length(condition));
//    if prefix <> '' then
//      cond := cond + ' AND ' + fieldname + ' LIKE ' + FormatSQLString(prefix+suffix+'%');
//    if cond <> '' then begin
//      delete(cond,1,5);
//      sql := sql + ' WHERE ' + cond;
//    end;
//    buffer := myConnection.OpenSQL(sql);
//    if buffer.RecordCount > 0 then begin
//      code := BufferToString(buffer.Fields[0].Value);
//      //cut off the prefix the last digit if it is a character
//      //code := copy(code,length(prefix)+1,length(code)-length(prefix));
//      code := copy(code, length(code)-2, length(code)); //add by alin 14-05-07
//      //code := EkstrakString(code,'.',1);
//      codeNum := StrToIntDef(code,0);
//      inc(codeNum);
//      result := prefix+suffix+'.'+IntToStrFixed(minlength,codeNum);
//    end else
//      result := prefix+suffix+'.'+IntToStrFixed(minlength,1);
//    buffer.Close;
//end;


end.
