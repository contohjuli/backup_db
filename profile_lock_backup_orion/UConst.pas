unit UConst;
{$DEFINE ORCL}                                                                                                                            
{DEFINE POSTGRE}
interface

uses Graphics, AdvStyleIF;

const
  {EMAIL}
//  Setting_EASend = 'ES-AA1141023508-00959-1856F4BBF62E35C2338720D2259DC407';
//  Setting_EAGet = 'EG-AA1150422356-00734-CF213AB1F2B11FC81A0135087066E04D';
//  Setting_From = 'orion.bandung@gmail.com';
//  Setting_From = 'coba.orion.bandung@gmail.com';
//  Setting_From = 'accsopsg@gmail.com';
//  Setting_Email_SMTP = 'smtp.gmail.com';
//  Setting_Email_POP = 'imap.gmail.com';
//  Setting_Email_Port_Kirim = 465;
//  Setting_Email_Port_Ambil = 993;
//  Setting_Email_Password = 'orionbdg123';
//  Setting_Email_Password = 'psg123456';
//  Setting_Email_Password = 'orionbdg';

  MailServerImap4        = 1;

  EMAIL_FILE = 'Email.dll';

  {System Messages}
  MSG_UNAUTHORISED_ACCESS       = 'Anda tidak memiliki autorisasi untuk fungsi ini';
  MSG_SAVE_CONFIRMATION         = 'Apakah data sudah benar?';
  MSG_DELETE_CONFIRMATION       = 'Data ini akan dihapus?';
  MSG_SUCCESS_SAVING            = 'Penyimpanan berhasil.';
  MSG_UNSUCCESS_SAVING          = 'Penyimpanan tidak berhasil.';
  MSG_SUCCESS_UPDATE            = 'Update data berhasil.';
  MSG_UNSUCCESS_UPDATE          = 'Update data gagal.';
  MSG_DUPLICATE                 = 'Duplikasi data.';
  MSG_NO_DATA_FOUND             = 'Data tidak ada atau tidak ditemukan.';
  MSG_ADD_DATA                  = 'Tambah data lagi ?';
  MSG_SAVE_ADD_DATA             = 'Penyimpanan berhasil, '+#13+'tambah data lagi ?';
  MSG_NO_NUMBER                 = '[Empty]';
  MSG_UNDERCONSTRUCTION         = 'Under Construction.';
  MSG_CONFIRMED_AKTIVASI        = 'Data akan dinonaktifkan / diaktifkan ? ';
  MSG_SUCCESS_DELETING          = 'Data berhasil dihapus.';
  MSG_UNSUCCESS_DELETING        = 'Data tidak berhasil dihapus.';
  MSG_SUCCESS_AKTIVASI          = 'Data berhasil diaktifkan.';
  MSG_UNSUCCESS_AKTIVASI        = 'Data tidak berhasil diaktifkan.';
  MSG_SUCCESS_NOT_AKTIVASI      = 'Data berhasil dinonaktifkan.';
  MSG_UNSUCCESS_NOT_AKTIVASI    = 'Data tidak berhasil dinonaktifkankan.';
  MSG_CAN_NOT_DELETE            = 'Transaksi ini tidak bisa dihapus, '+#13+
                                  'karena sudah digunakan oleh transaksi yang lain.';
  MSG_ROW_DELETE_CONFIRMATION   = 'Baris ini akan dihapus?';
  MSG_SERVER_BUSY               = 'Komputer server sedang sibuk.'+#13+ 'Silahkan coba diulangi lagi.';
  MSG_CAN_NOT_UPDATE            = 'Transaksi ini tidak bisa diedit, '+#13+
                                  'karena sudah digunakan oleh transaksi yang lain.';
  MSG_PRINT                     = 'Cetak Transaksi ini?';

  MSG_BRG_SUDAH_DIOPNAME_EDIT   = 'Data tidak dapat diedit karena ada barang yang sudah diopname.';
  MSG_BRG_SUDAH_DIOPNAME_HAPUS  = 'Data tidak dapat dihapus karena ada barang yang sudah diopname.';
  MSG_SUDAH_JURNAL_EDIT         = 'Data tidak dapat diedit karena sudah dijurnal.';
  MSG_SUDAH_JURNAL_HAPUS        = 'Data tidak dapat dihapus karena sudah dijurnal.';

  MSG_CAN_NOT_DELETE_PGW        = 'Pegawai dari transaksi ini tidak bisa dihapus, '+#13+
                                  'karena sudah digunakan oleh transaksi yang lain.';
  MSG_CAN_NOT_EDIT              = 'Transaksi ini tidak bisa diedit, '+#13+
                                  'karena sudah digunakan oleh transaksi yang lain.';
  MSG_CAN_NOT_EDIT_PGW          = 'Pegawai dari transaksi ini tidak bisa diedit, '+#13+
                                  'karena sudah digunakan oleh transaksi yang lain.';

  {Detail pembayaran massage}
  MSG_JENIS_BAYAR               = 'Tipe bayar harus dipilih.';
  MSG_ALAT_BAYAR                = 'Kas / Bank harus dipilih.';
  MSG_JUMLAH                    = 'Kolom jumlah harus diisi.';
  MSG_NO_DOKUMEN                = 'No dokumen harus diisi.';
  MSG_TGL_CAIR                  = 'Tgl. cair harus diisi.';
  MSG_TGL_JT                    = 'Tgl. jatuh tempo harus diisi.';
  MSG_DUPLICATE_DOKUMEN         = 'No. dokumen sudah ada.';
  MSG_TGL_JTH_TEMPO             = 'Tgl. jatuh tempo harus >= tgl transaksi.';
  MSG_TGL_CAIR_TRANS            = 'Tgl. cair harus >= tgl transaksi.';
  MSG_CEK_SALDO                 = 'Saldo tidak mencukupi.';
  MSG_TGL_TRANS                 = 'Tanggal transaksi tidak boleh > dari akhir bulan ini.';

  MSG_SUPPLIER                  = 'Supplier harus dipilih.';
  MSG_CUSTOMER                  = 'Customer harus dipilih.';
  MSG_RELASI                    = 'Relasi harus dipilih.';
  MSG_NO_VOUCHER                = 'No. Voucher harus diisi.';

  BTN_CAP_OK                    = '&OK';
  BTN_CAP_SAVE                  = '&Simpan';
  BTN_CAP_CANCEL                = '&Batal';
  BTN_CAP_DELETE                = '&Hapus';
  BTN_CAP_CLOSE                 = 'Close';
  BTN_CAP_PRINT                 = '&Cetak';
  BTN_CAP_RESET                 = '&Reset';
  BTN_CAP_ADD                   = '&Tambah';
  BTN_CAP_EDIT                  = '&Ubah';
  BTN_CAP_LIST                  = 'List All';
  BTN_CAP_FIND                  = 'Ca&ri';
  BTN_CAP_DETAIL                = '&Detail';
  BTN_CAP_SHOW_FILTER           = '&Lihat Filter';
  BTN_CAP_HIDE_FILTER           = 'Tutup &Filter';

  {table field constants}

  {Menu Groups}
  MENU_GROUP_SYSTEM    = 'System';
  MENU_GROUP_FINANCE   = 'Finance';
  MENU_GROUP_INVENTORY = 'Inventory';
  MENU_GROUP_SALES     = 'Sales';
  MENU_GROUP_PURCHASES = 'Purchases';

    {Form_Type}
  Form_type_setting        = 1;
  Form_type_master         = 2;
  Form_type_transaksi      = 3;
  Form_type_keuangan       = 4;
  Form_type_laporan        = 5;

  STYLE_OFFICE_2003_BLUE     = 'S1';
  STYLE_OFFICE_2003_CLASSIC  = 'S2';
  STYLE_OFFICE_2003_OLIVE    = 'S3';
  STYLE_OFFICE_2003_SILVER   = 'S4';
  STYLE_OFFICE_2007_LUNA     = 'S5';
  STYLE_OFFICE_2007_OBSIDIAN = 'S6';
  STYLE_OFFICE_2007_SILVER   = 'S7';
  STYLE_OFFICE_2010_BLACK    = 'S8';
  STYLE_OFFICE_2010_BLUE     = 'S9';
  STYLE_OFFICE_2010_SILVER   = 'S10';
  STYLE_TERMINAL             = 'S11';
  STYLE_WHIDBEY              = 'S12';
  STYLE_WINDOWS_7            = 'S13';
  STYLE_WINDOWS_VISTA        = 'S14';
  STYLE_WINDOWS_XP           = 'S15';

  STATUS_TRUE  = 'T';
  STATUS_FALSE = 'F';

  {Tipe Harga}
  HRG_MRPKAN_DPP = '1';
  HRG_INC_PPN    = '2';
  HRG_NON_PPN    = '3';


  HRG_MRPKAN_DPP_TEXT = 'Harga merupakan DPP';
  HRG_INC_PPN_TEXT    = 'Harga sudah termasuk PPN';
  HRG_NON_PPN_TEXT    = 'Harga tanpa PPN';

  {Tipe PPN}
  TIPE_PPN       = '1';
  TIPE_NON_PPN   = '2';

  //tipe patch saldo
  PATCH_SALDO_BARANG             = 'CJ';
  PATCH_SALDO_BARANG_KAIN        = 'CI';
  PATCH_SALDO_BARANG_KAIN_ECERAN = 'CE';
  PATCH_SALDO_BAHAN_BAKU         = 'CB';
  PATCH_SALDO_KASBANK            = 'CK';
  PATCH_SALDO_ASSET              = 'CA';
  PATCH_SALDO_MODAL              = 'CM';
  PATCH_SALDO_AKUN               = 'CN';

  CG_TIPE_GUDANG_INTERNAL = 'INT';
  CG_TIPE_GUDANG_EXTERNAL = 'EXT';
  CG_TIPE_GUDANG_INTERNAL_TEXT = 'Internal';
  CG_TIPE_GUDANG_EXTERNAL_TEXT = 'Eksternal';

  {Status PO}
  PO_BARU            = 'B';
  PO_TERIMA_SEBAGIAN = 'T';
  PO_TERIMA_SEMUA    = 'S';
  PO_TUTUP           = 'C';
  PO_BATAL           = 'A';
  PO_BUKA            = 'U';

  {Status DO}
  DO_OK       = 'O';
  DO_SELISIH  = 'S';
  DO_BATAL    = 'B';

  {dokumen pembayaran tipe}
  DOC_TYPE_CASH                   = 'C';
  DOC_TYPE_CHEQUE                 = 'Q';
  DOC_TYPE_GIRO                   = 'G';
  DOC_TYPE_TRANSFER               = 'T';     
  DOC_TYPE_DEBIT                  = 'D';
  DOC_TYPE_KREDIT                 = 'K';

  DOC_TYPE_CASH_TEXT              = 'Cash';
  DOC_TYPE_CHEQUE_TEXT            = 'Cheque';
  DOC_TYPE_GIRO_TEXT              = 'Bilyet Giro';
  DOC_TYPE_TRANSFER_TEXT          = 'Transfer';
  DOC_TYPE_DEBIT_TEXT             = 'Debit';
  DOC_TYPE_KREDIT_TEXT            = 'Kredit';

  //setting bulan tahun
  GLOBAL_ED_INPUT = 'MMM yyyy';


  ERR_LOCKED_TABLE        = 'ORA-00060';
  MSG_SERVER_BUSY_INFORM  = 'Penyimpanan tidak berhasil. Server sedang sibuk.';


  {max level constraint}

  MAX_PROG_LEVEL                = 3;

  {document numbering schema}

  DISABLED_BG_COLOR = clBtnFace;
  DISABLED_FG_COLOR = clWindowText;

  {user management}
  LEVEL_OPERATOR    = 1;
  LEVEL_SUPERVISOR  = 2;
  LEVEL_MANAGER     = 3;
  LEVEL_ADMIN       = 4;
  LEVEL_OWNER       = 5;
  LEVEL_DEFAULT     = 9;
  LEVEL_DEVELOPER   = 10;
  PWD_LENGTH        = 8;
  DEVELOPER_NIK     = '~';
  OWNER_NIK         = '0';

  TEXT_LEVEL_OPERATOR    = 'Operator';
  TEXT_LEVEL_SUPERVISOR  = 'Supervisor';
  TEXT_LEVEL_MANAGER     = 'Manager';
  TEXT_LEVEL_ADMIN       = 'Admin';
  TEXT_LEVEL_OWNER       = 'Direksi';
  TEXT_LEVEL_DEVELOPER   = 'Developer';
                                                                       
  //Transaksi


  TRANS_TERIMA_BJ_DARI_PROD     = 'TP';
  TRANS_RETUR_KE_PROD           = 'RP';
  TRANS_SALES_ORDER             = 'SO';
  TRANS_DELIVERY_ORDER          = 'DO';
  TRANS_SURAT_JALAN             = 'SJ';
  TRANS_FAKTUR_JUAL             = 'FJ';
  TRANS_RETUR_JUAL              = 'RJ';
  TRANS_PO_BARANG_JADI          = 'PB';
  TRANS_TERIMA_BRG_JD_DR_SUPP   = 'TB';
  TRANS_FAKTUR_BELI             = 'FB';
  TRANS_RETUR_BELI              = 'RB';
  TRANS_MUTASI_GUDANG           = 'MG';
  TRANS_KELUAR_BRG_DI_TK        = 'KB';
  TRANS_KELUAR_BRG_NON_JUAL     = 'KN';
  TRANS_KELUAR_BRG_KE_SALES     = 'KS';
  TRANS_KEMBALI_BRG_DR_SALES    = 'BS';
  TRANS_JUAL_BRG_DR_SALES       = 'JS';
  TRANS_SETTING_KOMISI_SALES    = 'SK';
  TRANS_STOK_OPNAME             = 'SP';    
  TRANS_PATCH_SALDO         = 'PS';

  TRANS_KONTRAK_HRG_PROSES  = 'KH';

  //nomor patch saldo
  H_Patch_Saldo_Barang  = 'PSB';
  H_Patch_Saldo_Barang_KAIN  = 'PSL';
  H_Patch_Saldo_Barang_KAIN_ECERAN = 'PSE';
  H_Patch_Saldo_Hutang  = 'PSH';
  H_Patch_Saldo_Kasbank = 'PKB';
  H_Patch_Saldo_Piutang = 'PSP';
  H_Patch_Saldo_Asset   = 'PSA';
  H_Patch_Saldo_Modal   = 'PSM';
  H_Patch_Saldo_KASBON_PGW  = 'PKP';

  TYPE_TRANS_PENDAPATAN = 'P';
  TYPE_TRANS_BIAYA      = 'B';
  //TIPE DEBET / CREDIT
  DB_CR_TYPE_DEBIT      = 'D';
  DB_CR_TYPE_CREDIT     = 'C';

  NR_LR_TYPE_NR         = 'N';
  NR_LR_TYPE_LR         = 'L';

  {TIpe Transaksi Akun}
  TIPE_TRANS_AKUN_BIAYA = 'B';
  TIPE_TRANS_AKUN_PENDAPATAN = 'P';

  {TIPE AKUN}
  AKUN_ASSET       = '1';
  AKUN_PASIVA      = '2';
  AKUN_MODAL       = '3';   
  AKUN_PENDAPATAN  = '4';
  AKUN_BIAYA       = '5';
  AKUN_BIAYA_DIBAYAR_DIMUKA = '6';

  //Setting TUnj & Potongan
  TIPE_TUNJANGAN = 'TUNJANGAN';
  TIPE_POTONGAN  = 'POTONGAN';


  SALDO_HUTANG_SUPP                  = 'H0';
  SALDO_HUTANG_RETUR_CUST            = 'H1';
  SALDO_HUTANG_JASA_PRODUKSI         = 'H2';
  SALDO_HUTANG_JASA_LAIN_LAIN        = 'H3';
  SALDO_HUTANG_ASSET_SUPP            = 'H4';
  SALDO_HUTANG_ASSET_CUST            = 'H5';
  SALDO_HUTANG_PINJAMAN_RELASI_SUPP  = 'H6';
  SALDO_HUTANG_PINJAMAN_RELASI_CUST  = 'H7';
  SALDO_HUTANG_RETUR_CUST_PUTUS      = 'H8';
  SALDO_HUTANG_PINJAMAN_PIHAK3       = 'H9';
  SALDO_HUTANG_DIST                  = 'H10';
  SALDO_HUTANG_BIAYA                 = 'H11';

  SALDO_PIUTANG_RETUR_SUPP           = 'R0';

  SALDO_PIUTANG_DP_SUPP              = 'D0';
  SALDO_PIUTANG_DP_ASSET             = 'D1';
  SALDO_HUTANG_DP_CUST               = 'D2';
  SALDO_HUTANG_DP_ASSET              = 'D3';
  SALDO_PIUTANG_DP_DIST              = 'D5';

  SALDO_PIUTANG_CUST                 = 'P0';
  SALDO_PIUTANG_SALES                = 'P1';
  
  NILAI_RUGI              = 'R';
  NILAI_LABA              = 'L';

  KASBANK_KAS             = 'K';
  KASBANK_BANK            = 'B';


  {JENIS_MASTER}
  MASTER_AKUN = 'MA';
  MASTER_SUPPLIER = 'MS';
  MASTER_CUSTOMER = 'MC';
  MASTER_RELASI = 'MR'; 
  MASTER_SALES = 'ML';  
  MASTER_ISI = 'MI';
  MASTER_WARNA ='MW';

  CG_JENIS_AGEN                  = 'AGN';
  CG_JENIS_ONLINE_RETAIL         = 'ONL';
  CG_JENIS_TOKO_STORE            = 'TOK';
  CG_JENIS_KOPERASI              = 'KOP';
  CG_JENIS_MITRA                 = 'MIT';
  CG_JENIS_MARKETING             = 'MRT';
  CG_JENIS_LAINNYA               = 'LAI';

  CG_JENIS_AGEN_TEXT                  = 'Agen';
  CG_JENIS_ONLINE_RETAIL_TEXT         = 'Online Retail';
  CG_JENIS_TOKO_STORE_TEXT            = 'Toko / Store';
  CG_JENIS_KOPERASI_TEXT              = 'Koperasi';
  CG_JENIS_MITRA_TEXT                 = 'Mitra';
  CG_JENIS_MARKETING_TEXT             = 'Marketing';
  CG_JENIS_LAINNYA_TEXT               = 'Lainnya';

  JENIS_KELUAR_ZAKAT  = 'Z';
  JENIS_KELUAR_INFAQ  = 'I';
  JENIS_KELUAR_MARKOM = 'M';
  
  JENIS_KELUAR_ZAKAT_TEXT  = 'Zakat';
  JENIS_KELUAR_INFAQ_TEXT  = 'Infaq';
  JENIS_KELUAR_MARKOM_TEXT = 'Markom';

  TIPE_MASTER_BRAND       = 1;
  TIPE_MASTER_SUB_BRAND   = 2;
  TIPE_MASTER_UKURAN      = 3;
  TIPE_MASTER_BARANG_JADI = 4;
  TIPE_MASTER_CUSTOMER    = 5;
  TIPE_MASTER_SUPPLIER    = 6;
  TIPE_MASTER_PEGAWAI     = 7;
  TIPE_MASTER_GUDANG      = 8;   
  TIPE_MASTER_WARNA       = 9;

  CG_TIPE_BARANG_GRADE_A = 'A';
  CG_TIPE_BARANG_GRADE_B = 'B';
  CG_TIPE_BARANG_NORMAL  = 'N';
  CG_TIPE_BARANG_PROMO   = 'P';                   

  CG_TIPE_BARANG_GRADE_A_TEXT = 'Grade A';
  CG_TIPE_BARANG_GRADE_B_TEXT = 'Grade B';
  CG_TIPE_BARANG_NORMAL_TEXT  = 'Normal';
  CG_TIPE_BARANG_PROMO_TEXT   = 'Promo';

  
  //tipe_vc
  VC_SUPPLIER = 'S';
  VC_CUSTOMER = 'C';
  VC_RELASI   = 'R';

  VC_SUPPLIER_TEXT = 'SUPPLIER';
  VC_CUSTOMER_TEXT = 'CUSTOMER';


// FOCUS   
//  arti dari CG adalah Constanta Global

// ----------- SEQUENCE FIXED ------------
// KELOMPOK BARANG
  CG_SEQ_KEL_BRG_KNITTING = 1;
  CG_SEQ_KEL_BRG_RIB = 4;
  CG_SEQ_KEL_BRG_KRAH = 5;
  CG_SEQ_KEL_BRG_MANSET = 6;
  CG_SEQ_KEL_BRG_BARANG   = 1;
  
// AKUN :
  CG_SEQ_AKUN_MODAL = 1;
  CG_SEQ_LABA_DI_TAHAN = 2;
  CG_SEQ_AKUN_PENDAPATAN_SELISIH_SETOR = 17;
  CG_SEQ_AKUN_BIAYA_SELISIH_SETOR = 16;


// SATUAN :
  CG_SEQ_SATUAN_KG   = 1;
  CG_SEQ_SATUAN_YARD = 2;
  CG_SEQ_SATUAN_PCS  = 3;

// NAMA SPESIFIKASI                                                                                                                                                                                                                                                                                                                                                                                                                             F                      2
//  1 WARNA                                                                                                                                                                                                                                                                                                                                                                                                                                      F                      4
//  2 NAMA KAIN                                                                                                                                                                                                                                                                                                                                                                                                                                  F                      2
//  3 LEBAR                                                                                                                                                                                                                                                                                                                                                                                                                                      F                      2
//  4 UKURAN BENANG                                                                                                                                                                                                                                                                                                                                                                                                                              F                      2
//  5 JENIS BENANG                                                                                                                                                                                                                                                                                                                                                                                                                               F                      2
//  6 GRAMASI                                                                                                                                                                                                                                                                                                                                                                                                                                    F                      2
//  7 PROSES                                                                                                                                                                                                                                                                                                                                                                                                                                     F                      1
//  8 LEBAR GET                                                                                                                                                                                                                                                                                                                                                                                                                                  F                      2
//  9 UKURAN                                                                                                                                                                                                                                                                                                                                                                                                                                     F                      2
//  10 TIPE   
//  11 JENIS BARANG  
  CG_SPEK_SEQ_WARNA = 1; 
  CG_SPEK_SEQ_LEBAR = 3;
  CG_SPEK_SEQ_GRAMASI = 6;


//  GUDANG :
  CG_SEQ_GUDANG_TOKO = 1;  
  CG_SEQ_GUDANG_1 = 2;

//  CUSTOMER :
  CG_SEQ_CUST_PUTUS = 1;

//  KAS BANK
  CG_SEQ_KAS_TOKO = 1;   
  CG_SEQ_KAS_BESAR = 2;

// ----------- End Sequence --------------


  CG_TIPE_HARGA_ROLL = 1;
  CG_TIPE_HARGA_KG   = 2;

  CG_TIPE_HARGA_ROLL_TEXT = 'R';
  CG_TIPE_HARGA_KG_TEXT   = 'E';

  CG_STATUS_JUAL_ROLL_TEXT   = 'Roll';
  CG_STATUS_JUAL_ECERAN_TEXT = 'Eceran';


  CG_EXE_ANTRIAN = 'ANTRIAN';
  CG_EXE_SO = 'SO';   
  CG_EXE_FJ = 'FJ';

  // Tipe
  CG_KELOMPOK_BARANG_TIPE_KAIN      = 1;
  CG_KELOMPOK_BARANG_TIPE_LAIN_LAIN = 2;
  
  CG_KELOMPOK_BARANG_TIPE_KAIN_TEXT      = 'KAIN';
  CG_KELOMPOK_BARANG_TIPE_LAIN_LAIN_TEXT = 'LAIN - LAIN';
  
  CG_TIPE_BERAT_BRUTO = 'B';
  CG_TIPE_BERAT_NETTO = 'N';

  CG_TIPE_PO_KAIN      = 'K';
  CG_TIPE_PO_LAIN_LAIN = 'L';
  CG_TIPE_PO_KHUSUS_KAIN = 'H';

  // TIPE BARANG
  CG_TIPE_BARANG_REGULER = 1;
  CG_TIPE_BARANG_PESANAN = 2;
  // TIPE BARANG TEXT
  CG_TIPE_BARANG_REGULER_TXT = 'Reguler';
  CG_TIPE_BARANG_PESANAN_TXT = 'Pesanan';

  // TRANSAKSI PAJAK & NON PAJAK
  CG_TRANSAKSI_PAJAK = 'W';
  CG_TRANSAKSI_NON_PAJAK = 'Z';

  {TIPE PENERIMAAN}
  CG_TIPE_PENERIMAAN_LAIN = 'L';
  CG_TIPE_PENERIMAAN_KAIN = 'K';

  // Faktur Jual
  TIPE_FAKTUR_PIUTANG = 'P';
  TIPE_FAKTUR_CASH = 'C';
  //------------

  // Kode Master Barang Pembeda Spandex dan Non Spdx
  CG_PART_KODE_BRG_SPANDEX = 'S';
  CG_PART_KODE_BRG_NON_SPANDEX = 'N';
  CG_SPANDEX_TEXT = 'SPANDEX';

  {SEQ JENIS PROSES}
  CG_SEQ_JENIS_PROSES_CELUP = 1;

  {SEQ NAMA SPESIFIKASI}
  CG_SEQ_NAMA_SPESIFIKASI_JENIS_BENANG = 2;



  
// kode akun fix
  KODE_AKUN_PENJUALAN = '40101001';
  KODE_AKUN_HPP ='50301004';  
  KODE_AKUN_PPN_MASUK ='10108002';
  KODE_AKUN_PPN_KELUAR ='20102004';
  
  KODE_AKUN_LABA_DITAHAN_PERIODE_SEKARANG ='30101002';
  KODE_AKUN_LABA_DITAHAN_PERIODE_LALU = '30101003';
  
  {akun wip lain lain}
  KODE_AKUN_DUMMY_WIP_LAIN_LAIN = '10104009';
  KODE_AKUN_WIP_LAIN_LAIN = '10104010';

  // NAMA AKUN DEFAULT SUPPLIER
  NAMA_AKUN_HUTANG_SUPP = 'HUTANG SUPPLIER';
  NAMA_AKUN_PIUTANG_SUPP = 'PIUTANG SUPPLIER';

  // NAMA AKUN DEFAULT CUSTOMER // + christian 140917
  NAMA_AKUN_HUTANG_CUST = 'HUTANG CUSTOMER';
  NAMA_AKUN_PIUTANG_CUST = 'PIUTANG CUSTOMER';

  // TIPE SURAT JALAN
  TIPE_SURAT_JALAN_DUMMY = 'D';
  TIPE_SURAT_JALAN_REGULER = 'R';
  TIPE_SURAT_JALAN_DUMMY_TEXT = 'Dummy';
  TIPE_SURAT_JALAN_REGULER_TEXT = 'Reguler';

  TipeSaldoAawal  = 1;
  TipeSaldoMasuk  = 2;
  TipeSaldoKeluar = 3;
  TipeSaldoAkhir  = 4;
  
  //Tutup Buku
  SALDO_RL_CLOSING            = 'T';
  SALDO_RL_AMBIL_LABA         = 'A';
                                 
  FLOAT_FORMAT_NOMOR   = '%.0N';
  FLOAT_FORMAT_QTY     = '%.0N';
  FLOAT_FORMAT_HARGA   = '%.0N';

  COL_LEBAR_KOLOM_NO_URUT = 35;
  COL_LEBAR_KOLOM_BARKODE = 105;
  COL_LEBAR_KOLOM_NAMA = 350;
  COL_LEBAR_KOLOM_QTY  = 50;
  COL_LEBAR_KOLOM_HARGA  = 75;
  COL_LEBAR_KOLOM_TOTAL  = 100;

{Pewarnaan Menu}      
//  WARNA_MENU_KE_2 = $00D58453;//$001D93FE;
//  WARNA_MENU_KE_1 = $00FEF0DE;
//  WARNA_MENU_KE_3 = $00FDC5A6;
//  WARNA_MENU_AGAK_TERANG = $00FEF0DE;

//  WARNA_MENU_KE_2 = $00393C3E;
//  WARNA_MENU_KE_1 = $00ACA59D;
//  WARNA_MENU_KE_3 = $00ACA59D;
//  WARNA_MENU_AGAK_TERANG = $00ACA59D;
  
{global variable}
var                                                 
  FOTOPATH: string = '\Images\';
  LOGPATH: string = 'Log\';
  SKINPATH: string = '???\Skin\';
  GLOBAL_REPORT_ID : integer;
  GLOBAL_THEMES : TTMSStyle = tsOffice2003Blue;

{cashier variable}
var
  TAX_RATE : integer = 10; {percent}
  BANK_CODE: string;
  BANK_DAY : integer =  3; {day}
  CANCEL_TOLERANCE: integer = 2; {dlm menit}

  isPOByPass : Boolean;

  SeqSalesIntern, SeqCustIntern : integer;
const
  {$IFDEF ORCL}
    DBisORCL = True;
  {$ENDIF}

  {$IFDEF POSTGRE}
    DBisORCL = False;
  {$ENDIF}
implementation

end.




